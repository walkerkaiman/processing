import processing.video.*;
Movie video;

ArrayList <Node> nodes = new ArrayList();
int pop = 20;

void setup(){
  size(600, 600);
  video = new Movie(this, "elevator.mov");
  
  frameRate(24);
  video.loop();
  
  stroke(255, 100);
  strokeWeight(2);
}

void draw(){
  background(0);
  video.volume(0);
  
  if(video.available() == true){
    video.read();
  }
  pushMatrix();
    translate(video.width/4+150, 0);
    rotate(radians(90));
    image(video, 0, 0, video.width*.6, video.height*.6);
  popMatrix();
  
  for(int i = 0; i < nodes.size(); i++){
    Node current = nodes.get(i);
    current.run();
    
    if(i > 1){
      
      //stroke(255-current.red, 0, 0, 100);
      Node previous = nodes.get(i-1);
      fill(current.red, previous.red, nodes.get(i-2).red, 50);
      beginShape();
        vertex(nodes.get(i-2).pos.x, nodes.get(i-2).pos.y);
        vertex(previous.pos.x, previous.pos.y);
        vertex(current.pos.x, current.pos.y);
      endShape(CLOSE);
    }
  }
  //saveFrame("/Users/Kman/Desktop/frames/frame-###.tiff");
  //println(frameCount);
}

void mousePressed(){
  nodes.add(new Node(mouseX, mouseY));
}



