class Ripple{
  PVector pos;
  float time;
  
  Ripple(float temp_x, float temp_y){
    pos = new PVector(temp_x, temp_y);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    time += 5;
  }
  
  void display(){
    strokeWeight(2);
    stroke(255, 255-time);
    noFill();
    ellipse(pos.x, pos.y, time, time);
  }
}
