class Node{
  PVector center, pos;
  float r, angle, red, incr;

  Node(float temp_x, float temp_y){
    angle = random(0, 360);
    r = random(0, 100);
    center = new PVector(temp_x, temp_y);
    pos = new PVector(center.x + cos(angle) * r, center.y + sin(angle) * r);
    red = random(255);
    incr = random(-5, 5);
  }
  
  void run(){
    update();
    //display();
  }
  
  void update(){
    pos.x = center.x;
    pos.y = center.y;
    
    if(center.y > height){
      center.y = 0;
    }
    
    angle = angle % 360;
    angle += incr;
  }
  
  void display(){
    //noFill();
    //noStroke();
    stroke(0, 0, 0, 150);
    strokeWeight(2);
    fill(0, 150);
    
    ellipse(pos.x, pos.y, 5, 5);
  }
  
}
