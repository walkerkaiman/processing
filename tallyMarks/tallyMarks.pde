int transX, transY, barLength;
int space = 10;

void setup(){
  size(displayHeight-100, displayHeight-100);
  
  background(255);
  stroke(0);
  strokeWeight(2);
  
  barLength = space * 4;
}

void draw(){
  smooth();
  
  pushMatrix();
    if(frameCount % 5 == 0){
      translate(space, 0);
    }
    translate(transX, transY);
    line(0, 0, 0, barLength);
  popMatrix();
  
  transX += space;
  
  if(frameCount % 5 == 0){
    line(transX, transY, transX+(space*3), transY+barLength);
    translate(space, 0);
  }
  
  if(transX > width){
    transY += barLength + space;
    transX = 0;
  }
  
  if(transY > height){
    println("FINISHED.");
    //saveFrame("/Users/Kman/Desktop/tattooIdeas/tallyMarks.png");
    noLoop();
  }
}
