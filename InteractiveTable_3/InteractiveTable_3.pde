import codeanticode.syphon.*;
import gab.opencv.*;
import processing.video.*;

OpenCV opencv;
Capture camera;
SyphonServer server;
PGraphics canvas;

void settings(){
  size(640, 480, P3D);
  PJOGL.profile = 1;
}

void setup() {
  
  String[] cameras = Capture.list();

   if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } 
  else {
    println("Available cameras:");
    printArray(cameras);
    
    camera = new Capture(this, cameras[0]);
    camera.start();
  }
  
  opencv = new OpenCV(this, width, height);
  //camera = new Capture(this, width/2, height/2, "Microsoft LifeCam VX-800", 30);
  server = new SyphonServer(this, "Processing Server");
  
  canvas = createGraphics(width, height, P2D);
  camera.start();
}

void draw() {
  canvas.beginDraw();
    canvas.scale(2);
    canvas.background(0);
    
    opencv.calculateOpticalFlow();
    //canvas.image(camera, 0, 0);
    
    opencv.drawOpticalFlow();
  canvas.endDraw();
  
  image(canvas, 0, 0);
  image(camera, 0, 0);
  server.sendImage(canvas);
}

void movieEvent(Movie m) {
  m.read();
  opencv.loadImage(camera);
}