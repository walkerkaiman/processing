import processing.video.*;
Capture cam;

ArrayList<PImage> frames = new ArrayList<PImage>();
ArrayList<PImage> bars = new ArrayList<PImage>();

int framePop = 12;
int barHeight = 50;

void setup(){
  size(640, 480);
  frameRate(24);
  
  cam = new Capture(this, width, height);
  cam.start();
}

void draw(){
  PImage imgCurrent = createImage(width, height, RGB);
  
  if(cam.available()){
    cam.read();
    
    cam.loadPixels();
    arrayCopy(cam.pixels, imgCurrent.pixels);
    frames.add(imgCurrent);
    
    if(frames.size() > framePop){
      frames.remove(0);
    }
    
    bars = new ArrayList<PImage>();
  }
  
  for(int i = 0; i <= height/(barHeight*2); i++){
    int barY = (i*width) * (barHeight*2);
    PImage currentBar = createImage(width, barHeight, RGB);
    
    currentBar.loadPixels();
    
    for(int y = 0; y < currentBar.height; y++){
      for(int x = 0; x < currentBar.width; x++){
        currentBar.pixels[(y*imgCurrent.width)+x] = imgCurrent.pixels[(barY+(y*imgCurrent.width))+x]; 
      }
    }
    
    currentBar.updatePixels();
    bars.add(currentBar);
  }
  
  if(frames.size() == framePop){
    //delayed images
    PImage delayed = frames.get(0);
    image(delayed, 0, 0);
    
    for(int i = 0; i < bars.size(); i++){
      PImage currentBar = bars.get(i);
      float barY = i * (barHeight+barHeight);
      
      //real time images (bars)
      image(currentBar, 0, barY);
    }
  }
}