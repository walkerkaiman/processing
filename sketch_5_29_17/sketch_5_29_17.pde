import peasy.*;
PeasyCam cam;

final color BLACK = color(0);
final color WHITE = color(255);
final int FPS = 30;
final float radius = 500;

boolean recording = false;
float theta, gamma, thetaStep, gammaStep;

void setup(){
  size(600, 600, P3D);
  frameRate(FPS);
  stroke(WHITE);
  fill(BLACK);
  
  cam = new PeasyCam(this, 1000);
  thetaStep = .5;
  gammaStep = 1;
}

void draw(){
  background(BLACK);
  
  for(int a = 0; a < 180; a += 5){
    for(int b = 0; b < 180; b += 5){
      PVector pos = pointOnSphere(radius, a+theta, b+gamma);
      
      pushMatrix();
        translate(pos.x, pos.y, pos.z);
        rotateY(radians(pos.y));
        rotateX(radians(pos.y));
        box(20);
      popMatrix();
    }
  }
  
  theta += thetaStep;
  gamma += gammaStep;
  
  if(recording){
    saveFrame("data/frame_#####.png");
  }
}

PVector pointOnSphere(float r, float deg_A, float deg_B){
  float rad_A = radians(deg_A);
  float rad_B = radians(deg_B);
  float x = r * cos(rad_A) * sin(rad_B);
  float y = r * sin(rad_A) * sin(rad_B);
  float z = r * cos(rad_B);
  
  return new PVector(x, y, z);
}