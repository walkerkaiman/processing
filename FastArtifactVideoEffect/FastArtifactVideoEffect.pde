import processing.video.*;
Movie video;

int artifactPopulation = 200;
int step = 3;
boolean recording;
Artifact[] artList = new Artifact[artifactPopulation];

void setup(){
  size(1280, 720);
  frameRate(24);
  
  recording = false;
  
  video = new Movie(this, "1.mov");
  video.volume(0);
  video.loop();
  
  for(int i = 0; i < artList.length; i++){
    artList[i] = new Artifact();
  }
}

void draw(){
  if(video.available()){
    video.read();
    
    for(Artifact current : artList){
      current.update();
    }
  }

  image(video, 0, 0, width, height);

  for(Artifact current : artList){
    current.display();
  }
  
  if(recording){
   saveFrame("/Users/kaimanwalker/Desktop/frames/frame-####.tiff");
  }
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}