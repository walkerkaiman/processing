class Artifact{
  int xPos, yPos, wide, tall;
  PImage artifact;
  
  Artifact(){
    xPos = int(random(width));
    yPos = int(random(height));
    
    wide = int(random(50, 300));
    tall = int(random(50, 300));
    artifact = createImage(wide, tall, HSB);
    
    update();
  }
  
  void update(){
    //Load pixels from video frame to Artifact's img
    artifact.loadPixels();
    
    int index = 0;
    for(int y = 0; y < tall; y += step){
      for(int x = 0; x < wide; x += step){
        color colorVideo = video.get(xPos + x, yPos + y);
        color c = color(hue(colorVideo)+random(-20, 20), saturation(colorVideo), brightness(colorVideo));
        
        artifact.pixels[index] = c;
        index++;
      }
    }
    artifact.updatePixels();
    checkEdges();
  }
  
  void display(){
    /*
    strokeWeight(2);
    stroke(255, 150);
    noFill();
    rect(xPos, yPos, wide, tall);
    */
    image(artifact, xPos, yPos);
  }
  
  void checkEdges(){
    if(xPos + wide < 0){
      xPos = width;
      yPos = int(random(-tall, height));
      update();
    }
    else{
      xPos -= wide/10;
    }
  }
}