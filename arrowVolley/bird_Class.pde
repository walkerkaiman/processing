class Bird{
  int index;
  PVector position, velocity, acceleration;
  float maxForce, maxSpeed, r;
  
  Bird(int temp_index){
    index = temp_index;
    position = new PVector(random(edgeSize), random(edgeSize), random(edgeSize));
    maxForce = random(.1, .5);
    maxSpeed = random(7, 10);
    acceleration = new PVector(0, 0, 0);
    velocity = new PVector(0, 0, 0);
    r = 10;
  } 
  
  void run(){
    update();
    borders();
    display();
  }
  
void follow(Flow field) {
    // What is the vector at that spot in the flow field?
    PVector desired = field.lookup(position);
    // Scale it up by maxspeed
    desired.mult(maxSpeed);
    // Steering is desired minus velocity
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);  // Limit to maximum steering force
    applyForce(steer);
  }

  void applyForce(PVector force) {
    // We could add mass here if we want A = F / M
    acceleration.add(force);
  }

  // Method to update location
  void update() {
    // Update velocity
    velocity.add(acceleration);
    // Limit speed
    velocity.limit(maxSpeed);
    position.add(velocity);
    // Reset accelertion to 0 each cycle
    acceleration.mult(0);
  }

  void display() {
    float theta = velocity.heading2D() + radians(90);
    
    stroke(255);
    strokeWeight(2);
    
    noFill();
    
    pushMatrix();
      translate(position.x, position.y, position.z);
      rotate(theta);
      
      //arrow head
      beginShape();
        vertex(-r, r);
        vertex(0, -r);
        vertex(r, r);
      endShape();
      
    popMatrix();
  }
  
  void borders() {
    if (position.x < -r) position.x = edgeSize+r;
    if (position.y < -r) position.y = edgeSize+r;
    if (position.z < -r) position.z = edgeSize+r;
    if (position.x > edgeSize+r) position.x = -r;
    if (position.y > edgeSize+r) position.y = -r;
    if (position.z > edgeSize+r) position.z = -r;
  }
  
}
