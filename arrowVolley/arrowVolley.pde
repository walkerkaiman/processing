import peasy.*;
PeasyCam cam;

int population = 400;
int edgeSize = 600;

Bird[] flock;
Flow field;

void setup(){
  size(640, 480, P3D);
  
  cam = new PeasyCam(this, 1000);
  field = new Flow(20);
  flock = new Bird [population];
  
  for(int i = 0; i < flock.length; i++){
    flock[i] = new Bird(i);
  }
}

void draw(){
  background(0);
  //translate(-edgeSize/2, -edgeSize/2, -edgeSize/2);
  rotateY(radians(180));
  field.run();
  
  for(Bird current : flock){
    current.follow(field);
    current.run();
  }  
  
  if(frameCount > 500 && frameCount < 1000){
    println("RECORDING FRAME: " + frameCount);
    //saveFrame("/Users/Kman/Desktop/frames/frames-####.png");
  }else{println("Not Recording...");}
}
