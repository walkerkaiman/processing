class Flow{
  PVector[][][] fieldList;
  float increment, timeDelta, time, force;
  int resolution, cols, rows, stacks;
  
  Flow(int temp_resolution){
    resolution = temp_resolution;
    increment = .04;
    timeDelta = .02;
    cols = edgeSize/resolution;
    rows = edgeSize/resolution;
    stacks = edgeSize/resolution;
    fieldList = new PVector[cols][rows][stacks];
    
    updateForce();
    
    noFill();
  }
  
  void run(){
    updateForce();
    //display();
  }
  
  void updateForce(){
    float xOff = 0;
    for(int x = 0; x < cols; x++){
      float yOff = 0;
      for(int y = 0; y < rows; y++){
        float zOff = 0;
        for(int z = 0; z < stacks; z++){
          float theta = map(noise(xOff+time, yOff+time, zOff+time), 0, 1, 0, TWO_PI);
          
          fieldList[x][y][z] = new PVector(cos(theta), sin(theta), noise(zOff+time));
          zOff += increment;
        }
        yOff += increment;
      }
      xOff += increment;
    }
    time += timeDelta;
  }
  
  void display(){
    for(int x = 0; x < cols; x++){
      for(int y = 0; y < rows; y++){
        for(int z = 0; z < stacks; z++){
          drawVector(fieldList[x][y][z], x*resolution, y*resolution, z*resolution, resolution+2);
        }
      } 
    }
    drawEdges();
  }
  
  void drawVector(PVector v, float x, float y, float z, float scayl){
    stroke(136, 191, 131, 80);
    strokeWeight(2);
    
    pushMatrix();
      translate(x, y, z);
      rotate(v.heading2D());
      float len = v.mag()*scayl;
      line(0, 0, 0, len, 0, 0);
    popMatrix();
  }
  
  void drawEdges(){
    stroke(0, 30);
    strokeWeight(4);
    strokeCap(PROJECT);
    
    line(0, 0, 0, edgeSize, 0, 0);
    line(0, 0, 0, 0, edgeSize, 0);
    line(0, 0, 0, 0, 0, edgeSize);
    line(edgeSize, 0, 0, edgeSize, 0, edgeSize);
    line(edgeSize, 0, 0, edgeSize, edgeSize, 0);
    line(0, edgeSize, 0, 0, edgeSize, edgeSize);
    line(0, edgeSize, 0, edgeSize, edgeSize, 0);
    line(0, 0, edgeSize, 0, edgeSize, edgeSize);
    line(0, 0, edgeSize, edgeSize, 0, edgeSize);
    line(edgeSize, 0, edgeSize, edgeSize, edgeSize, edgeSize);
    line(0, edgeSize, edgeSize, edgeSize, edgeSize, edgeSize);
    line(edgeSize, edgeSize, edgeSize, edgeSize, edgeSize, 0);
  }
  
  PVector lookup(PVector lookup){
    int column = int(constrain(lookup.x/resolution, 0, cols-1));
    int row = int(constrain(lookup.y/resolution, 0, rows-1));
    int stack = int(constrain(lookup.z/resolution, 0, stacks-1));
    
    return fieldList[column][row][stack].get();
  }
}
