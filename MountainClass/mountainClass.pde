class Mountain{
  
  // Lower values means higher resolution.
  float resolution;
  float  xStart, xEnd, zStart, zEnd, peakHeight;
  int cols, rows;
  float[][] peak;
  
  Mountain(float temp_xStart, float temp_xEnd, float temp_zStart, float temp_zEnd, float temp_resolution, float temp_peakHeight){
    xStart = temp_xStart;
    xEnd = temp_xEnd;
    zStart = temp_zStart;
    zEnd = temp_zEnd;
    peakHeight = temp_peakHeight;
    
    if(temp_resolution > 0){
      resolution = temp_resolution;
      cols = int(abs((xEnd-xStart))/resolution);
      rows = int(abs((zEnd-zStart))/resolution);
    }
    else{
      println("Resolution must be greater than zero.");
      noLoop();
    }
    peak = new float[cols][rows];
  }
  
  void compute(){
    float rabbit = 0;
    
    for(int x = 0; x < cols; x++){
      for(int z = 0; z < rows; z++){
        float y = noise(rabbit+x+z) * peakHeight;
        
        peak[x][z] = y;
        rabbit += .00005;
      }
    }
  }
  
  void display(){
    strokeWeight(2);
    stroke(255);
    noFill();
    
    for(int x = 0; x < cols; x++){
      beginShape();
        for(int z = 0; z < rows; z++){
          vertex(x * resolution + xStart, peak[x][z], z * -resolution + zStart);
        }
      endShape();
    }
    
    for(int z = 0; z < rows; z++){
      beginShape();
        for(int x = 0; x < cols; x++){
          vertex(x * resolution + xStart, peak[x][z], z * -resolution + zStart);
        }
      endShape();
    }
  } 
}
