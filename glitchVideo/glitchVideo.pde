import processing.video.*;
Movie video;

int artifactPopulation = 100;
Artifact[] artList = new Artifact[artifactPopulation];

void setup(){
  size(1280, 720);
  frameRate(24);
  
  video = new Movie(this, "1.mov");
  video.play();
  video.volume(0);
  video.loop();
  
  for(int i = 0; i < artList.length; i++){
    artList[i] = new Artifact();
  }
}

void draw(){
  background(0);
  
  if(video.available()){
    video.read();
  }
  
  image(video, 0, 0, video.width, video.height);
  
  video.loadPixels();
  
  for(Artifact current : artList){
    current.run();
  }
  
  video.updatePixels();
  
  saveFrame("/Users/kaimanwalker/Desktop/frames/frame_####.tiff");
}