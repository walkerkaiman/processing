class Artifact{
  int wide, tall, xPos, yPos;
  PImage img;
  
  Artifact(){
    xPos = int(random(width));
    yPos = int(random(height));
    wide = int(random(5, 500));
    tall = int(random(5, 50));
    img = createImage(wide, tall, RGB);
  }
  void run(){
    update();
    display();
  }
  
  void update(){
    //Load pixels from video frame to Artifact's img
    int index = 0;
    img.loadPixels();
    
    for(int y = 0; y < tall; y++){
      for(int x = 0; x < wide; x++){
        img.pixels[index] = video.get(xPos + x, yPos + y);
        index++;
      }
    }
    
    img.updatePixels();
    
    //Displace Artifact
    xPos = int(random(video.width));
    yPos = int(random(video.height));
  }
  
  void display(){
    if(yPos < mouseY-80 || yPos > mouseY+80){
      image(img, xPos, yPos);
    }
  }
  
}