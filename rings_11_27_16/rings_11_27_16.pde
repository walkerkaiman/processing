import peasy.*;
PeasyCam cam;

final color black = color (0);
final color white = color (255);
ArrayList<Ring> rings = new ArrayList<Ring>();
PVector center;
float time;
boolean recording;
int arcSize = 360;

float angleStep = 5;
float radiusStep = 10;
float smoothAmount = 40;
float waveSize = 15;
int ringPop = 30;

void setup () {
  cam = new PeasyCam(this, 1000);
  center = new PVector(width/2, height/2);
  
  size(600, 600, P3D);
  
  for (int i = 0; i < ringPop; i++) {
    float radius = (i+1) * radiusStep;
    rings.add(new Ring(radius));
  }
}

void draw () {
  background(black);
  
  for (Ring r : rings) {
    pushMatrix();
    rotate(radians(20));
    r.run();
    popMatrix();
  }
  
  arcSize += 2;
  arcSize = arcSize % 360;
  time += .05;
  
  if (recording) {
    saveFrame("data/frames/frame-#####.png");
  }
}

void keyPressed () {
  if (key == 'r' || key == 'R') {
    recording =! recording;
  }
}