class Ring {
  float radius;
  PVector [] points;
  
  Ring (float temp_radius) {
    radius = temp_radius;
  }
  
  void run () {
    update();
    display();
  }
  
  void update () {
    int NUMBER_OF_POINTS = int(arcSize/angleStep);
    points = new PVector[NUMBER_OF_POINTS + 1];
    
    for (int i = 0; i < points.length; i++) {
      float degree = i * angleStep;
      float angle = radians(degree);
      float x = cos(angle) * radius + center.x;
      float y = sin(angle) * radius + center.y;
      float z = 0;
      
      points[i] = new PVector(x, y, z);
    }
    
    for (PVector p : points) {
      float z = cos(p.x / smoothAmount + p.y / smoothAmount + radius / smoothAmount + time) * waveSize;
      p.z = z;
    }
  }
  
  
  void display () {
    stroke(white);
    noFill();
    strokeWeight(2);
    
    beginShape();
      for(PVector p : points) {
        vertex(p.x, p.y, p.z);
      }
    endShape();
  }
}