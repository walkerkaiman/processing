import peasy.*;
PeasyCam cam;

float time;
boolean recording;

void setup(){
  size(700, 700, P3D);
  frameRate(24);
  //noFill();
  
  cam = new PeasyCam(this, 1000);
}

void draw(){
  background(250);
  smooth();
  //lights();
  
  
  for(int z = 0; z < 50; z++){
    float ang = map(z, 0, 50, 0, PI);
    float offset = cos(time + z/10);
    float r = sin(ang)*800*offset;
    float weight = map(r, -800, 800, 2, 4);
    
    strokeWeight(weight);
    
    pushMatrix();
      translate(0, 0, z*16);
      ellipse(0, 0, r, r);
    popMatrix();
  }
  
  time += .1;
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.tiff"); 
  }
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}