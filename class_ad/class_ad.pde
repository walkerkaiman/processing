final color WHITE = color (255, 150);
final color BLACK = color (0);
final int STROKE_WEIGHT = 3;
final int GRID_STEP = 100;
final int MAX_DIA = 350;
final int MIN_DIA = GRID_STEP;
final float DIA_STEP = 1;
final int FPS = 30;

float currentDia = GRID_STEP;
boolean toggle = true;

void setup(){
  frameRate(FPS);
  size(700, 700);
  stroke(WHITE);
  strokeWeight(STROKE_WEIGHT);
  noFill();
}

void draw(){
  background(BLACK);
  
  for(int y = 0; y < height+GRID_STEP; y += GRID_STEP){
    for(int x = 0; x < width+GRID_STEP; x += GRID_STEP){
      ellipse(x, y, currentDia, currentDia);
    }
  }
  
  if(currentDia > MAX_DIA || currentDia < MIN_DIA){
    toggle =! toggle;
  }
  
  if(toggle){
    currentDia += DIA_STEP;
  }else{
    currentDia -= DIA_STEP;
  }
  
  //saveFrame("data/frame_#####.png");
}