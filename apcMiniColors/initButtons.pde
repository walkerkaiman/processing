void initButtons() {
  
  buttons = new ArrayList <Button> ();
  
  
  int buttonW = width/16;
  int buttonH = height/8;
  
  int note = 0;
  
  for (int y = height-buttonH; y >= 0; y -= buttonH) {
    for (int x = 0; x <= width/2-buttonW; x += buttonW) {
      buttons.add(new Button("left", note, x, y));
      note++;
    }
  }
  
  note = 0;
  
  for (int y = height-buttonH; y >= 0; y -= buttonH) {
    for (int x = width/2; x <= width-buttonW; x += buttonW) {
      buttons.add(new Button("right", note, x, y));
      note++;
    }
  }
  
  println();
  println("Buttons: " + buttons.size());
}