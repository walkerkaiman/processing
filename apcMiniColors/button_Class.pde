class Button {
  boolean activate;
  String side;
  int note, x, y;
  color pixel;
  
  Button (String temp_side, int temp_note, int temp_x, int temp_y) {
    side = temp_side;
    note = temp_note;
    x = temp_x;
    y = temp_y;
  }

  void run() {
    updateState();
    updateLED();
  }

  void updateState() {
    pixel = pixels[y * width+x];

    if (brightness(pixel) > 127) {
      activate = true;
    } else {
      activate = false;
    }
  }

  void updateLED() {
    if (activate) { // Turn the button on
      if (side.equals("left")) {
        if(red(pixel) > green(pixel) && red(pixel) > blue(pixel)){ // If red if the dominant color
          midiL.sendNoteOn(0, note, 3);
        }else if (green(pixel) > red(pixel) && green(pixel) > blue(pixel)) { // If green is the dominant color
          midiL.sendNoteOn(0, note, 1);
        }else if(blue(pixel) > red(pixel) && blue(pixel) > green(pixel)) { // If blue is the dominant color
          midiL.sendNoteOn(0, note, 5);
        }
      } else if (side.equals("right")) {
        if(red(pixel) > green(pixel) && red(pixel) > blue(pixel)){ // If red if the dominant color
          midiR.sendNoteOn(0, note, 3);
        }else if (green(pixel) > red(pixel) && green(pixel) > blue(pixel)) { // If green is the dominant color
          midiR.sendNoteOn(0, note, 1);
        }else if(blue(pixel) > red(pixel) && blue(pixel) > green(pixel)) { // If blue is the dominant color
          midiR.sendNoteOn(0, note, 5);
        }
      }
    } 
    else {
      if (side.equals("left")) { // Turn the button off
        midiL.sendNoteOn(0, note, 0);
      } else if (side.equals("right")) {
        midiR.sendNoteOn(0, note, 0);
      }
    }
  }
}