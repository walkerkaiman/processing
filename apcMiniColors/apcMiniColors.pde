import codeanticode.syphon.*;
import themidibus.*;

SyphonClient modul8;
MidiBus midiL, midiR;

PGraphics canvas;
ArrayList<Button> buttons;

final color off = color (0, 0, 0);

void settings() {
  size(640, 320, P3D);
  PJOGL.profile = 1;
}

void setup () {
  modul8 = new SyphonClient(this);
  midiL = new MidiBus(this, -1, 2);
  midiR = new MidiBus(this, -1, 3);
  //MidiBus.list();
  
  initButtons();
}

void draw() {
  
  if (modul8.newFrame()) {
    canvas = modul8.getGraphics(canvas);
    background(off);
    image(canvas, 0, 0, width, height);
  }
  
  loadPixels();
    for (Button b : buttons) {
      b.run();
    }
  updatePixels();
}