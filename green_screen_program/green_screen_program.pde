import SimpleOpenNI.*;
import processing.opengl.*;
SimpleOpenNI kinect;

float rotation = 0;
int zoom;

int begin = 200;
int red = 1500;
int green = 2000;
int end = 3000;

void setup(){
  
  size(1024, 768, OPENGL);
  
  kinect = new SimpleOpenNI(this);
  kinect.enableDepth(); 
  kinect.enableRGB();
}


void draw(){
  
  background(0);
  
  kinect.update();
  
  translate(width/2, height/2, -2000);
  
  rotateX(radians(180));
  
  translate(0, 0, 100 + (zoom));
  
  // Manual mouse rotation 
  //float mouseRotate = map(mouseX, 0, width, -180, 180);
 // rotateY(radians(mouseRotate));
  
 // Automatic Rotation 
  rotation += 1;
  rotateY(radians(rotation));
  
  
  PVector[] depthPoints = kinect.depthMapRealWorld();
  for(int i = 0; i < depthPoints.length; i += 3){
    
     PVector currentPoint = depthPoints[i];
     
     if(currentPoint.z > begin && currentPoint.z < red){
    stroke(255-i, 255, 255);
    point(currentPoint.x, currentPoint.y, currentPoint.z); 
    }
    
    if(currentPoint.z > red && currentPoint.z < green){
      stroke(0, 0, 0);
      point(currentPoint.x, currentPoint.y, currentPoint.z);
    }
    
    if(currentPoint.z > green && currentPoint.z < end){
    stroke(0, 0, 0);
    point(currentPoint.x, currentPoint.y, currentPoint.z); 
    }
  } 
  
  //println(zoom);
  //saveFrame("Alina-#####.png");
  //saveFrame("Gavin-#####.png");
}

void mousePressed(){
 saveFrame("businesscard-###.png"); 
  
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      zoom -= 40;
    } 
    if(keyCode == DOWN){
      zoom += 40;
}
  }
}
