final color black = color(0);
final color white = color(255);
final String foregroundName = "1.png";
final float offset = .3;
final float whispStrokeWeight = 2;
final float starStrokeWeight = 2;
final float dist = 10;
final float animaitonSpeed = 30;
final float charSize = 400;
final int numberOfStars = 100;
final PVector charPos = new PVector (80, 400);

ArrayList<Whisp> whisps = new ArrayList<Whisp>();
ArrayList<PVector> stars = new ArrayList<PVector>();
PImage foreground;

void setup () {
  size(800, 800);
  frameRate(animaitonSpeed);
  
  foreground = loadImage(foregroundName);
  
  for (int star = 0; star < numberOfStars; star++) {
    stars.add(new PVector(random(width), random(height)));
  }
}

void draw () {
  background(black);
  displayStars();
  
  for (Whisp w : whisps) {
    w.run();
  }
  
  image(foreground, charPos.x, charPos.y, charSize, charSize);
}

void displayStars () {
  stroke(white);
  strokeWeight(starStrokeWeight);
  
  for (PVector s : stars) {
    s.x -= 3;
    s.y += 3;
    
    if (s.x < 0) {
      s.x = width;
      s.y = random(height);
    }
    
    if (s.y > height) {
      s.x = random(width);
      s.y = 0;
    }
    
    point(s.x, s.y);
  }
}

void mousePressed () {
  color fill = whisps.size() % 2 == 0 ? white : black;
  color stroke = color(white);
  
  whisps.add(new Whisp(fill, stroke));
}

void mouseDragged () {
  PVector prevPoint = whisps.get(whisps.size()-1).points.get(whisps.get(whisps.size()-1).points.size()-1);
  float d = dist(mouseX, mouseY, prevPoint.x, prevPoint.y);
  
  if (d > dist) {
    whisps.get(whisps.size()-1).points.add(new PVector(mouseX, mouseY));
  }
}

void keyPressed() {
  if (key == BACKSPACE || key == DELETE) {
    if (whisps.size() > 0) {
      whisps.remove(whisps.size()-1);
    }
  }
}