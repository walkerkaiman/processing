class Whisp {
  ArrayList<PVector> points = new ArrayList<PVector>();
  color fill, stroke;
  
  Whisp (color fill, color stroke) {
    this.fill = fill;
    this.stroke = stroke;
    points.add(new PVector(mouseX, mouseY));
    points.add(new PVector(mouseX, mouseY));
  }
  
  void run () {
    if (points.size() > 2) {
      update();
      display();
    }
  }
  
  void update () {
    for (int i = 0; i < points.size(); i++) {
      PVector p = points.get(i);
      float x = random(-offset, offset) + p.x;
      float y = random(-offset, offset) + p.y;
      
      points.get(i).x = x;
      points.get(i).y = y;
    }
  }
  
  void display () {
    stroke(stroke);
    strokeWeight(whispStrokeWeight);
    fill(fill);
    
    beginShape();
      for (PVector p : points) {
        curveVertex(p.x, p.y);
      }
    endShape(CLOSE);
  }
}