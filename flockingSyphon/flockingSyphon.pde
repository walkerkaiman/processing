import codeanticode.syphon.*;

Flock flock;
ArrayList<Obsticle> obsticles;

PGraphics canvas;
SyphonServer server;

float time;

void settings(){
  size(600, 600, P2D);
  PJOGL.profile = 1;
}

void setup() {
  flock = new Flock();
  canvas = createGraphics(width, height, P2D);
  server = new SyphonServer(this, "Processing Frames");
  obsticles = new ArrayList<Obsticle>();
  
  for (int i = 0; i < 200; i++) {
    Boid b = new Boid(width/2,height/2);
    flock.addBoid(b);
  }
  
  for(int i = 0; i < 4; i++){
    float size = random(50, 200);
    PVector position = new PVector(random(width), random(height));
    
    obsticles.add(new Obsticle(position, size));
  }
}

void draw() {
  canvas.beginDraw();
    canvas.smooth();
    canvas.background(0);
    
    for(Obsticle current : obsticles){
      current.display();
    }
    
    flock.run();
  canvas.endDraw();
  
  time += .01;
  
  image(canvas, 0, 0);
  server.sendImage(canvas);
}

void mouseReleased() {
  
  for(Obsticle i : obsticles){
    i.pos = new PVector(random(width), random(height));
    i.size = random(50, 200);
  }
}