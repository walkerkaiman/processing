class Obsticle{
  PVector pos;
  float size, pulse, seed;
  
  Obsticle(PVector temp_pos, float temp_size){
    pos = temp_pos;
    size = temp_size;
    seed = random(200);
  }
  
  void update(){
    pulse = abs(noise(pos.x, pos.y, time)) * size;
  }
  
  void display(){
    update();
    
    canvas.stroke(255);
    canvas.strokeWeight(5);
    canvas.fill(0);
    
    canvas.ellipse(pos.x, pos.y, pulse, pulse);
  }
}