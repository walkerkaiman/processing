float time;

void setup(){
  size(500, 500);
  frameRate(24);
}

void draw(){
  background(0);
  
  pushMatrix();
  translate(width/2, height/2);
  //rotate(radians(time*5));
    stroke(255);
    strokeWeight(10);
    noFill();
    ellipse(0, 0, 450, 450);
  
    strokeWeight(4);
  
    
    beginShape();
      for(int x = -width/2+25; x < width/2-25; x++){
       vertex(x, cos(x/25+time)*40);
      }
    endShape();
  popMatrix();
  
  if(frameCount < 360){
  //saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.png");
  }
  time += .2;
}