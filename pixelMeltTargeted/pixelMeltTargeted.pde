PImage img;
ArrayList<Melt> melts = new ArrayList();
int recordingFrame;
boolean recording;
final int framesRecorded = 360;

void setup(){
  size(640, 480);
  frameRate(30);
  img = loadImage("2.png");
  img.resize(width, height);
  image(img, 0, 0);
  
  recording = false;
}

void draw(){
  if(keyPressed){
    for(int i = 0; i < melts.size(); i++){
      melts.get(i).run();
    }
  }

  record();
}

void mouseDragged(){
  melts.add(new Melt(mouseX, mouseY));
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}

void record(){
  if(recording){
    if(recordingFrame < framesRecorded){
      println("Recording...");
      saveFrame("FILE TO SAVE FILE HERE");
      recordingFrame++;
    }
    else{
      recording = false;
      recordingFrame = 0;
    }
  }
  else{
    println("Not Recording...");
  }
}