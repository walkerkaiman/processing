class Melt{
  PVector pos;
  color fill;
  float size, speed;
  
  Melt(int temp_x, int temp_y){
    pos = new PVector(temp_x, temp_y);
    fill = img.get(temp_x, temp_y);
    size = random(1, 4);
    speed = random(.1, size);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    if(keyPressed && key == CODED){
      switch(keyCode){
        case UP:
          pos.y -= speed;
          break;
        case DOWN:
          pos.y += speed;
          break;
        case LEFT:
          pos.x -= speed;
          break;
        case RIGHT:
          pos.x += speed;
          break;
      }
    }
  }
  
  void display(){
    noStroke();
    fill(fill);
    
    rect(pos.x, pos.y, size, size);
  }
}