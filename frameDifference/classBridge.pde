class Line{
  PVector pos, neighborPos;
  float intensity;
  
  Line(float temp_x, float temp_y, float temp_intensity){
    pos = new PVector(temp_x, temp_y);
    neighborPos = new PVector(temp_x, temp_y);
    intensity = temp_intensity;
  }
  
  void run(){
    findNeighbor();
    display();
  }
  
  void findNeighbor(){
    float distance = 1000000;
    
    for(Line other : lines){
      float other_dist = dist(pos.x, pos.y, other.pos.x, other.pos.y);
      
      if(other_dist > lineMin && other_dist < lineMax && other_dist < distance){
        distance = other_dist;
        neighborPos = other.pos;
      }
    }
  }
  
  void display(){
    strokeWeight(1);
    stroke(255, intensity);
    
    line(pos.x, pos.y, neighborPos.x, neighborPos.y);
  }
  
}