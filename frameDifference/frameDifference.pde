import processing.video.*;
Capture cam;

ArrayList<PImage> frames;
ArrayList<Line> lines;

PImage diff_Img;
int threshold, performance, lineMin, lineMax;

void setup(){
  size(640, 480);
  frameRate(24);
  
  cam = new Capture(this, width, height);
  cam.start();
  
  frames = new ArrayList<PImage>();
  lines = new ArrayList<Line>();
  
  diff_Img = createImage(width, height, RGB);
  
  threshold = 40;
  performance = 4;
  
  lineMin = 10;
  lineMax = 60;
}

void draw(){
  if(cam.available()){
    updateFrame();
    compareFrames();
    updateLines();
  }
  
  background(0);

  for(Line current : lines){
    current.run();
  }
}

void updateFrame(){
  PImage currentFrame = createImage(width, height, RGB);
  
  cam.read();
  cam.loadPixels();
  arrayCopy(cam.pixels, currentFrame.pixels);
  frames.add(currentFrame);
  
  if(frames.size() > 2){
    frames.remove(0);
  }
}

void compareFrames(){
  diff_Img = createImage(width, height, RGB);
  
  if(frames.size() == 2){
    PImage currentFrame = frames.get(1);
    PImage lastFrame = frames.get(0);
   
    currentFrame.loadPixels();
    lastFrame.loadPixels();
    diff_Img.loadPixels();
    
    for(int i = 0; i < width*height; i++){
      color currentPixel = currentFrame.pixels[i];
      color lastPixel = lastFrame.pixels[i];
      
      float currentRed = red(currentPixel);
      float currentGreen = green(currentPixel);
      float currentBlue = blue(currentPixel);
      
      float lastRed = red(lastPixel);
      float lastGreen = green(lastPixel);
      float lastBlue = blue(lastPixel);
      
      float diffRed = abs(currentRed - lastRed);
      float diffGreen = abs(currentGreen - lastGreen);
      float diffBlue = abs(currentBlue - lastBlue);
      
      diff_Img.pixels[i] = color(diffRed, diffGreen, diffBlue);
    }
    
    diff_Img.updatePixels();
  }
}

void updateLines(){
  lines = new ArrayList<Line>();
  diff_Img.loadPixels();
  
  for(int y = 0; y < diff_Img.height; y += performance){
    for(int x = 0; x < diff_Img.width; x += performance){
      float pixelBrightness = brightness(diff_Img.pixels[x+y*width]);
      
      if(pixelBrightness > threshold){
        lines.add(new Line(x, y, pixelBrightness));
      }
    }
  }
}