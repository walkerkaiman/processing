import peasy.*;

PeasyCam cam;

int resolution = 20;
float time;
boolean recording = false;

void setup(){
  size(1920, 1080, P3D);
  //size(320, 240, P3D);
  
  frameRate(24);
  
  cam = new PeasyCam(this, 1000);
  
  stroke(255);
  //fill(255);
  strokeWeight(2);
  noFill();
  sphereDetail(resolution);
}

void draw(){
   background(0, 255, 0);
   //background(0);
   
      for(float x = 0; x < height; x += 10){
        float rotation = radians(abs(cos(time+x/width)*360));
        float y = 0;
        float size = 10;
        
        pushMatrix();
        rotateX(rotation);
        
        ellipse(0, 0, x, x);
        popMatrix();
      }
   
   if(recording){
     saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.png");  
   }
   
   time += .01;
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}