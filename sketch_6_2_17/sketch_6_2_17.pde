import peasy.*;
PeasyCam cam;

final color BLACK = color(0);
final color WHITE = color(255);
float time;

void setup (){
  cam = new PeasyCam(this, 1000);
  size(600, 600, P3D);
  stroke(WHITE);
  noFill();
}

void draw(){
  background(BLACK);
  
  for(int ring = 0; ring < 100; ring++){
    float r = ring*50;
    float rot_rad = radians(noise((time+ring)/100)*360);
    
    pushMatrix();
      translate(width/2, height/2);
      rotateX(rot_rad);
      
      ellipse(0, 0, r, r);
    popMatrix();
  }
  
  time += .1;
}