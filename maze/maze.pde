ArrayList<Cell> cells = new ArrayList<Cell>();
int step = 30;
int perCol, perRow;

void setup() {
  perCol = int(width/step);
  perRow = int(height/step);

  size(640, 480);
  frameRate(4);
  stroke(255);
  noFill();
  strokeWeight(1);

  for (int x = 0; x <= width; x += step) {
    for (int y = 0; y <= height; y += step) {
      cells.add(new Cell(x, y));
    }
  }
}

void draw() {
  background(0);
  
  for (Cell cell : cells) {
    cell.update();
    cell.display();
  }
  //saveFrame("/Users/kaimanwalker/Desktop/frames/frame_###.png");
}