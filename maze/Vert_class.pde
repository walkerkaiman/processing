class Cell{
  PVector pos;
  int selector;
  float thickness;
  
  Cell(int _x, int _y){
    pos = new PVector(_x, _y);
    thickness = step/10;
    update();
  }
  
  void update(){
    float temp = random(1);
    
    if (temp < .3) {
      selector = 0;
    }else if (temp > .3 && temp < .6) {
      selector = 1;
    }else if(temp > .6 && temp < 1){
      //selector = 2;
    }
  }
  
  void display(){
    switch(selector){
      case 0:
        displayA();
        break;
      case 1:
        displayB();
        break;
      case 2:
        displayC();
        break;
    }
  }
  
  void displayA(){
    beginShape();
      vertex(pos.x, pos.y);
      vertex(pos.x + thickness, pos.y);
      vertex(pos.x + step, pos.y + step - thickness);
      vertex(pos.x + step, pos.y + step);
      vertex(pos.x + step - thickness, pos.y + step);
      vertex(pos.x, pos.y + thickness);
    endShape(CLOSE);
  }
  
  void displayB(){
    beginShape();
      vertex(pos.x + step, pos.y);
      vertex(pos.x + step, pos.y + thickness);
      vertex(pos.x + thickness, pos.y + step);
      vertex(pos.x, pos.y + step);
      vertex(pos.x, pos.y + step - thickness);
      vertex(pos.x + step - thickness, pos.y);
    endShape(CLOSE);
  }
  
  void displayC(){
    float mid = step / 2;
    
    beginShape();
      vertex(pos.x, pos.y + mid);
      vertex(pos.x + thickness, pos.y + mid - thickness);
      vertex(pos.x + step - thickness, pos.y + mid - thickness);
      vertex(pos.x + step, pos.y + mid);
      vertex(pos.x + step - thickness, pos.y + mid + thickness);
      vertex(pos.x + thickness, pos.y + mid + thickness);
    endShape(CLOSE);
  }
}