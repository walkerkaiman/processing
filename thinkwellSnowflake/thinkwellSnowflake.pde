import gab.opencv.*;
import processing.video.*;
import java.awt.*;

Capture video;
OpenCV opencv;

PVector pointer;
ArrayList<PVector> brushStroke = new ArrayList<PVector>();

final float brushSize = 2; // Thickness of line.
final float angStep = 20; // How large the slices are.
final float resolution = 10; // How detailed the line is.
final float speed = .05;
final float slices = 360/angStep;

void setup() {
  size(640, 480);
  
  video = new Capture(this, 640/2, 480/2);
  opencv = new OpenCV(this, 640/2, 480/2);
  opencv.loadCascade("haarcascade_palm.xml");  
  
  pointer = new PVector(0, 0);
  video.start();
  
  stroke(255);
  strokeWeight(brushSize);
  noFill();
  
  brushStroke.add(pointer); // Make a draw point in the center
}

void draw() {
  background(0);
  updatePointer();
  displayStroke();
  
  ellipse (pointer.x, pointer.y, 10, 10); // Pointer
}

void captureEvent(Capture c) {
  c.read();
}

void updatePointer () {
  //scale(2);
  opencv.loadImage(video);
  Rectangle[] faces = opencv.detect();
  
  if (faces.length > 0) {
    Rectangle face = faces[0];
    float x = face.x + face.width/2;
    float y = face.y + face.height/2;
    
    pointer = new PVector(x, y);
    
    PVector last = brushStroke.get(brushStroke.size() - 1);
    PVector current = new PVector(pointer.x, pointer.y);
    float delta = dist(current.x, current.y, last.x, last.y);
  
    if (delta > resolution) {
      brushStroke.add(pointer);
    }    
  }
}

void displayStroke() { 
  pushMatrix();
    translate(width/2, height/2); // Make new coordinate system zero'ed at center.
    
    for (int slice = 0; slice < slices; slice++) { // Repeat per slice
      rotate(radians(angStep));
      
      for (int i = 1; i < brushStroke.size(); i++) { // Draw all brush points
        PVector current = brushStroke.get(i);
        PVector previous = brushStroke.get(i-1);
        
        line(current.x, current.y, previous.x, previous.y); // Make a line from current to previous point.
        
        current.y -= speed;
        
        if (dist(current.x, current.y, width/2, height/2) > width/2) {
          brushStroke.remove(i);
        }  
      }
    }
  popMatrix();
}