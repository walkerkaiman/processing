float rabbit;

void setup(){
  size(500, 500);
  smooth();
  
  background(255);
  noFill();
}

void draw(){
  //background(255);
  
  float bigCircle = width-30;
  float smallCircle = bigCircle-600;
  
  float BigOrbitX = width/2+cos(radians(rabbit))*bigCircle/2;
  float BigOrbitY = height/2+sin(radians(rabbit))*bigCircle/2;
  
  float SmallOrbitX = width/2+cos(radians(rabbit*4))*smallCircle/2;
  float SmallOrbitY = height/2+sin(radians(rabbit*4))*smallCircle/2;
  
  strokeWeight(1);
  stroke(0, 50);
  line(BigOrbitX, BigOrbitY, SmallOrbitX, SmallOrbitY);
  
  strokeWeight(1);
  //ellipse(SmallOrbitX, SmallOrbitY, 10, 10);
  //ellipse(BigOrbitX, BigOrbitY, 10, 10);
  
  rabbit += 1;
}