void controllerChange(int channel, int number, int value) {

  // Receive a controllerChange
  println();
  println("Controller Change:");
  
  println("Channel: " + channel);
  println("Number: " + number);
  println("Value: " + value);
  println("------------------------");
  println();

  if (number == dmxSlider) {
    int scaled = int(map(value, 0, 127, 0, 255));
    String udpMessage = "DMX" + " " + number + " " + scaled;

    udp.send(udpMessage, remoteIP, udpSendPort);
    println("UDP Sent: " + udpMessage);
  }
}