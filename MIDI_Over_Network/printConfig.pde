void printConfig() {
  println();
  println("------------------------");
  println("Configuration");
  println("------------------------");

  if (isSlave) {
    println("SLAVE");
  } else {
    println("MASTER");
  }

  println("Remote IP: " + remoteIP);
  println();
  println("MIDI Input: " + midi_IN);
  println("MIDI Output: " + midi_OUT);
  println();
  println("UDP Remote Listen Port: " + udpSendPort);
  println("UDP Local Listen Port: " + udpListenPort);
  println();
  println("DMX Port: " + dmxPort);
  println("DMX Universe Size: " + universeSize);
  println("DMX Channel: " + dmxChannel);
  println("Serial Baud Rate: " + baudRate);
  println("------------------------");

  println("Initialization Completed!");
  println();
  println("Congrats, fucker...");
  println(";)");
  println();
}