/*
___________ MIDI over UDP - Read Me_________

Each computer needs to be configured via the JSON file
located in data directory. A computer is configured as 
either a MASTER or a SLAVE.

MASTER computers have a MIDI input device and transmit
UDP messages to SLAVE computers. 

Messages travel one way, thus, MASTER computers do not listen
for UDP messages and SLAVES do not have a MIDI input device.

System settings on SLAVE computers should be checked that the 
MIDI bus you need is activated and available.

SLAVE computers also have a channel of DMX. The USB DMX Pro
needs to be connected for a SLAVE computer to start.

PROGRAM FUNCTION:
The MASTER computer converts MIDI to a UDP message and sends 
to the "Remote IP" that is configured in the JSON file.

When the SLAVE receives a UDP message, it checks to see if the
message is intended for DMX or MIDI and converts to the 
appropriate protocol.
_____________________________________________

JSON File
  data/config.json

MASTER config
  "DMX Port": "",
  "UDP Send Port": 8888,
  "UDP Listen Port": 0,
  "DMX Universe Size": 0,
  "DMX Channel": 0,
  "Remote IP": "192.168.0.100",
  "MIDI Output": "-1",
  "Baud Rate": 0,
  "MIDI Input": "APC MINI"


SLAVE config
  "DMX Port": "/dev/tty.usbserial-6AVH1FM6",
  "UDP Send Port": 0,
  "UDP Listen Port": 8888,
  "DMX Universe Size": 2,
  "DMX Channel": 1,
  "Remote IP": "192.168.0.200",
  "MIDI Output": "Bus 1",
  "Baud Rate": 115200,
  "MIDI Input": "-1"
*/

import processing.serial.*;
import codeanticode.prodmx.*;
import themidibus.*;
import hypermedia.net.*;
import java.util.*;

MidiBus midi;
UDP udp;
DMX dmx;

String remoteIP, midi_IN, midi_OUT, dmxPort;
int udpSendPort, udpListenPort, baudRate, universeSize, dmxChannel, dmxSlider;
boolean isSlave;

final int on = 127;
final int off = 0;
final int channel = 0;
final color black = color(0);
final color white = color(255);

void setup () {
  size(200, 150);
  init();
}

void draw () {
  display();
}