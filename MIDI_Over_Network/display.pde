void display() {
  background(black);

  float offset = frameCount % width;

  for (int x = -width; x < width; x += 50) {
    line(x + offset, 0, x + offset, height);
  }
}