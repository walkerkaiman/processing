void noteOn(int channel, int pitch, int velocity) {
  
  String message = "MIDI " + str(pitch) + " ";
  
  udp.send(message, remoteIP, udpSendPort);

  println("MIDI NOTE PRESSED: " + pitch);
  println("------------------------");
  println();
}