void saveJSON (String fileName) {
  JSONObject json = new JSONObject();

  json.setString("Remote IP", "192.168.0.100");
  
  json.setString("MIDI Input", "APC MINI");
  json.setString("MIDI Output", "Bus 1");
  json.setInt("DMX Slider", 55);
  
  json.setString("DMX Port", "/dev/tty.usbserial-6AVH1FM6");
  json.setInt("DMX Universe Size", 2);
  json.setInt("DMX Channel", 1);
  json.setInt("Baud Rate", 115200);
  
  json.setInt("UDP Send Port", 8888);
  json.setInt("UDP Listen Port", 8888);
  
  saveJSONObject(json, "data/" + fileName);
}

void loadJSON(String fileName) {
  JSONObject json = loadJSONObject(fileName);

  remoteIP = json.getString("Remote IP");
  
  midi_IN = json.getString("MIDI Input");
  midi_OUT = json.getString("MIDI Output");
  dmxSlider = json.getInt("DMX Slider");
  
  udpSendPort = json.getInt("UDP Send Port");
  udpListenPort = json.getInt("UDP Listen Port");
  
  dmxPort = json.getString("DMX Port");
  baudRate = json.getInt("Baud Rate");
  universeSize = json.getInt("DMX Universe Size");
  dmxChannel = json.getInt("DMX Channel");
}