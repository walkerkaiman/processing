void init () {

  // Set display parameters
  stroke(white);
  strokeWeight(5);
  fill(white);

  //saveJSON("config.json");
  loadJSON("config.json");



  /*
  ------------------------
   INITIALIZE MIDI
  ------------------------
  */

  List<String> availableMIDI = Arrays.asList(midi.availableInputs());

  if (!midi_IN.equals("-1") && !availableMIDI.contains(midi_IN)) { 
    println();
    println("Configured MIDI device not found.");
    println("Connect device or reconfigure JSON file...dumbass");

    println();
    println("Available MIDI Input Devices:");
    printArray(midi.availableInputs());
    println();

    exit();
    return;
  }
  
  
  // Set up computer as a MASTER or SLAVE
  if (midi_IN.equals("-1")) { 
    midi = new MidiBus(this, int(midi_IN), midi_OUT);
    isSlave = true;
  } else { 
    midi = new MidiBus(this, midi_IN, -1); 
    isSlave = false;
  }








  /*
  ------------------------
   INITIALIZE DMX
  ------------------------
  */



  if (isSlave) { // If computer configured as SLAVE, set up USB DMX Pro.
    List<String> availableSerial = Arrays.asList(Serial.list());

    if (availableSerial.contains(dmxPort)) { // If USB DMX is found, initialize.
      dmx = new DMX(this, dmxPort, baudRate, universeSize);
    } else { // If USB DMX is not found, report and quit the program.
      println();
      println("USB DMX Pro not found...");
      println("Connect USB DMX Pro and restart program.");
      println();
      println("Available Serial Devices:");
      printArray(Serial.list());
      
      exit();
      return;
    }
  }






  /*
  ------------------------
   INITIALIZE UDP
  ------------------------
  */

  udp = new UDP(this, udpListenPort);

  // If this computer is a SLAVE, listen for UDP messages.
  // If this computer is a MASTER, do not listen for UDP messages.
  udp.listen(isSlave); 

  // Report Initialization
  printConfig();
}