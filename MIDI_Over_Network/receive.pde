void receive(byte[] incoming) {
  String message = new String(incoming);

  // If the incoming message is for DMX
  if (isSlave && message.startsWith("DMX ")) {
    
    String[] parse = message.split(" ");
    String protocol = parse[0];
    int number = int(parse[1]);
    int value = int(parse[2]);
    
    if (number == dmxSlider && value > 10) {
      dmx.setDMXChannel(dmxChannel, value);
    }

    println(message);
    return;
  }
  
  
  else if (message.startsWith("MIDI ")) {
    String[] parse = message.split(" ");
    String protocol = parse[0];
    int pitch = int(parse[1]);
    
    midi.sendNoteOn(channel, pitch, on);
    println("MIDI note " + pitch + " sent to port: " + midi_OUT);
  }
  
}