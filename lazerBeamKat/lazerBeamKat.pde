float thickness = 100;
float colorRabbit;

void setup(){
  size(displayWidth-100, displayHeight-100);
  
}

void draw(){
  background(255);
  
  for(int lazerY = 0; lazerY < thickness; lazerY++){
    float c = noise(colorRabbit + lazerY) * 255;
    stroke(255, c, c);
    
    line(20, height/2-thickness+lazerY, width-20, height/2-thickness+lazerY);
    
    colorRabbit += .1;
  }
}
