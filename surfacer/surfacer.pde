import SimpleOpenNI.*;
import peasy.*;

SimpleOpenNI kinect;
PeasyCam cam;

int pixelStep = 200;
float min = 50;
float max = 600;

Point[] points;
PVector[] depthPoints;

void setup(){
  size(640, 480, P3D);
  
  cam = new PeasyCam(this, 1000);
  kinect = new SimpleOpenNI(this);
  points = new Point[width*height/pixelStep];
  
  for(int i = 0; i < points.length; i++){
    points[i] = new Point(new PVector(0, 0, 300), i);
  }
  
  if(kinect.isInit() == false){
    println("CAMERA NOT CONNECTED...(dumbass)"); 
    exit();
    return;
  }
  
  kinect.enableDepth();
  
  perspective(radians(45), float(width)/float(height), 10, 150000);
}

void draw(){
  background(255);
  
  kinect.update();
  
  rotateX(radians(180));
  translate(0, 0, -1000);
  
  depthPoints = kinect.depthMapRealWorld();
  
  //Maps and updates depth values to point instance positions
  for(int i = 0; i < depthPoints.length; i += pixelStep){
     PVector currentPoint = depthPoints[i];
     
     points[i/pixelStep].position = new PVector(currentPoint.x, currentPoint.y, currentPoint.z); 
  }
  
  for(int i = 0; i < depthPoints.length; i += pixelStep){
         PVector currentPoint = depthPoints[i];
         if(currentPoint.z > min && currentPoint.z < max)
         point(currentPoint.x, currentPoint.y, currentPoint.z);
  }

  for(Point current : points){
    current.run();
  }
}
