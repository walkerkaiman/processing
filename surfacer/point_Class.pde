class Point{
  int pointIndex;
  PVector position;
  boolean render = false;
  
  Point(PVector temp_pos, int temp_index){
    position = temp_pos;
    pointIndex = temp_index;
    stylePlane();
  }
  
  void run(){
    updateDist();
    
    if(render){
      //stylePoint();
      //displayPoint();
      displayPlane();
    }
  }
  
  void updateDist(){
    if(position.z > min && position.z < max && pointIndex < points.length-width-1 && pointIndex > 1){
      render = true;
    }
    else{
      render = false;
    }
  }
  
  void displayPoint(){
    point(position.x, position.y, position.z);
  }
  
  void displayPlane(){
    PVector a = points[pointIndex].position;
    PVector b = points[pointIndex+1].position;
    PVector c = points[pointIndex+width].position;
    PVector d = points[pointIndex+width+1].position;
    
    
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(b.x, b.y, b.z);
      vertex(c.x, c.y, c.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(b.x, b.y, b.z);
      vertex(d.x, d.y, d.z);
      vertex(c.x, c.y, c.z);
    endShape(CLOSE);
  }
  
  void stylePoint(){
    stroke(0);
    strokeWeight(5);
  }
  
  void stylePlane(){
    fill(232, 178, 0, 150);
    
    stroke(0, 50);
    strokeWeight(1);
    //noStroke();
  }
}
