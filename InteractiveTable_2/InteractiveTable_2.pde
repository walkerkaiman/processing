import codeanticode.syphon.*;
import processing.video.*;
import gab.opencv.*;
import java.awt.*;

Capture camera;
OpenCV opencv;
SyphonServer server;

PGraphics canvas;

void settings() {
  size(640, 480, P3D);
  PJOGL.profile=1;
}

void setup() {
  frameRate(100);
  String[] cameras = Capture.list();

  if (cameras == null) {
    println("Failed to retrieve the list of available cameras, will try the default...");
  } if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    printArray(cameras);
    
    camera = new Capture(this, width, height, "Microsoft LifeCam VX-800", 30);
  }
  
  
  opencv = new OpenCV(this, 640/2, 480/2);
  server = new SyphonServer(this, "Processing Server");
  canvas = createGraphics(width, height, P3D);

  canvas.beginDraw();
  canvas.noFill();
  canvas.stroke(0, 255, 0);
  canvas.strokeWeight(3);
  canvas.endDraw();

  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE); 
  camera.start();
}

void draw() {
  opencv.loadImage(camera);
  
  canvas.beginDraw();
    canvas.background(0);
    canvas.scale(2);
  
    //canvas.image(camera, 0, 0);
  
    Rectangle[] faces = opencv.detect();
  
    for (int i = 0; i < faces.length; i++) {
      canvas.rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
    }
    canvas.rotateZ(radians(180));
  canvas.endDraw();

  image(canvas, 0, 0);
  server.sendImage(canvas);
}

void captureEvent(Capture c) {
  c.read();
  
}