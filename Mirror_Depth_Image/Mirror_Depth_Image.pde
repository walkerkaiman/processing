import SimpleOpenNI.*;
import processing.opengl.*;
SimpleOpenNI kinect;

float zoom;
color c;

void setup(){
  
  size(1024, 768, OPENGL);
  stroke(255);
  
  kinect.enableDepth(); 
  kinect = new SimpleOpenNI(this);
}

void draw(){
  
  kinect.update();
  PVector[] depthPoints = kinect.depthMapRealWorld();

  background(0);

  for(int i = 0; i < depthPoints.length; i += 3){
    
     PVector currentPoint = depthPoints[i];
    
     if(currentPoint.z > 200 && currentPoint.z < 2000){
         point(currentPoint.x, currentPoint.y, currentPoint.z); 
    }
  } 
}

