ArrayList <Planet> planetList = new ArrayList<Planet>();
ArrayList <Blackhole> blackholeList = new ArrayList<Blackhole>();

int initialPop = 10;

void setup(){
  size(600, 600);
  
  for(int i = 0; i < initialPop; i++){
    planetList.add(new Planet(random(width), random(height)));
  }
  background(255);
}

void draw(){
  background(255);
  
   for(Blackhole current : blackholeList){
    if(current.ready){
      current.run();
    }
  }
  
  for(int planet = 0; planet < planetList.size(); planet++){
    Planet current = planetList.get(planet);
    
    for(int i = 0; i < blackholeList.size(); i++){
      PVector force = blackholeList.get(i).attract(current);
      current.applyForce(force);
    }
    
    planetList.get(planet).run();
  }
  
 temp_blackhole();
 textSize(32);
 fill(0, 150);
 text(planetList.size(), 20, height-20);
 
 if(frameCount < 900 && frameCount % 3 == 0){
  // saveFrame("/Users/Kman/Desktop/frames/frame-####.png");
 }
}

void temp_blackhole(){
  stroke(0, 150);
  fill(255);
  
   if(mousePressed && blackholeList.size() > 0){
      Blackhole current = blackholeList.get(blackholeList.size()-1);
      float temp_size = dist(current.position.x, current.position.y, mouseX, mouseY) * 2;
      
      ellipse(current.position.x, current.position.y, temp_size, temp_size);
  }
}

void mousePressed(){
  blackholeList.add(new Blackhole(mouseX, mouseY));
}

void mouseReleased(){
  Blackhole current = blackholeList.get(blackholeList.size()-1);
  float size = dist(current.position.x, current.position.y, mouseX, mouseY) * 2;
  
  current.size = size;
  current.ready = true;
}