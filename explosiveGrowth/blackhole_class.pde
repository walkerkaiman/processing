class Blackhole{
  PVector position;
  boolean ready;
  float mass, time, size, G;
  
  Blackhole(float temp_x, float temp_y){
   position = new PVector(temp_x, temp_y);
   G = 1;
   ready = false;
  }
  
  void run(){
    display();
  }
  
  PVector attract(Planet p){
    mass = size/100;
    PVector force = PVector.sub(position, p.position);
    float d = force.mag();
    
    d = constrain(d, 15, 25);
    force.normalize();
    
    float strength = (G * mass * p.mass) / (d*d);
    force.mult(strength);
    
    return force;
  }
  
  void display(){
    float thick = abs(cos(time)*3);
    strokeWeight(thick);
    stroke(0, 150);
    fill(255);
    
    ellipse(position.x, position.y, size, size);
     
    time += .02;
  }

}
