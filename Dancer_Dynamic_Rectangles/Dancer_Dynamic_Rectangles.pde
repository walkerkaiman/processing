import SimpleOpenNI.*;
SimpleOpenNI kinect;

float min = 1000;
float max = 2000;
float lerp = 0.01;

void setup(){
  size(640, 480);
  
  strokeWeight(3);
  noFill();
  //fill(255, 100);
  
  kinect = new SimpleOpenNI(this);
  kinect.setMirror(false);
  kinect.enableDepth();
  
  if(kinect.isInit() == false){
     println("// Plug In The KINECT //"); 
     exit();
     return;  
  }
}

void draw(){
  background(0);

  kinect.update();
  
  int[] depthMap = kinect.depthMap();
  
  float pilotX = kinect.depthWidth();
  float pilotY = kinect.depthHeight();
  float sizeX = 0;
  float sizeY = 0;
  
  // CYCLE THROUGH ARRAY OF KINECT DEPTH VALUES
  for(int y = 0; y < kinect.depthHeight(); y += 5){
    for(int x = 0; x < kinect.depthWidth(); x += 5){
      
      int index = x + (y * kinect.depthWidth() );
      
      // RESTRICTS VIEW BETWEEN min AND max UNITS AWAY FROM CAMERA...
      if(depthMap[index] > min && depthMap[index] < max){ 
        
        //REVERSE X VALUES
        int currentX = width - x - 1;
        
        // DETERMINES POSITION OF TOP LEFT CORNER
        if(currentX < pilotX){
          pilotX = lerp * pilotX + (1 - lerp) * currentX;
        }
        
        if(y < pilotY){
          pilotY = lerp * pilotY + (1 - lerp) * y;
        }
        
        // DETERMINES X AND Y VALUES FOR SIZE
        if(currentX > sizeX){
          sizeX = (lerp * sizeX + (1 - lerp) * currentX);
        }
        
        if(y > sizeY){
          sizeY = (lerp * sizeX + (1 - lerp) * y);
        }
        
        stroke(255, 0, 0);
        point(currentX, y);
      }
    }
  }
  
  rect(pilotX, pilotY, sizeX - pilotX, sizeY - pilotY);
}
