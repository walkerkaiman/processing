float posX;
float posY = 100;
float doorHeight = 20;
float doorWidth = 12;

void setup(){
  size(displayHeight-100, displayHeight-100);
  frameRate(30);
  posX = width/4;
  
  background(0);
  stroke(255);
  strokeWeight(1);
  noFill();
}

void draw(){
  
  rect(width/4-20, 100-doorHeight-10, width/2+40, height);
  
  for(float floor = 100 + doorHeight+5; floor < height; floor += doorHeight+10){
    line(width/4, floor, width * .75, floor);
  }
  
  pushMatrix();
    rect(posX, posY + 5, doorWidth, doorHeight);
    ellipse(posX+10, posY+15, 2, 2);
  popMatrix();
  
  posX += doorWidth + random(50);
  
  if(posX + doorWidth > width * .75){
    posX = width/4 + random(20);
    posY += doorHeight+10;
  }
  
  if(posY > height){
    background(0);
    posX = width/4;
    posY = 100;
  }
}
