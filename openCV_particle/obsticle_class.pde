class Obsticle {
  PVector pos, size;
  
  Obsticle(PVector temp_pos, PVector temp_size){
    pos = temp_pos;
    size = temp_size;
  }
  
  void display(){
    stroke(0, 255, 0);
    fill(255, 0, 0);
    
    rect(pos.x, pos.y, size.x, size.y);
  }
  
  
}