class Boid{
  PVector accel, vel, pos;
  float maxSpeed, maxForce, size;
  
  Boid(PVector temp_pos, float temp_size){
    pos = temp_pos;
    vel = new PVector();
    accel = new PVector();
    maxSpeed = 10;
    maxForce = .3;
    size = temp_size;
  }
  
  void run(){
    update();
    display();
  }
  
  void update()  {
    vel.add(accel);
    vel.limit(maxSpeed);
    pos.add(vel);
    accel.mult(0);
  }
  
  void applyForce(PVector force)  {
    accel.add(force);
  }
  
  void seperate(ArrayList<Obsticle> obsticles) {
    float desiredSeperation = size * 2;
    PVector sum = new PVector();
    int count = 0;
    
    for(Obsticle other : obsticles) {
      float d = PVector.dist(pos, other.pos);
      
      if((d > 0) && (d < desiredSeperation)) {
        PVector diff = PVector.sub(pos, other.pos);
        
        diff.normalize();
        diff.div(d);
        sum.add(diff);
        count++;
      }
    }
    
    if(count > 0)  {
      sum.setMag(maxSpeed);
      
      PVector steer = PVector.sub(sum, vel);
      steer.limit(maxForce);
      applyForce(steer);
    }
  }
  
  void display()  {
    stroke(255);
    strokeWeight(1);
    noFill();
    
    pushMatrix();
      translate(pos.x, pos.y);
      ellipse(0, 0, size, size);
    popMatrix();
  }
  
  void wrapAround() {
    if(pos.x < -size)  {
      pos.x = width+size;
    }
    if(pos.y < -size) {
      pos.y = height+size;
    }
    if(pos.x > width+size) {
      pos.x = -size;
    }
    if(pos.y > height+size) {
      pos.y = -size;
    }
  }
  
  
  
  
}