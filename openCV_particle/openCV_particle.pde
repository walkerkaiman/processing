import processing.video.*;
import gab.opencv.*;

Capture camera;
OpenCV opencv;

ArrayList<Boid> boids;
ArrayList<Obsticle> obsticles; 

void setup()  {
  size(640, 360);
  noFill();
  strokeWeight(2);
  
  initOpenCV();
  initBoid(100);
}

void draw()  {
  background(0);
  
  scale(2);
  image(camera, 0, 0);
  
  if(camera.available())  {
    camera.read();
    opencv.loadImage(camera);
  }
  
  if(obsticles != null){
    for(Obsticle current : obsticles)  {
      current.display();
    }
  }
  
  for(Boid current : boids)  {
    current.run();
  }

}












void initBoid(int population)  {
  boids = new ArrayList<Boid>();
  float BoidSize = 10;
  
  for(int i = 0; i < population; i++)  {
    PVector BoidPos = new PVector(random(50), random(50), random(50));
   
    boids.add(new Boid(BoidPos, BoidSize));
  }
}

void initOpenCV(){
  String[] cameras = Capture.list();
  
  for(int i = 0; i < cameras.length; i++)  {
    println(cameras[i]);
  }
  
  opencv = new OpenCV(this, width/2, height/2);
  camera = new Capture(this, width/2, height/2, 30);
  camera.start();
}