void screenSize() {
  if (fullscreen) {
    surface.setSize(displayWidth, displayHeight);
  }else {
    surface.setSize(640, 480);
  }
}