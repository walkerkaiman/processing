import processing.video.*;
import controlP5.*;
import java.io.*;

Movie clip;

float logoDuration = 60;
float clipDuration = 15;
float fadeSpeed = 3;
boolean fullscreen, fading;
float clipTimer, logoTimer, alpha;

void setup() {
  size(640, 480);
  
  surface.setResizable(true);
  loadClip(chooseClip());
  
  fullscreen = true;
  fading = false;
  clipTimer = 0;
  logoTimer = 0;
}

void draw() {
  timer();
  set(0, 0, clip);
  // logo will go here
  fadeClip(); // Must go at bottom of draw
}