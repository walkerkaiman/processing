String chooseClip() {
  File media = new File(dataPath(""));
  String[] clipNames = media.list();
  String clipName = "";
  
  if (clipNames == null) {
    println("Data folder contains no files.");
  }
  else {
    int randomIndex = int(random(clipNames.length));
    clipName = clipNames[randomIndex];
  }
  
  return clipName;
}