void loadClip(String file) {
  if (file.endsWith(".mov")) {
    clip = new Movie(this, file);
  }else{
    loadClip(chooseClip());
  }
  
  clip.loop();
  clip.play();
}