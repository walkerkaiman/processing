void fadeClip() {
  
  if (fading) { 
    if(alpha < 255) {
      alpha += fadeSpeed;
    }else{
      fading = false;
      loadClip(chooseClip());
    }
  }else {
    alpha -= fadeSpeed;
  }
  
  alpha = constrain(alpha, 0, 255);
  
  noStroke();
  fill(0, alpha);
  rect(0, 0, width, height);
}