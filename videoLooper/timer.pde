void timer () {
  clipTimer++;
  logoTimer++;
  
  // Action once the clip timer runs over.
  if (clipTimer > clipDuration * frameRate){
    fading = true;
    clipTimer = 0;
    report();
  }
  
  // Action once the logo timer runs over.
  if (logoTimer > logoDuration * frameRate) {
    
    logoTimer = 0;
  }
}