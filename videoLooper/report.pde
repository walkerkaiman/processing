void report () {
  println("////////");
  println("Clip hold time: " + clipDuration);
  println("Clip hold timer: " + clipTimer/frameRate);
  println("Fade Speed: " + fadeSpeed);
  println("In fullscreen mode: " + fullscreen);
  println("\\\\\\\\\\\\\\\\");
  println();
}