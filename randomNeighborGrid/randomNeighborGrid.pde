ArrayList<Vert> verts = new ArrayList<Vert>();
int step = 30;
int perCol, perRow;

void setup() {
  perCol = int(width/step);
  perRow = int(height/step);

  size(640, 480);
  frameRate(16);
  stroke(255);
  noFill();
  strokeWeight(1);

  for (int x = -100; x <= width+100; x += step) {
    for (int y = -100; y <= height+100; y += step) {
      verts.add(new Vert(x, y));
    }
  }
}

void draw() {
  background(0);
  PVector[] neighbors = new PVector[4];
  
  for (int i = 0; i < verts.size(); i++) {
    Vert currentVert = verts.get(i);
    PVector currentPos = currentVert.position;

    if (currentPos.x > 0 && currentPos.x < width && currentPos.y > 0 && currentPos.y < height) {
      neighbors[0] = verts.get(i-1).position;
      neighbors[1] = verts.get(i+1).position;
      neighbors[2] = verts.get(i+perCol+2).position;
      neighbors[3] = verts.get(i-perCol-2).position;

      for (PVector n : neighbors) {
        if (random(1) > .97) {
          currentVert.displayLine(n);
        }
      }
    }
  }
  //saveFrame("Users/kaimanwalker/Desktop/frames/frame_###.png");
}