class Vert{
  PVector position;
  
  Vert(int _x, int _y){
    position = new PVector(_x, _y);
  }
  
  void update(){
    
  }
  
  void displayEllipse(float size){
    ellipse(position.x, position.y, size, size);
  }
  
  void displayLine(PVector other){
    line(position.x, position.y, other.x, other.y);
  }
}