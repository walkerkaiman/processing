class Tracker{
  PVector[] positions;
  PVector currentPos;
  boolean recording;
  int index;
  
  Tracker(int temp_index){
    positions = new PVector[frames];
    recording = true;
    index = temp_index;
    
    for(int i = 0; i < positions.length; i++){
      positions[i] = new PVector(-1000, -1000);
    }
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    
    if(recording){
      positions[currentFrame].x = mouseX;
      positions[currentFrame].y = mouseY;
      
      println("Recording: "+ index);
      println("X: "+positions[currentFrame].x);
      println("Y: "+positions[currentFrame].y);
    }
   
  }
  
  void display(){
    noFill();
    stroke(255, 150);
    strokeWeight(2);
    
    currentPos = positions[currentFrame];

    ellipse(currentPos.x, currentPos.y, 20, 20);
  }
  
}

