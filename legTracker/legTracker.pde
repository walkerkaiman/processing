import processing.video.*;
Movie movie;

ArrayList<Tracker> trackers = new ArrayList<Tracker>();
int frames, currentFrame;
boolean render = false;
int rate = 24;

void setup(){
  size(600, 600);
  frameRate(rate);
  
  movie = new Movie(this, "1.mov");
  movie.loop();
  movie.volume(0);
  
  frames = int(movie.duration() * rate)+1;
  background(0);
}

void draw(){
  
  if(movie.available()){
    movie.read(); 
  }
  
  currentFrame = int(movie.time() * rate);
  
  image(movie, 0, 0, width, height);
  
  for(Tracker current : trackers){
    current.run();
  }
  
  stroke(255, 0, 0, 100);
  strokeWeight(2);
  
  for(Tracker current : trackers){
    if(trackers.size() > 1){
      for(Tracker neighbor : trackers){
        float distance = dist(current.currentPos.x, current.currentPos.y, neighbor.currentPos.x, neighbor.currentPos.y);
        
        if(distance > 0 && distance < 1400){
          line(current.currentPos.x, current.currentPos.y, neighbor.currentPos.x, neighbor.currentPos.y);
        }
      }
    }
  }
  
  if(render){
    println("Rendering...");
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.tiff");
  }else{println("Not Rendering");}
}

void mousePressed(){
  trackers.add(new Tracker(trackers.size()));
}

void keyPressed(){
  if(key == BACKSPACE){
    trackers.remove(trackers.size()-1);
  }
  
  switch(key){
    case 'q':
      if(trackers.size() > 0){
        trackers.get(0).recording =! trackers.get(0).recording;
      }
      break;
    case 'w':
      if(trackers.size() > 1){
        trackers.get(1).recording =! trackers.get(1).recording;
      }
      break;
    case 'e':
      if(trackers.size() > 2){
        trackers.get(2).recording =! trackers.get(2).recording;
      }
      break;
    case 'r':
      if(trackers.size() > 3){
        trackers.get(3).recording =! trackers.get(3).recording;
      }
      break;
    case 't':
      if(trackers.size() > 4){
        trackers.get(4).recording =! trackers.get(4).recording;
      }
      break;
    case 'y':
      if(trackers.size() > 5){
        trackers.get(5).recording =! trackers.get(5).recording;
      }
      break;
    case 'u':
      if(trackers.size() > 6){
        trackers.get(6).recording =! trackers.get(6).recording;
      }
      break;
    case 'i':
      if(trackers.size() > 7){
        trackers.get(7).recording =! trackers.get(7).recording;
      }
      break;
    case 'o':
      if(trackers.size() > 8){
        trackers.get(8).recording =! trackers.get(8).recording;
      }
      break;
    case 'p':
      if(trackers.size() > 9){
        trackers.get(9).recording =! trackers.get(9).recording;
      }
      break;
    case 'm':
      render =! render;
      break;
  }

}
