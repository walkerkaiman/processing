final color whiteTrans = color(255, 150);
final color white = color(255);
final color black = color(0);
final color red = color(255, 0, 0);
final float speed = .05;
final float brushSize = 2; // Thickness of line.
final float angStep = 20; // How large the slices are.
final float resolution = 5; // How detailed the line is.

ArrayList<PVector> brushStroke = new ArrayList<PVector>();

void setup () {
  size(640, 480);
  stroke(white);
  noFill();
  strokeWeight(brushSize);
  
  brushStroke.add(new PVector()); // Make a draw point in the center
}

void draw() {
  background(black);
  
  ellipse(mouseX, mouseY, brushSize * 3, brushSize * 3); // Brush Location

  pushMatrix();
    translate(width/2, height/2); // Make new coordinate system zero'ed at center.
    
    ellipse(0, 0, 20, 20); // Center indicator
    
    for (int slice = 0; slice < 360/angStep; slice++) { // Repeat per slice
      rotate(radians(angStep));
      
      for (int i = 1; i < brushStroke.size(); i++) { // Draw all brush points
        PVector current = brushStroke.get(i);
        PVector previous = brushStroke.get(i-1);
        
        line(current.x, current.y, previous.x, previous.y); // Make a line from current to previous point.
        
        current.y -= speed;
        
        if (dist(current.x, current.y, 0, 0) > width/2) {
          brushStroke.remove(i);
        }  
      }
    }
  popMatrix();
}

void mouseDragged() {
  PVector last = brushStroke.get(brushStroke.size() - 1);
  PVector current = new PVector(mouseX - width/2, mouseY - height/2);
  float delta = dist(current.x, current.y, last.x, last.y);
  
  if (delta > resolution) { // Only makes a new point when the distance from the current to the last is greater than resolution.
    brushStroke.add(new PVector(current.x, current.y));
  }
}

void keyPressed() { 
  if (key == 'r' || key == 'R') { // Press the 'r' key to reset.
    brushStroke = new ArrayList<PVector>();
    brushStroke.add(new PVector());
  }
  else if (key == 's' || key == 'S') { // Press the 's' key to save and image.
   saveFrame("data/pattern_####.png"); 
   background(white);
  }
}