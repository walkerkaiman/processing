import processing.video.*;
Movie video;

ArrayList<PImage> frames;
ArrayList<Tracker> trackers;

PImage diff_Img;
int threshold, performance;

void setup(){
  size(1280, 720);
  frameRate(24);
  background(0);
  
  video = new Movie(this, "2.mov");
  video.play();
  video.volume(0);
  video.loop();
  
  frames = new ArrayList<PImage>();
  trackers = new ArrayList<Tracker>();
  
  diff_Img = createImage(width, height, RGB);
  
  threshold = 30;
  performance = 5;
}

void draw(){
  background(0);
  
  if(video.available()){
    video.read();
    updateFrame();
    compareFrames();
    updateTrackers();
  }
  
  for(Tracker current : trackers){
    current.displayPixel();
  }
}

void updateFrame(){
  PImage currentFrame = createImage(video.width, video.height, RGB);
  
  video.loadPixels();
  arrayCopy(video.pixels, currentFrame.pixels);
  frames.add(currentFrame);
  
  if(frames.size() > 2){
    frames.remove(0);
  }
}

void compareFrames(){
  if(frames.size() == 2){
    PImage currentFrame = frames.get(1);
    PImage lastFrame = frames.get(0);
    int movementSum = 0;
    
    currentFrame.loadPixels();
    lastFrame.loadPixels();
    diff_Img.loadPixels();
    
    for(int i = 0; i < diff_Img.width * diff_Img.height; i++){
      color currentPixel = currentFrame.pixels[i];
      color lastPixel = lastFrame.pixels[i];
      
      float currentRed = red(currentPixel);
      float currentGreen = green(currentPixel);
      float currentBlue = blue(currentPixel);
      
      float lastRed = red(lastPixel);
      float lastGreen = green(lastPixel);
      float lastBlue = blue(lastPixel);
      
      float diffRed = abs(currentRed - lastRed);
      float diffGreen = abs(currentGreen - lastGreen);
      float diffBlue = abs(currentBlue - lastBlue);
      
      movementSum += diffRed + diffGreen + diffRed;
      
      diff_Img.pixels[i] = color(diffRed, diffGreen, diffBlue);
    }
    
    if(movementSum > 0){
      diff_Img.updatePixels();
    }
  }
}

void updateTrackers(){
  trackers = new ArrayList<Tracker>();
  
  diff_Img.loadPixels();
  video.loadPixels();
  
  for(int y = 0; y < height; y += performance){
    for(int x = 0; x < width; x += performance){
      float delta = brightness(diff_Img.pixels[x+y*width]);
      color pixelColor = video.pixels[x+y*width];
      
      if(delta > threshold){
        trackers.add(new Tracker(x, y, delta, pixelColor));
      }
    }
  }
}