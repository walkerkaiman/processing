class Tracker{
  PVector pos;
  float intensity;
  color fill;
  
  Tracker(float temp_x, float temp_y, float temp_intensity, color temp_fill){
    pos = new PVector(temp_x, temp_y);
    intensity = temp_intensity;
    fill = temp_fill;
  }
  
  void displayPixel(){
    noStroke();
    fill(fill);
    
    rect(pos.x, pos.y, performance, performance);
  }
}