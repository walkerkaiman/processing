import peasy.*;
PeasyCam cam;

ArrayList<Block> blocks = new ArrayList<Block>();

float time;
boolean recording = false;

int levels = 20;
int div = 30;
int equator = 50;

void setup(){
  size(600, 600, P3D);
  
  fill(0);
  stroke(255);
  strokeWeight(2);
  
  cam = new PeasyCam(this, 1000);
  
  float index = 0;
  float angStep = radians(360/div);
  
  for(int level = levels; level > 0; level--){
    float levelFactor = map(level, 0, levels, .1, 2);
    float girth = equator*levelFactor;
    
    float step_x = cos(angStep)*girth;
    float step_y = sin(angStep)*girth;
    float prev_x = cos(0)*girth;
    float prev_y = sin(0)*girth;
  
    float size = dist(step_x, step_y, prev_x, prev_y);
    
    for(float angle = 0; angle < 2*PI; angle += angStep){
      blocks.add(new Block(index, level, angle, size, girth));
      index += angStep;
    }
  }
  
  println("Blocks: "+blocks.size());
}

void draw(){
  background(0);
  
  for(Block current : blocks){
    current.run();
  }
  
  time += .05;
  
  record();
}

void record(){
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.tiff");
  }
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
    break;
  }
}