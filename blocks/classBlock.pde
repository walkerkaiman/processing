class Block{
  int level;
  float index, size, girth, angle, stretch;
  
  Block(float temp_index, int temp_level, float temp_angle, float temp_size, float temp_girth){
    index = temp_index;
    level = temp_level;
    size = temp_size;
    angle = temp_angle;
    girth = temp_girth;
  }
  
  void run(){
    updateLength();
    display();
  }
  
  void updateLength(){
    stretch = abs(cos(time+index+level/10)*50);
  }
  
  void display(){
    pushMatrix();
      rotate(angle);
      translate(0, girth+stretch, level*size);
      
      box(size, stretch, size);
    popMatrix();
  }
  
}