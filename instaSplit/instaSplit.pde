/*
INSTAGRAM IMAGE SPLITTER
Splits up a single image into multiple images
to be displayed on a grid such as Instagram.

Put an image on your desktop and name it "insta.jpg".
Run the program.
A directory with your images will be on the Desktop.
The images are named in sequence that they should be posted.

Only works on OS X. Will need to adapt file paths if used on 
a different OS.
*/

final String originalFile_name = "insta.jpg";
final String outputFile_extension = ".png";
PImage pic;
    
void settings () {
  final String originalFilePath = "/Users/"+user+"/Desktop/"+originalFile_name;
  pic = loadImage(originalFilePath); 
  size(pic.width, pic.height);
}

void setup () {
  
}

void draw () {
  noLoop();
  
  try {
    //pic.resize(width, height);
    image(pic, 0, 0, width, height);
    final int cols = 3; // Do not change if using for Instagram.
    final int rows = 3; // Adapt if original image's ratio distorts on the grid.
    final int xSize = int(width/cols);
    final int ySize = int(height/rows);
    
    int i = cols*rows;
    
    for (int y = 0; y < rows; y++) {  
      for (int x = 0; x < cols; x++) {
        int xPos = x * xSize;
        int yPos = y * ySize;
        PImage crop = pic.get(xPos, yPos, xSize, ySize);
        String fileName = str(i)+outputFile_extension;
        String filePath = "/Users/"+user+"/Desktop/"+user+"'s Instagram Splitter/"+fileName;
        crop.save(filePath);
        i--;
      }
    }
  }
  catch (NullPointerException e) {
    exit();
  }
  exit();
}