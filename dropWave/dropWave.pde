import peasy.*;
PeasyCam cam;

float time;

void setup () {
  size(600, 600, P3D);
  stroke(255, 200);
  strokeWeight(2);
  noFill();
  
  cam = new PeasyCam(this, 1000);
}

void draw() { 
  background(0);
  
  for (int i = 0; i < 50; i++) {
    float radius = i * 10;
    float wave = cos(time + radius/50) * 30;
    PVector pos = new PVector (0, 0, wave);
    
    pushMatrix();
      //scale(radius);
      translate(pos.x, pos.y, pos.z);
      //rotateX(wave/100);
      
      ellipse (0, 0, radius*2, radius*2);
    popMatrix();
  }
  
  time += .05;
}