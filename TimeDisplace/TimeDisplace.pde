import processing.video.*;

Capture video;
ArrayList<PImage> frames = new ArrayList<PImage>();
int barHeight;

void setup() {
  size(640, 480);
  frameRate(24);

  video = new Capture(this, width, height);
  video.start();  
  
  barHeight = 8;
}

void draw() {
 int currentImage = 0;
 
 loadPixels();
  
  for(int y = 0; y < video.height; y += barHeight){
    if(currentImage < frames.size()){
      PImage img = frames.get(currentImage);
      
      if(img != null){
        img.loadPixels();

        for(int x = 0; x < video.width; x++){
          for(int i = 0; i < barHeight; i++){
            pixels[x + (y+i) * width] = img.pixels[x + (y+i) * video.width];
          }
        }  
      }
      currentImage++;
    } 
  }
  updatePixels();
}

void captureEvent(Capture camera) {
  PImage img = createImage(width, height, RGB);
  
  camera.read();
  video.loadPixels();
  arrayCopy(video.pixels, img.pixels);
  
  frames.add(img);
  
  if(frames.size() > height/barHeight){
    frames.remove(0);
  }
}