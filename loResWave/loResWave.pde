import peasy.*;
PeasyCam cam;

float[] peakHeight = new float[1000];
float rabbit;

void setup(){
  size(displayWidth-100, displayHeight-100, P3D);
  cam = new PeasyCam(this, 1000);
}

void draw(){
  smooth(4);
  background(0);
  
  translate(-width/2, -height/2, 0);
  rotateX(radians(15));
  //rotateY(rabbit);
  
  calc_Peak();
  draw_Waves();
 
 rabbit += .09;
 println(rabbit);
}



void calc_Peak(){
  int indexPeak = 0;
  
  for(int x = 0; x < sqrt(peakHeight.length); x++){
    for(int y = 0; y < sqrt(peakHeight.length); y++){
      
      float baseHeight = cos(rabbit+x*5)*100;
      float offset = noise(10*cos(x)+20*sin(y)+rabbit)*100;
      
      if(indexPeak < peakHeight.length){
        peakHeight[indexPeak] = baseHeight;
        indexPeak ++;
      }
    }
  }
}
  
void draw_Waves(){
  strokeWeight(5);
  stroke(255);
  noFill();
  float Height = 100;
  
 for(int indexX = 0; indexX < int(sqrt(peakHeight.length)); indexX++){
   beginShape();
     for(int indexZ = 0; indexZ < int(sqrt(peakHeight.length)); indexZ++){
       vertex(indexX*Height, peakHeight[indexX*int(sqrt(peakHeight.length))+indexZ], indexZ*-Height);
     }
   endShape();
 }
 
 for(int indexZ = 0; indexZ < int(sqrt(peakHeight.length)); indexZ++){
   beginShape();
     for(int indexX = 0; indexX < int(sqrt(peakHeight.length)); indexX++){
       vertex(indexX*Height, peakHeight[indexX*int(sqrt(peakHeight.length))+indexZ], indexZ*-Height);
     }
   endShape();
 }
}


void drawCompass(){
  strokeWeight(5);
  
  // Y Axis
  stroke(255, 0, 0);
  beginShape(LINES);
    vertex(0, 100, 0);
    vertex(0, 0, 0);
  endShape();
  
   // X Axis
  stroke(0, 255, 0);
  beginShape(LINES);
    vertex(100, 0, 0);
    vertex(0, 0, 0);
  endShape();
  
   // Z Axis
  stroke(0, 0, 255);
  beginShape(LINES);
    vertex(0, 0, -100);
    vertex(0, 0, 0);
  endShape();
}

