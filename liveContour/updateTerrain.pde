PImage updateTerrain () {
  PImage terrain = new PImage(w, h);

  terrain.loadPixels();

  for (int i = 0; i < wh; i++) {
    float x = i / w;
    float y = i % w;
    int colorValue = int(noise(x/10, y/10 + time) * 255);

    terrain.pixels[i] = 255 << 24 | colorValue << 16 | colorValue << 8 | colorValue;
  }

  fastBlur(terrain);

  return terrain;
}