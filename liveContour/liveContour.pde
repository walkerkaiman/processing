import blobDetection.*;
import peasy.*;

PeasyCam cam;

// User changable parameters
final String framePath = dataPath("frames/frame_#####.png");
final float speed = .1;
final int w = 80;
final int h = 60;
final float levels = 20;
final float factor = 20;
final float elevation = 500;
final int blurRadius = 1;

// Do not change the following
final color white = 255 << 24 | 255 << 16 | 255 << 8 | 255;
final color black = 255 << 24 | 0 << 16 | 0 << 8 | 0;
final int wm = w - 1;
final int hm = h - 1;
final int wh = w * h;
final int div = 2 * blurRadius + 1;
final int r[] = new int[wh];
final int g[] = new int[wh];
final int b[] = new int[wh];
final int vmin[] = new int[max(w, h)];
final int vmax[] = new int[max(w, h)];
final int dvSize = 256 * div;
final int dv[] = new int[dvSize];

// Dynamic variables
BlobDetection theBlobDetection[] = new BlobDetection[int(levels)];
float time;
boolean recording;

void setup() {
  cam = new PeasyCam(this, 100);
  recording = false;

  size(600, 600, P3D);  
  stroke(white);
  fill(black);
}

void draw() { 
  time += speed;
  background(black);

  computeBlobs(updateTerrain());
  drawLevels();

  if (recording) {
    saveFrame(dataPath("frames/frame_#####.png"));
  }
}

void keyPressed () {
  switch(key) {
  case 'r':
  case 'R':
    recording =! recording;
    println("Recording: " + recording);
    break;
  }
}