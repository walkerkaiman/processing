void computeBlobs (PImage frame) {

  for (int i = 0; i < levels; i++) {
    theBlobDetection[i] = new BlobDetection(w, h);
    theBlobDetection[i].setThreshold(i / levels);
    theBlobDetection[i].computeBlobs(frame.pixels);
  }
}