void drawLevels() {
  for (int i = 0; i < levels; i++) {
    color c = i % 2 == 0 ? black : white;
    translate(0, 0, -elevation/levels);  
    
    fill(c);
    drawContours(i);
  }
}