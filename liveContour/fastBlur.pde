void fastBlur(PImage img) {
  
  if (blurRadius < 1) {
    return;
  }

  int rsum, gsum, bsum, x, y, i, p, p1, p2, yp, yi, yw;
  int[] pix = img.pixels;

  for (i = 0; i < dvSize; i++) {
    dv[i] = i / div;
  }

  yw = yi = 0;

  for (y = 0; y < h; y++) {
    rsum = gsum = bsum = 0;

    for (i = -blurRadius; i <= blurRadius; i++) {
      p = pix[yi + min(wm, max(i, 0))];
      rsum += (p & 0xff0000) >> 16;
      gsum += (p & 0x00ff00) >> 8;
      bsum += p & 0x0000ff;
    }

    for (x = 0; x < w; x++) {
      r[yi] = dv[rsum];
      g[yi] = dv[gsum];
      b[yi] = dv[bsum];

      if (y == 0) {
        vmin[x] = min(x + blurRadius + 1, wm);
        vmax[x] = max(x - blurRadius, 0);
      }

      p1 = pix[yw + vmin[x]];
      p2 = pix[yw + vmax[x]];

      rsum += ((p1 & 0xff0000) - (p2 & 0xff0000)) >> 16;
      gsum += ((p1 & 0x00ff00) - (p2 & 0x00ff00)) >> 8;
      bsum += (p1 & 0x0000ff) - (p2 & 0x0000ff);
      
      yi++;
    }

    yw += w;
  }

  for (x = 0; x < w; x++) {
    rsum = gsum = bsum = 0;
    yp = -blurRadius * w;

    for (i = -blurRadius; i <= blurRadius; i++) {
      yi = max(0, yp) + x;
      rsum += r[yi];
      gsum += g[yi];
      bsum += b[yi];
      
      yp += w;
    }

    yi = x;

    for (y = 0; y < h; y++) {
      pix[yi] = 0xff000000 | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

      if (x == 0) {
        vmin[y] = min(y + blurRadius + 1, hm) * w;
        vmax[y] = max(y - blurRadius, 0) * w;
      }

      p1 = x + vmin[y];
      p2 = x + vmax[y];

      rsum += r[p1] - r[p2];
      gsum += g[p1] - g[p2];
      bsum += b[p1] - b[p2];

      yi += w;
    }
  }
}