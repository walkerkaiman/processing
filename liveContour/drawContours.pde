void drawContours(int i) {

  for (int n = 0; n < theBlobDetection[i].getBlobNb(); n++) {
    Blob b = theBlobDetection[i].getBlob(n);

    if (b != null) {
      beginShape();
      int numberOfVertex = b.getEdgeNb();
      
      for (int m = 0; m < numberOfVertex; m++) {
        EdgeVertex vert = b.getEdgeVertexA(m);

        if (vert != null) {
          float x = vert.x * w * factor;
          float y = vert.y * h * factor;
          
          vertex(x, y);
        }
      }
      endShape(CLOSE);
    }
  }
}