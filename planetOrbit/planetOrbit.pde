import peasy.*;
PeasyCam cam;

ArrayList<Planet>planets = new ArrayList<Planet>();

int generations = 2;
int incr = 30;

void setup(){
  size(400, 400, P3D);
  cam = new PeasyCam(this, 1000);
  
  planets.add(new Planet(0, 0, generations, generations*incr));
  
  noFill();
  stroke(255);
  sphereDetail(10);
}

void draw(){
  background(0);
  
  for(Planet current : planets){
    current.run(); 
  }
  
}