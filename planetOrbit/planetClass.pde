class Planet{
  Planet parent;
  PVector pos;
  int gen, size;
  float angle, speed;
  
  Planet(Planet temp_parent, float x, float y, int temp_gen, int temp_size){
    parent = temp_parent;
    pos = new PVector(x, y);
    gen = temp_gen;
    size = temp_size;
    speed = random(.01, .05);
    
    if(gen > 0){
      spawn();
    }
  }
  
  void run(){
    if(gen > 0){
      update();
    }
    display();
  }
  
  void update(){
    pos.x = cos(angle) * size + parent.pos.x + parent.size;
    pos.y = sin(angle) * size + parent.pos.y + parent.size;
    
    angle += speed;
  }
  
  void display(){
    
    pushMatrix();
      translate(pos.x, pos.y);
      rotate(angle);
      
      sphere(size);
    popMatrix();
  }
  
  void spawn(){
    Planet parent = planets.get(0);
    
    int child = gen - 1;
    int size = gen * incr;
    
    PVector parentPos = pos;
    float x = cos(angle) * (size+child*incr) + parentPos.x;
    float y = sin(angle) * (size+child*incr) + parentPos.y;
    
    planets.add(new Planet(parent, x, y, child, size));
  }
  
}