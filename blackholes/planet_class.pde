class Planet{
  PVector position, velocity, acceleration;
  float mass;
  boolean inside, preInside;
  
  Planet(float temp_x, float temp_y){
    position = new PVector(temp_x, temp_y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(random(-5, 5), random(-5, 0));
    mass = 10;
    inside = false;
    preInside = false;
  }

  void run(){
    update();
    checkCollision();
    checkLeaves();
    style();
    display();
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(5);
    position.add(velocity);
    acceleration.mult(0);
    
    preInside = inside;
  }
  
  void display(){
    float theta = velocity.heading2D() + radians(90);
    
    pushMatrix();
      translate(position.x, position.y);
      rotate(theta);
      
      ellipse(0, 0, 5, 5);
      line(0, 0, 0, -5);
    popMatrix();
  }
  
  void applyForce(PVector force){
    acceleration.add(force);
  }
  
  void checkCollision(){
    inside = false;
    
    for(int i = 0; i < blackholeList.size(); i++){
      Blackhole currentBlackhole = blackholeList.get(i);
      float distance = dist(position.x, position.y, currentBlackhole.position.x, currentBlackhole.position.y);
      
      if(distance < currentBlackhole.size/2){
        inside = true;
      }
      
    }
  }
  
  void checkLeaves(){
    if(inside && preInside != inside){
      planetList.add(new Planet(position.x, position.y));
    }
  }
  
  void style(){
    if(inside){
      strokeWeight(1);
      stroke(255, 0, 0);
      //fill();
      noFill();
      //noStroke();
    }
    else{
      strokeWeight(1);
      stroke(0);
      //fill();
      noFill();
      //noStroke();
    }
  }
  
}
