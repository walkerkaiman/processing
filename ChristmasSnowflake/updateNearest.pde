void updateNearest() {
  int[] rawDepth = kinect.getRawDepth();
  float nearestDepth = Integer.MAX_VALUE;
  
  depthImg.loadPixels();
  
  for (int y = 0; y < height; y ++) {
    for (int x = 0; x < width; x ++) {
      int pixelIndex = x + y * width;
      int currentDepth = rawDepth[pixelIndex];
      
      if (currentDepth >= minDepth && currentDepth <= maxDepth) { // Tracked Pixels
        if (currentDepth < nearestDepth) {
          nearestDepth = currentDepth;
          nearestPixel = new PVector(x, y);
        }
      
      depthImg.pixels[pixelIndex] = white;
      } 
      else { // Filtered Out Pixels
        depthImg.pixels[pixelIndex] = black;
      }
    }
  }
  depthImg.updatePixels();
  
  drawing = (nearestDepth > minDepth && nearestDepth < maxDepth) ? true : false;
}