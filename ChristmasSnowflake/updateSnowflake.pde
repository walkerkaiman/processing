void updateSnowflake () {

  canvas.background(black);
  canvas.stroke(white);
  canvas.strokeWeight(brushSize);
  
  canvas.pushMatrix();
    canvas.translate(width/2, height/2);
    
    for (int slice = 0; slice < slices; slice ++) {
      canvas.rotate(radians(angStep));
      
      for (int i = 1; i < brushStroke.size(); i++) {
        PVector current = brushStroke.get(i);
        PVector previous = brushStroke.get(i-1);
        
        canvas.line(current.x, current.y, previous.x, previous.y);
        
        current.y -= speed;
        
        if (dist(current.x, current.y, 0, 0) > width/2) {
          brushStroke.remove(i);
        }
      }
    }
  canvas.popMatrix();

}