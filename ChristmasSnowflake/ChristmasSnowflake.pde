import org.openkinect.freenect.*;
import org.openkinect.processing.*;
import codeanticode.syphon.*;

Kinect kinect;
SyphonServer server;

PGraphics canvas;
PImage depthImg;
PVector nearestPixel;
boolean drawing;
ArrayList<PVector> brushStroke = new ArrayList<PVector>();

final int minDepth = 600;
final int maxDepth = 900;
final float speed = .1;
final int brushSize = 4;
final float angStep = 45;
final float resolution = 15;
final float slices = 360/angStep;
final color white = color(255);
final color black = color(0);
final color red = color(255, 0, 0);

void settings() {
  size(640, 480, P3D);
  PJOGL.profile = 1;
}

void setup() {
  kinect = new Kinect(this);
  server = new SyphonServer(this, "Syphon Frames");
  
  canvas = createGraphics(640, 480);
  depthImg = new PImage(kinect.width, kinect.height);
  nearestPixel = new PVector();
  brushStroke.add(new PVector());
  drawing = false;
  kinect.initDepth();
}

void draw() {
  updateNearest();
  
  canvas.beginDraw();
  if (drawing) {
    addStrokePoint();
  }
  
  updateSnowflake();
  //displayDepth();
  canvas.endDraw();
  
  //image(canvas, 0, 0);
  server.sendImage(canvas);
}