void addStrokePoint() {
  PVector last = brushStroke.get(brushStroke.size()-1); 
  PVector current = new PVector(nearestPixel.x - width/2, nearestPixel.y - height/2);
  float delta = dist(current.x, current.y, last.x, last.y);
  
  if (delta > resolution) {
    brushStroke.add(new PVector(current.x, current.y));
  }
}