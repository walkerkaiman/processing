import peasy.*;
PeasyCam cam;

ArrayList<Layer> layers = new ArrayList<Layer>();
int layerPop = 1;
float time;
boolean recording;

void setup(){
  size(500, 500, P3D);
  frameRate(24); //<>//
  
  cam = new PeasyCam(this, 1000);
  
  for(int i = 0; i < layerPop; i++){ //<>//
    float zPos = i * -3;
    layers.add(new Layer(zPos, 50, 300)); //<>//
  }
}

void draw(){ //<>//
  background(0);
  
  for(Layer i : layers){
    i.display();
  }
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_####.tiff");
  }
  
  time += .1;
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
  
  if(recording){
    println("Recording...");
  }
  else{
    println("Not Recording...");
  }
}