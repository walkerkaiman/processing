class Layer{
  PShape shape;
  
  Layer(float zPos, float hSize, float lSize){
    makeLayer(zPos, hSize, lSize);
  }
  
  void display(){
    shape(shape);
  }
  
  void makeLayer(float z, float holeSize, float layerSize){
    int outerStep = int(radians(random(10, 60)));
    int innerStep = int(radians(random(10, 60)));
    
    shape = createShape();
    shape.beginShape();
    shape.fill(255);
    shape.noStroke();
      
      //Exterior 
      for(int angle = 0; angle < 360; angle += outerStep){
        float x = width/2 + layerSize * cos(angle);
        float y = height/2 + layerSize * sin(angle);
        
        shape.vertex(x, y, z);
      }
      
      //Hole
      shape.beginContour();
        for(int angle = 360; angle > 0; angle -fd= innerStep){
          float x = width/2 + holeSize * cos(angle);
          float y = height/2 + holeSize * sin(angle);
          
          shape.vertex(x, y, z);
        }
      shape.endContour();
    shape.endShape(CLOSE);
  }
}