class Ridge{
  ArrayList<PVector> vertices;
  float scale;
  float step;
  float speed;
  
  Ridge(float temp_scale, float temp_step, float temp_speed){
    scale = temp_scale;
    step = temp_step;
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    vertices = new ArrayList<PVector>();
    
    for(float deg = 0; deg < 360; deg += step){
      float degrees = radians(deg);
      float xSet = cos(degrees);
      float ySet = sin(degrees);
      float radius = noise(xSet, ySet) * scale;
      
      float x = xSet * radius + width/2;
      float y = ySet * radius + height/2;
      
      vertices.add(new PVector(x, y));
    }
  }
  
  void display(){
    for(PVector v : vertices){
      point(v.x, v.y, v.z);
    }
  }
}