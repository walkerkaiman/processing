import peasy.*;
PeasyCam cam;
Ridge ridge;

float time;

void setup(){
  size(600, 600, P3D);
  
  cam = new PeasyCam(this, 1000);
  ridge = new Ridge(100, 2, 3);
}

void draw(){
  background(0, 255, 0);
  ridge.run();
  
  time += .1;
}