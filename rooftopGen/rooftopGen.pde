ArrayList<Building> buildingList = new ArrayList();
int population = 500;

void setup(){
  size(600, 600);
  frameRate(24);
  
  for(int i = 0; i < population; i++){
    buildingList.add(new Building(random(width), random(height)));
  }
  
  background(255);
}

void draw(){
  smooth();
  //background(255);
  fill(255, 30);
  rect(0, 0, width, height);
  
  for(Building current : buildingList){
    current.run(); 
  }
  
  if(frameCount < 360){
    println("Saving Frame: "+frameCount);
    //saveFrame("/Users/kaimanwalker/Desktop/frames2/frame-#####.png");
  }
}
