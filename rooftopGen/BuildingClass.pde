class Building{
  Path path;
  PVector pos;
  float rectWidth, rectHeight;
  
  Building(float temp_posX, float temp_posY){
    pos = new PVector(temp_posX, temp_posY);
    
    rectWidth = random(1, 100);
    rectHeight = random(1, 100);
    
    path = new Path(pos);
  }
 
  void run(){
    checkEdge();
    update();
    path.run();
    display();
  }
 
  void update(){
    if(mousePressed == false){
      pos.x -= 3;
    }
  }
 
  void display(){
    float fill = map(pos.y, 0, height, 0, 255);
    
    strokeWeight(2);
    stroke(0);
    fill(255, 100);
    
    rect(pos.x, pos.y, rectWidth, rectHeight);
  } 
  
  void checkEdge(){
    if(pos.x + rectWidth < 0){
      pos.x = width;
      pos.y = random(height); 
    }
  }
  
}
