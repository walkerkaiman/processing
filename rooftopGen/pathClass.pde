class Path{
  PVector current, neighbor;
  
  Path(PVector temp_current){
    current = temp_current;
    neighbor = new PVector(10000, 10000);
  }
 
  void run(){
    update();
    display();
  }
  
  void update(){
    for(Building i : buildingList){
      float currentDist = dist(current.x, current.y, i.pos.x, i.pos.y);
      float neighborDist = dist(current.x, current.y, neighbor.x, neighbor.y);
      
      if(currentDist > 0 && currentDist < neighborDist){
        neighbor = i.pos;
      }
    }
  }
  
  void display(){
    stroke(255, 0, 0);
    strokeWeight(2);
    
    line(current.x, current.y, neighbor.x, neighbor.y);
  }
  
}
