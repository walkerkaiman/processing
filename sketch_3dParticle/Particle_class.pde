class Particle{
  PVector pos, vel, accel;
  float topSpeed, topForce, size, spin;
  
  Particle(){
    pos = new PVector(random(width), random(height), random(width));
    vel = new PVector(0, 0, 0);
    accel = new PVector(0, 0, 0);
    
    topSpeed = 20;
    topForce = .1;
    size = random(5, 20);
    spin = 0;
  }
  
  void run(){
    movement();
    update();
    display();
  }
  void movement(){
    
    for(Particle p : particles){
      PVector neighbor = p.pos;
      float mass = p.size;
      float distance = dist(pos.x, pos.y, pos.z, neighbor.x, neighbor.y, neighbor.z);
      
      if(distance > size + p.size){
        seek(p.pos);
      }
    }
  }
  
  void seek(PVector target){
    PVector desired = PVector.sub(target, pos);
    desired.setMag(topSpeed);
    
    PVector steer = PVector.sub(desired, vel);
    steer.limit(topForce);
    
    accel.add(steer);
  }
  
  void update(){
    accel.limit(topForce);
    vel.add(accel);
    vel.limit(topSpeed);
    pos.add(vel);
    accel.mult(0);
    
    float rotate = map(vel.mag(), 0, topSpeed, 15, 0);
    spin += rotate;
  }
  
  void display(){
    strokeWeight(2);
    stroke(255);
    fill(0);
    
    pushMatrix();
      translate(pos.x, pos.y, pos.z);
      rotateY(radians(spin));
      sphere(size);
    popMatrix();
  }
}