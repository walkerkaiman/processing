import peasy.*;

PeasyCam cam;
ArrayList<Particle>particles;
boolean recording;

void setup(){
  size(500, 500, P3D);
  frameRate(30);
  sphereDetail(3);
  
  cam = new PeasyCam(this, 10000);
  particles = new ArrayList<Particle>();
  recording = false;
  
  for(int i = 0; i < 100; i++){
    particles.add(new Particle());
  }
}

void draw(){
  background(0);
  
  for(int i = 0; i < particles.size(); i++){
    Particle p = particles.get(i);
    p.run();
    
    if(i > 0){
      stroke(255, 0, 0, 100);
      strokeWeight(2);
  
      PVector current = p.pos;
      PVector neighbor = particles.get(i-1).pos;
      line(current.x, current.y, current.z, neighbor.x, neighbor.y, neighbor.z);
    }
  }
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.tiff");
  }
}

void keyPressed(){
  switch(key){
     case 'r':
       recording =! recording;
       break; 
  }
}