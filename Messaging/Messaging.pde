/*///////// ESCAPE ROOM TRACKING AND TRIGGER SYSTEM //////////

THIS SKETCH RECIEVES OSC MESSAGES FROM TSPS ON 
USER'S CENTER OF MASS POSITION BEING TRACKED WITH THE IR CAMERA.

THIS SKETCH ALSO TAKES IN INFORMATION VIA SERIAL PORTS THAT ARE
TRIGGERED BY RFID.

THIS SKETCH KEEPS TRACK OF BOUDARY POSITIONS. WHEN A PERSON ENTERS AND LEAVES A BOUNDARY, 
A LEFT, RIGHT, OR BACK MESSAGE WILL BE SENT REMOTELY 
TO THE UNITY AND QLAB COMPUTER AND LOCALLY TO THE FOREST ROOM SKETCH VIA OSC.

A USER CANNOT TRIGGER ANOTHER MOVEMENT UNTIL THE TRACKED POSITION HAS LEFT THE AREA OF INTEREST.

___________________________________________________________________________

KEYBOARD CONTROLS

///UPDATE WHEN FINISHED
THERE ARE TWO DIFFERENT MODES THAT CONTROL FUNCTIONALITY IN THIS SKETCH.
THE PURPOSE OF THIS IS TO HELP ELIMINATE THE UNWANTED TRIGGERING OF MESSAGES.

1 - RIVER / FOREST MODE
2 - CAVE MODE

RIVER/FOREST MODE - ONLY RIVER MESSAGES ARE TRIGGERED AND SENT REMOTELY TO THE UNITY COMPUTER.
                    UNITY COMPUTER REPURPOSES RIVER MESSAGES FOR FOREST NAVIGATION.
                    
CAVE MODE - ALL OTHER MESSAGES ARE TRIGGERED AND SENT.



OSC MESSAGE PROTOCOL

SOME OSC LIBRARIES WON'T RECOGNIZE A MESSAGE WITH A NULL VALUE. THIS IS THE
REASON FOR THE 1 AT THE END OF THE MESSAGE.

"/river_L 1"
"/river_R 1"

"/cave_L 1"
"/cave_L 1"
"/cave_BACK 1"

"/torchFORWARD 1"
"/torchBEHIND 1"
"/collectL 1"
"/collectC 1"
"/collectR 1"

__________________________________________________________________________

NOTES

STARTUP
SET IPs TO REMOTE ADDRESSES (UNITY AND QLAB)
CHECK SERIAL PORTS FOR MESSAGE ACCURACY

*/

import processing.serial.*;
import oscP5.*;
import netP5.*;

OscP5 osc;
NetAddress unity_Address;
NetAddress local;

Serial incomingF, incomingB;
Serial collectL, collectC, collectR;

Tracker centroid;
Boundary bounds;

String mode;
int time;

void setup(){
  //DO NOT CHANGE!!!
  //GUI ELEMENTS ARE NOT RELATIVE TO WINDOW SIZE.
  size(400, 400);
  
  //OscP5(HOST, LISTENING PORT FROM TSPS)
  osc = new OscP5(this, 12000);
  
  //REMOTE AND LOCAL ADDRESSES
  //NetAddress(SEND ADDRESS, SEND PORT)
  unity_Address = new NetAddress("10.10.20.10", 9000);
  local = new NetAddress("10.10.20.137", 7777);
  
  //PRINT POSSIBLE PORT INDEX FOR SERIAL INPUT
  //USED IF A SERIAL DEVICE THROWS AN ERROR
  printArray(Serial.list());
  
  //SERIAL(THIS, PORT, BAUD RATE);
  //incomingF = new Serial(this, "COM6", 9600);
  //incomingB = new Serial(this, "COM8", 9600);
  //collectL = new Serial(this, Serial.list()[2], 9600);
  //collectC = new Serial(this, Serial.list()[1], 9600);
  //collectR = new Serial(this, Serial.list()[4], 9600);
  
  //Tracker(size);
  centroid = new Tracker(20);
  
  //Boundary(padding);
  bounds = new Boundary(25);
  
  mode = "River";
}

void draw(){
  background(0);
  
  //uncomment to test tracker as mouse position
  centroid.pos = new PVector(mouseX, mouseY);
  
  bounds.setMessageGates(centroid.pos);
  bounds.trigger();
  
  bounds.displayBounds();
  centroid.display();
  
  //DISPLAY MODE
  fill(255);
  textSize(12);
  text("Mode: " + mode, 150, height - 15);
  
  time += 3;
}

void keyPressed(){
  
  // WHEN SWITCHING MODES, SOMETIMES INCORRECT MESSAGES ARE SENT.
  // SETS GATES TO FALSE WHEN SWITCHING MODES TO AVOID.
  bounds.riverR_gate = false;
  bounds.riverL_gate = false;
  bounds.caveR_gate = false;
  bounds.caveL_gate = false;
  bounds.caveBACK_gate = false;
  
  switch(key){
    case '3':
      mode = "Forest";
      break;
    case '2':
      mode = "Cave";
      break;
    case '1':
      mode = "River";
      break;
  }
}

// UPDATES THE POSITION FROM TSPS
void oscEvent(OscMessage incoming){
  PVector temp_position = new PVector();
  println(temp_position);
  if(incoming.checkAddrPattern("/TSPS/personUpdated/") == true){
   temp_position = new PVector(incoming.get(3).floatValue() * width, incoming.get(4).floatValue() * height);
  }
  
  if(mode != "River"){
   if(temp_position.x < bounds.padding+10 || temp_position.x > width-bounds.padding-10 || temp_position.y < bounds.padding + 10){
     centroid.pos = temp_position;
    }
  }
  
  if(mode == "River"){
    centroid.pos = temp_position;
  }
  
}