class Tracker{
  PVector pos;
  int size;
  color stroke;
 
  Tracker(int temp_size){
    pos = new PVector(width/2, height/2);
    size = temp_size;
    stroke = color(255, 255, 255);
  }
 
  void display(){
    float pulse = constrain(abs((cos(radians(time))) * size), 8, size);
    float weight = map(pulse, 1, size, 8, 1);
    
    stroke(stroke);
    strokeWeight(weight);
    noFill();
    
    ellipse(pos.x, pos.y, pulse, pulse);
  }
}