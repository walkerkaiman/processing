class Boundary{
  PVector nw, ne, sw, se;
  int padding;
  
  int riverL_east, riverL_west, riverR_east, riverR_west, deadSpace;
  int caveL_north, caveL_south, caveR_north, caveR_south, caveBACK_north, caveBACK_south;
  
  boolean outside;
  boolean riverR_gate, riverL_gate, caveR_gate, caveL_gate, caveBACK_gate, forestR_gate, forestL_gate;
  
  OscMessage riverR, riverL, caveR, caveL, caveBACK, forestR, forestL, torchFORWARD, torchBEHIND, collect_L, collect_C, collect_R;
  
  Boundary(int temp_padding){
    padding = temp_padding;
    
    //TRIGGER BOUNDARIES
    riverL_west = 50;
    riverL_east = 170;
    
    riverR_west = 230;
    riverR_east = 350;
    
    caveL_north = 50;
    caveL_south = 170;
    
    caveR_north = 230;
    caveR_south = 350;
    
    caveBACK_north = 80;
    caveBACK_south = 320;
    
    deadSpace = 50;
    
    //CORNERS OF THE BOUNDARY LINES
    nw = new PVector(padding, padding);
    ne = new PVector(width - padding, padding);
    sw = new PVector(padding, height);
    se = new PVector(width - padding, height);
    
    outside = false;
    
    //GATES MAKE SURE THAT A MESSAGE ISN'T STREAMED WHEN IT IS TRIGGERED
    riverR_gate = false;
    riverL_gate = false;
    
    forestR_gate = false;
    forestL_gate = false;
    
    caveR_gate = true;
    caveL_gate = true;
    caveBACK_gate = true;
    
    //MESSAGES TO BE SENT VIA OSC
    riverL = new OscMessage("/river_L");
    riverL.add(1);
    
    riverR = new OscMessage("/river_R");
    riverR.add(1);
    
    caveL = new OscMessage("/cave_L");
    caveL.add(1);
    
    caveR = new OscMessage("/cave_R");
    caveR.add(1);
    
    caveBACK = new OscMessage("/cave_BACK");
    caveBACK.add(1);
    
    forestR = new OscMessage("/forest_R");
    forestR.add(1);
    
    forestL = new OscMessage("/forest_L");
    forestL.add(1);
    
    torchBEHIND = new OscMessage("/torch_BEHIND");
    torchBEHIND.add(1);
    
    torchFORWARD = new OscMessage("/torch_FORWARD");
    torchBEHIND.add(1);
    
    collect_L = new OscMessage("/collect_L"); 
    collect_L.add(1);
    
    collect_C = new OscMessage("/collect_C");
    collect_C.add(1);
    
    collect_R = new OscMessage("/collect_R");
    collect_R.add(1);
  } 
  
  /*
  _______SET GATES_______
  
  THIS FUNCTION TAKES A PVECTOR POSITION AS AN INPUT AND CHECKS TO SEE
  IF THAT POSITION IS OUTSIDE ANY OF THE BOUNDARIES.
  
  THE FUNCTION THEN SETS THE MESSAGE GATES AND THEN RUNS THE TRIGGER FUNCTION.
  
  NOTE
  THE convert_1D FUNCTION CONVERTS A 2D POSITION TO 1D. FOR THIS REASON THE convert_1D FUNCTION
  NEEDS TO KNOW WHAT REGION IT IS IN TO SELECT THE CORRECT FORMULA FOR CONVERSION.
  
  THE TWO NORTH CORNERS NEED THIER OWN FUNCTIONS BECAUSE THESE REGIONS ARE IN TWO 
  REGIONS. FOR EXAMPLE, THE NORTH WEST CORNER IS IN BOTH THE NORTH AND WEST REGIONS.
  
  */
  
  void setMessageGates(PVector pos){
    
    // IS THE PARTICIPANT INSIDE OF ALL BOUNDARIES?
    if(pos.y > nw.y && pos.x < ne.x && pos.x > nw.x){  
      outside = false;
      
      
      forestR_gate = true;
      forestL_gate = true;
      caveL_gate = true;
      caveR_gate = true;
      caveBACK_gate = true;
      
      centroid.stroke = color(255, 255, 255);
    }
    else{
      outside = true; 
      centroid.stroke = color(255, 0, 0);
      time += 5;
    }
    
    if(pos.x > width/2-deadSpace && pos.x < width/2+deadSpace){
        riverL_gate = true;
        riverR_gate = true;
      }
  }
  
  
  /*
  _______Trigger Messages_______
  THIS FUNCTION TRIGGERS THE OSC MESSAGES IF THEY PASS A 
  SERIES OF CONDITIONAL STATEMENTS.
  
  MESSAGES                RECIPIENT
  ===================================
  "/river_L 1"            Unity
  "/river_R 1"            Unity
  "/cave_L 1"             Unity, local
  "/cave_R 1"             Unity, local
  "/cave_BACK 1"          Unity, local
  "/torchBEHIND 1"        Unity
  "/torchAHEAD 1"         Unity
  "/collect_L 1"          Unity
  "/collect_C 1"          Unity
  "/collect_R 1"          Unity
  */
  
  void trigger(){
    
    
    
    
    /*
    ----------------------------------------
    RIVER MODE MESSAGES
    ----------------------------------------
    */
    
    if(mode == "River"){
      
      //riverL
      //sends riverL message as tracker enters the region
      if(riverL_gate){
        if(centroid.pos.x < width/2-deadSpace){
            osc.send(riverL, unity_Address);
            osc.send(riverL, local);
            
            riverL_gate = false;
            
            println("/river_L 1");
          }
        }
      
      //riverR
      //sends riverR message as tracker enters the region
      if(riverR_gate){
         if(centroid.pos.x > width/2 + deadSpace){
            osc.send(riverR, unity_Address);
            osc.send(riverR, local);
            
            riverR_gate = false;
            
            println("/river_R 1");
         }
      }
    }
    
    /*
    ----------------------------------------
    FOREST MODE MESSAGES
    ----------------------------------------
    */
    
    if(mode == "Forest"){
      
      //riverL
      //sends riverL message as tracker enters the region
      if(forestL_gate && outside){
        if(centroid.pos.x > riverL_west && centroid.pos.x < riverL_east){
            osc.send(riverL, unity_Address);
            osc.send(riverL, local);
            
            forestL_gate = false;
            
            println("/forest_L 1");
          }
        }
      
      //riverR
      //sends riverR message as tracker enters the region
      if(forestR_gate && outside){
         if(centroid.pos.x > riverR_west && centroid.pos.x < riverR_east){
            osc.send(riverR, unity_Address);
            osc.send(riverR, local);
            
            forestR_gate = false;
            
            println("/forest_R 1");
         }
      }
    }
    
    /*
    ----------------------------------------
    CAVE MODE MESSAGES
    ----------------------------------------
    */
    
    if(mode == "Cave"){
      
      if(centroid.pos.x > width - padding){
        
        //CAVE L
        //sends caveL message as tracker ENTERS the region.
        if(caveL_gate && outside){
          if(centroid.pos.y > caveL_north && centroid.pos.y < caveL_south){
            
            osc.send(caveL, unity_Address);
            
            caveL_gate = false;
            
            println("/cave_L 1");
          }
        }
        
        //CAVE R
        //sends caveR message as tracker ENTERS the region.
        if(caveR_gate && outside){
          if(centroid.pos.y > caveR_north && centroid.pos.y < caveR_south){
            
            osc.send(caveR, unity_Address);
            
            caveR_gate = false;
            
            println("/cave_R 1");
          }
        }
      }
      else if(centroid.pos.x < padding){
        //CAVE BACK
        //sends caveBACK message as tracker ENTERS the region
        if(caveBACK_gate && outside){
          if(centroid.pos.y > caveBACK_north && centroid.pos.y < caveBACK_south){
            
            osc.send(caveBACK, unity_Address);
            
            caveBACK_gate = false;
            
            println("/cave_BACK 1");
          }
        }
      }
    }
  }
  
  /*
  _______ DISPLAY BOUNDS _______
  
  DISPLAYS THE BOUNDARIES AND TRIGGER POSITIONS TO THE GUI.
  
  */
  
  void displayBounds(){
    
    // TRIGGER BOUNDS IN RED.
    noFill();
    strokeWeight(5);
    stroke(255, 0, 0);
    
    // LEFT RIVER MESSAGE TRIGGER BOUNDARIES
    line(riverL_west, 0, riverL_west, padding);
    line(riverL_east, 0, riverL_east, padding);
    
    // RIGHT RIVER MESSAGE TRIGGER BOUNDARIES
    line(riverR_west, 0, riverR_west, padding);
    line(riverR_east, 0, riverR_east, padding);
    
    // LEFT CAVE MESSAGE TRIGGER BOUNDARIES
    line(width-padding, caveL_north, width, caveL_north);
    line(width-padding, caveL_south, width, caveL_south);
    
    // RIGHT CAVE MESSAGE TRIGGER BOUNDARIES
    line(width-padding, caveR_north, width, caveR_north);
    line(width-padding, caveR_south, width, caveR_south);
    
    // CAVE BACK MESSAGE BOUNDARY
    line(0, caveBACK_north, padding, caveBACK_north);
    line(0, caveBACK_south, padding, caveBACK_south);
    
    // PADDING BOUNDARY IN WHITE
    stroke(255);
    
    // NORTH WALL PADDING BOUNDARY
    line(nw.x, nw.y, ne.x, ne.y);
    
    // WEST WALL PADDING BOUNDARY
    line(nw.x, nw.y, sw.x, sw.y);
    
    // EAST WALL PADDING BOUNDARY
    line(ne.x, ne.y, se.x, se.y);
    
    //DEAD SPACE LINES FOR RIVER
    line(width/2-deadSpace, 0, width/2-deadSpace, height);
    line(width/2+deadSpace, 0, width/2+deadSpace, height);
    
    //TRIGGER LABELS
    
    text("Forest L", riverL_west + 20, 15);
    text("Forest R", riverR_west + 20, 15);
    
    pushMatrix();
      rotate(radians(90));
      text("Cave R", 270, -385);
      text("Cave L", 90, -385);
      text("Cave BACK", 165, -8);
    popMatrix();
  }
  
  
}