PImage preProcess(PImage input){ 
  input.resize(arWidth, arHeight); //DOWN SAMPLE
  
  opencv.loadImage(input); //LOAD IMAGE TO OPENCV TO BE PREPROCESSED
  
  opencv.blur(blur); //REMOVE NOISE
  
  opencv.loadImage(_brightness(opencv.getSnapshot())); //ADJUST BRIGHTNESS
  
  opencv.contrast(contrast);//SEPERATE IMAGE VALUES
  
  opencv.threshold(threshold); //MAKE A BINARY IMAGE
  
  opencv.erode(); //SMOOTH CONTOURS
  
  //opencv.dilate(); //CLOSE HOLES
  
  return opencv.getSnapshot();
}