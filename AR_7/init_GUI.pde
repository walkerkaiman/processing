void initGUI(){
  color textColor = color(255);
  
  // Slider for blur size
  gui.addSlider("blur")
     .setLabel("blur")
     .setPosition(650, 10)
     .setRange(1, 10)
     .setColorValue(textColor)
     ;
     
  // Slider for brightness
  gui.addSlider("_brightness")
     .setLabel("brightness")
     .setPosition(650, 60)
     .setRange(-255, 255)
     .setColorValue(textColor)
     ;
     
     // Slider for contrast
  gui.addSlider("contrast")
     .setLabel("contrast")
     .setPosition(650, 110)
     .setRange(.5, 2.)
     .setColorValue(textColor)
     ;
     
  // Slider for threshold
  gui.addSlider("threshold")
     .setLabel("threshold")
     .setPosition(650, 170)
     .setRange(0, 255)
     .setColorValue(textColor)
     ;
}