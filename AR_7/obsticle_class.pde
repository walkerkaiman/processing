class Obsticle{
  float size;
  PVector position;
  int index;
  
  Obsticle(int temp_index, float temp_size){
    index = temp_index;
    size = temp_size;
    position = new PVector(0, 0);
  }
  
  void run(){
    update();
    //display();
    printMarkerPos();
  }
  
  void update(){
    position.x = nya.getMarkerVertex2D(index)[0].x + unitSize/4;
    position.y = nya.getMarkerVertex2D(index)[0].y + unitSize/4;
    perspective();
  }
  
  void display(){
    stroke(255, 0, 0);
    noFill();
    strokeWeight(5);
    
    pushMatrix();
      translate(position.x, position.y);
      ellipse(0, 0, size, size);
    popMatrix();
  }
  
  void printMarkerPos(){
    println("Index: " + index);
    println("Position: " + position);
    println();
  }
}