PImage _brightness(PImage input){
  
  input.loadPixels();
    for(int i = 0; i < input.width * input.height; i++){
      color current = input.pixels[i];
      
      int r = current >> 16 & 0xFF;  
      int g = current >> 8 & 0xFF;
      int b = current & 0xFF;   
      
      r += _brightness;
      g += _brightness;
      b += _brightness;
      
      r = constrain(r, 0, 255);
      g = constrain(g, 0, 255);
      b = constrain(b, 0, 255);
      
      input.pixels[i] = color(r, g, b);
    }
  input.updatePixels();
  
  return input;
}