import peasy.*;
PeasyCam cam;

final color white = color(255);
final color black = color(0);
final float cellSize = 20;
final int angleStep = 5;
final int radius = 50;
final int cylinderHeight = 1000;

ArrayList<Cell> cells;
IntList state;
float time;
boolean recording;

void setup () {
  cam = new PeasyCam(this, 1000);
  recording = false;
  
  init();
  
  size(600, 600, P3D);
  noStroke();
}

void draw () {
  background(black);
  lights();
  
  for (Cell c : cells) {
    c.update();
    c.display();
  }
  
  if (recording) {
    saveFrame("data/frames/frame_#####.png");
  }
  time += .001;
}