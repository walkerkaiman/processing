void initStates (int patternSize) {
  state = new IntList();
  
  for (int i = 0; i < patternSize; i++) {
    state.append(int(random(2)));
  }
  
  println("Pattern Size: " + patternSize);
  println();
}