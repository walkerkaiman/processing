class Cell {
  color fill;
  PVector pos;
  float size;
  
  Cell (color fill, PVector pos, float size) {
    this.fill = fill;
    this.pos = pos;
    this.size = size;
  }
  
  void update () {
    pos.z = noise(pos.x/500, pos.y/500, time) * 1000;
  }
  
  void display () {
    pushMatrix();
      translate(pos.x, pos.y, pos.z);
      fill(fill);
      
      box(size);
    popMatrix();
  }
}