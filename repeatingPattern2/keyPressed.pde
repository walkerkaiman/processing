void keyPressed () {
  if (key == 'p' || key == 'P'){
    init();
  }
  else if (key == 'r' || key == 'R') {
    recording =! recording;
    println("Recording: " + recording);
  }
}