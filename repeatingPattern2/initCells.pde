void initCells (int patternSize) {
  cells = new ArrayList<Cell>();
  int stateIndex = 0;
  
  for (float y = 0; y < 1000; y += cellSize) {
    for (float x = 0; x < 1000; x += cellSize) {
      color fill = state.get(stateIndex) == 1 ? white : black;
      float z = noise(x/500, y/500, time) * 500;
      PVector pos = new PVector(x, y, z);
      float size = cellSize;
      
      cells.add(new Cell(fill, pos, size));
      
      stateIndex++;
      stateIndex = stateIndex % patternSize;
    }
  }
}