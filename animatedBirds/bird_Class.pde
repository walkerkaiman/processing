class Bird{
  int index;
  PVector position, velocity, acceleration;
  float maxForce, maxSpeed, r, angle;
  
  Bird(int temp_index){
    index = temp_index;
    position = new PVector(random(edgeSize), random(edgeSize), random(edgeSize));
    maxForce = random(.1, .5);
    maxSpeed = random(5, 7);
    acceleration = new PVector(0, 0, 0);
    velocity = new PVector(0, 0, 0);
    r = 10;
  } 
  
  void run(){
    update();
    borders();
    display();
  }
  
void follow(Flow field) {
    PVector desired = field.lookup(position);
    desired.mult(maxSpeed);
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }

  void applyForce(PVector force) {
    acceleration.add(force);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
  }

  void display() {
    float theta = velocity.heading2D() + radians(90);
    
    stroke(255);
    strokeWeight(1);
    fill(0);
    
    pushMatrix();
      translate(position.x, position.y, position.z);
      rotate(theta);
      
      float flap = cos(radians(angle+index));
  
      //RIGHT WING
      beginShape();
        vertex(-1, 0, 0);
        vertex(-1, 10, 0);
        vertex(2, 10, flap*2);
        vertex(6, 14, flap*6);
      endShape(CLOSE);
      
      //LEFT WING
      beginShape();
        vertex(-1, 0, 0);
        vertex(-1, 10, 0);
        vertex(-4, 10, -flap*-4);
        vertex(-8, 14, -flap*-8);
      endShape(CLOSE);
      
      //TAIL
      beginShape();
        vertex(-1, 0, 0);
        vertex(-1, 10, 0);
        vertex(-1, 10, -6);
      endShape(CLOSE);
      
    popMatrix();
      angle += 50;
      
      if(angle > 360){
        angle = 0;
      }
  }
  
  void borders() {
    if (position.x < -r) position.x = edgeSize+r;
    if (position.y < -r) position.y = edgeSize+r;
    if (position.z < -r) position.z = edgeSize+r;
    if (position.x > edgeSize+r) position.x = -r;
    if (position.y > edgeSize+r) position.y = -r;
    if (position.z > edgeSize+r) position.z = -r;
  }
  
}
