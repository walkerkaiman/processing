import peasy.*;
PeasyCam cam;

int population = 2000;
int edgeSize = 600;

Bird[] flock;
Flow field;

void setup(){
  size(500, 500, OPENGL);
  frameRate(24);
  
  cam = new PeasyCam(this, 1000);
  field = new Flow(20);
  flock = new Bird [population];
  
  for(int i = 0; i < flock.length; i++){
    flock[i] = new Bird(i);
  }
}

void draw(){
  background(0);
  rotateX(radians(90));
  
  field.run();

  for(Bird current : flock){
    current.follow(field);
    current.run();
  }  
  
  if(frameCount > 500 && frameCount < 1000){
    //println("RECORDING FRAME: " + frameCount);
    //saveFrame("");
  }else{//println("Not Recording...");
}
println(frameRate);
}
