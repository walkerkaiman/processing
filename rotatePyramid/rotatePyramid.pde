import peasy.*;
boolean recording;

PeasyCam cam;
Pyramid pyramid;
Orbit orbit;

void setup(){
  size(640, 480, P3D);
  
  recording = false;
  cam = new PeasyCam(this, 1000);
  pyramid = new Pyramid(100);
  orbit = new Orbit(20);
}

void draw(){
  background(0);
  
  pyramid.display();
  orbit.run(pyramid.pos);
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.tiff");
  }
}

void keyPressed(){
 switch(key){
   case 'r':
     recording =! recording;
     break;
 }
  
}