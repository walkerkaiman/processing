class Pyramid{
  PVector pos;
  float pyrHeight;
  
  Pyramid(float temp_pyrHeight){
    pos = new PVector(0, 0, 0);
    pyrHeight = temp_pyrHeight;
  }
  
  void display(){
    stroke(255);
    strokeWeight(3);
    fill(0);
  
    PVector a = new PVector(pos.x, pos.y-pyrHeight/2, pos.z);
    PVector b = new PVector(pos.x-pyrHeight/2, pos.y+pyrHeight/2, pos.z+pyrHeight/2);
    PVector c = new PVector(pos.x+pyrHeight/2, pos.y+pyrHeight/2, pos.z+pyrHeight/2);
    PVector d = new PVector(pos.x+pyrHeight/2, pos.y+pyrHeight/2, pos.z-pyrHeight/2);
    PVector e = new PVector(pos.x-pyrHeight/2, pos.y+pyrHeight/2, pos.z-pyrHeight/2);
        
    //BCDE
    beginShape();
      vertex(b.x, b.y, b.z);
      vertex(c.x, c.y, c.z);
      vertex(d.x, d.y, d.z);
      vertex(e.x, e.y, d.z);
    endShape(CLOSE);
    
    //ABC
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(b.x, b.y, b.z);
      vertex(c.x, c.y, c.z);
    endShape(CLOSE);
    
    //ACD
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(c.x, c.y, c.z);
      vertex(d.x, d.y, d.z);
    endShape(CLOSE);
    
    //ADE
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(d.x, d.y, d.z);
      vertex(e.x, e.y, e.z);
    endShape(CLOSE);
    
    //AEB
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(e.x, e.y, e.z);
      vertex(b.x, b.y, b.z);
    endShape(CLOSE);
  }
  
}