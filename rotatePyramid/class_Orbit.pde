class Orbit{
  PVector pos;
  float orbiterSize, angle, radius, orbitSpeed;
  boolean generation;
  Orbit child;
  
  Orbit(float temp_orbiterSize){
    orbiterSize = temp_orbiterSize;
    radius = orbiterSize*4;
    orbitSpeed = radians(random(2));
    generation = false;
    pos = new PVector(0, 0, 0);
    
    createChild();
  }
  
  void run(PVector parentPos){
    update(parentPos);
    display();
    displayPath(parentPos);
    
    if(generation){
      child.run(pos);
    }
  }
  
  void update(PVector parent_pos){
    angle += orbitSpeed;
    
    float x = cos(angle)*radius + parent_pos.x;
    float z = sin(angle)*radius + parent_pos.z;
    
    pos = new PVector(x, pyramid.pos.y, z);
  }
  
  void display(){
    sphereDetail(int(orbiterSize-5));
    stroke(255);
    strokeWeight(1);
    fill(0);
  
    pushMatrix();
      translate(pos.x, pos.y, pos.z);
      sphere(orbiterSize);
    popMatrix();
  }
  
  void displayPath(PVector parentPos){
    noFill();
    stroke(255, 100);
    strokeWeight(1);
    
    pushMatrix();
      translate(parentPos.x, parentPos.y, parentPos.z);
      rotateX(radians(90));
      ellipse(0, 0, radius+orbiterSize*4, radius+orbiterSize*4);
    popMatrix();
  }
  
  void createChild(){
    float chance = random(1);
    
    if(chance >= .2 && orbiterSize > 0){
      child = new Orbit(orbiterSize-5);
      generation = true;
    }
  }
  
}