import peasy.*;
PeasyCam cam;

float time;
boolean recording;

void setup(){
  size(700, 700, P3D);
  frameRate(24);
  
  cam = new PeasyCam(this, 1000);
  
  recording = false;
  
  stroke(0);
  strokeWeight(3);
  fill(255, 150);
  //noFill();
}

void draw(){
  background(0);
  //lights();
  //ortho();
  //float smooth = cos(time)*100;
  
  for(int x = 0; x < width*2; x += 15){
    beginShape();
      for(float y = 0; y < height*2; y += 20){
        float z = cos(x/100 + y/100 + time)*100;
        vertex(x, y, z);
      }
    endShape();
  }
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.png");
  }
  
  time += .1;
}
void keyPressed(){
 if(key == 'r'){
   recording =! recording;
 }
}