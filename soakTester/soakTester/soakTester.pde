import hypermedia.net.*;
UDP udp;

PrintWriter reportLog;

final int localPort = 4444;
final int remotePort = 8888;
final String remoteIP = "10.10.20.255";
final int FPS = 30;
final int oneSecond = 1;
final int seconds_between_restarts = 40;

int restarts, secondsElapsed, prevSecond;
boolean restartFlag;

void setup() {
  udp = new UDP(this, localPort);
  udp.listen(true);
  restartFlag = true;
  frameRate(FPS);
  reportLog = createWriter("soak_test_log.txt");
  restart();
}

void draw() {
  secondsElapsed = frameCount / FPS;

  if (secondsElapsed != prevSecond) {
    restartFlag = true;
  }

  if (restartFlag && secondsElapsed % seconds_between_restarts == 0) {
    restart();
  }

  prevSecond = secondsElapsed;
}

void logEvent(String event){
   reportLog.println("Event: " + event);
   reportLog.println("Date (year/month/day): " + year() + "/" + month() + "/" + day());
   reportLog.println("Time: " + hour() + ":" + minute() + ":" + second());
   reportLog.println("Elapsed Time: " + secondsElapsed);
   reportLog.println("Number of restarts: " + restarts); 
   reportLog.println();
}

void receive(){
  
}

void restart () {
  udp.send("restartcomputer", remoteIP, remotePort);
  restarts++;
  restartFlag = false;
  
  logEvent("UDP Message Sent");
  reportLog.flush();
}