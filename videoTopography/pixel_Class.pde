class Pixel{
  int x, y;
  PVector size;
  
  Pixel(int temp_x, int temp_y, PVector temp_size){
    x = temp_x;
    y = temp_y;
    size = temp_size;
  }
  
  void update(float elevation){
    size.z = elevation;
  }
  
  void display(color c){
    fill(c);
    noStroke();
    
    pushMatrix();
      translate(x, y, -size.z);
      box(size.x, size.y, size.z);
    popMatrix();
  }
  
}