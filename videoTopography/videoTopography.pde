import peasy.*;
import processing.video.*;

PeasyCam cam;
Movie video;
Capture feed;

ArrayList<Pixel> pixls;
int step = 2;

void setup(){
  size(640, 480, P3D);
  frameRate(24);
  
  cam = new PeasyCam(this, 1000);
  
  video = new Movie(this, "1.mov");
  video.play();
  video.loop();
  video.volume(0);
  
  feed = new Capture(this, width, height);
  feed.start();
  
  pixls = new ArrayList<Pixel>();
  
  for(int col = 0; col < width; col += step){
    for(int row = 0; row < height; row += step){
      
      PVector size = new PVector(step, step, 1);
      pixls.add(new Pixel(col, row, size));
    }
  }
}

void draw(){
  background(0);
  
  if(feed.available()){
    feed.read();
    feed.loadPixels();
  }
  
  if(video.available()){
    video.read();
   // video.loadPixels();
  }
 
  for(Pixel i : pixls){
    color pixelFill = feed.get(i.x, i.y);
    float pixelBrightness = brightness(feed.get(i.x, i.y));
    float elevation = map(pixelBrightness, 0, 255, 0, 50);
    
    i.update(elevation);
    i.display(pixelFill);
  }
}