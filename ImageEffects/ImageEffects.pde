import processing.video.*;

Capture cam;

void setup(){
  size(640, 360);
  cam = new Capture(this, 640, 360);
  cam.start();
}

void draw(){
  
  if(cam.available()){
    cam.read(); 
  }
  
  background(0);
  
  float thresh = map(mouseX, 0, width, 0, 255);
  float bright = map(mouseY, 0, height, 0, 255);
  float contrast = map(mouseY, 0, height, 0, 5);
  
  PImage binaryImage = binaryConvert(cam, thresh);
  PImage contrastImage = contrastConvert(binaryImage, contrast);
  
  
  image(contrastImage, 0, 0, width, height);

  println("Threshold: "+thresh);
  println("Brightness: "+bright);
  println("Contrast: "+contrast);
  println();
}

PImage binaryConvert(PImage input, float threshold){
  PImage output = new PImage(input.width, input.height);
  
  int w = input.width;
  int h = input.height;
  
  input.loadPixels();
  output.loadPixels();
  
  for(int i = 0; i < w*h; i++){
    if(brightness(input.pixels[i]) > threshold){
      output.pixels[i] = color(255, 255, 255);
    }
    else{
      output.pixels[i] = color(0, 0, 0);
    }
  }
  
  input.updatePixels();
  output.updatePixels();
  
  return output;
}

PImage brightConvert(PImage input, float brightness){
  PImage output = new PImage(input.width, input.height);
  int w = input.width;
  int h = input.height;
  
  input.loadPixels();
  output.loadPixels();
  
  for(int i = 0; i < w*h; i++){
    color inColor = input.pixels[i];
  
    int r = (inColor >> 16) & 0xFF;
    int g = (inColor >> 8) & 0xFF;
    int b = inColor & 0xFF;
    
    r = (int)(r + brightness);
    g = (int)(g + brightness);
    b = (int)(b + brightness);
    
    r = r < 0 ? 0 : r > 255 ? 255 : r;
    g = g < 0 ? 0 : g > 255 ? 255 : g;
    b = b < 0 ? 0 : b > 255 ? 255 : b;
    
    output.pixels[i]= 0xff000000 | (r << 16) | (g << 8) | b;
  }
  
  input.updatePixels();
  output.updatePixels();
  
  return output;
}

PImage contrastConvert(PImage input, float contrast){
  PImage output = new PImage(input.width, input.height);
  int w = input.width;
  int h = input.height;
  
  input.loadPixels();
  output.loadPixels();
  
  for(int i = 0; i < w*h; i++){
    color inColor = input.pixels[i];
  
    int r = (inColor >> 16) & 0xFF;
    int g = (inColor >> 8) & 0xFF;
    int b = inColor & 0xFF;
    
    r = (int)(r * contrast);
    g = (int)(g * contrast);
    b = (int)(b * contrast);
    
    r = r < 0 ? 0 : r > 255 ? 255 : r;
    g = g < 0 ? 0 : g > 255 ? 255 : g;
    b = b < 0 ? 0 : b > 255 ? 255 : b;
    
    output.pixels[i]= 0xff000000 | (r << 16) | (g << 8) | b;
  }
  
  input.updatePixels();
  output.updatePixels();
  
  return output;
}