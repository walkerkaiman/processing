import processing.net.*;

Client c;

int data[];
int index;

void setup(){
  size(640, 480);
  fill(255, 0, 0);
  noStroke();
  
  //Client(this, WiFi IP address of server, port);
  c = new Client(this, "127.0.0.1", 9000);
  index = 0;
}

void draw(){
  background(0);
  
  // Send mouse coords to server
  c.write(index + " " + mouseX + " " + mouseY + "\n");
  
  ellipse(mouseX, mouseY, 10, 10);
}