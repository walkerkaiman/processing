import megamu.mesh.*;
import peasy.*;

// User Defined
final int numberOfPoints = 100;

// Do not change the following
PeasyCam cam;
Delaunay delaunay;

PImage pic;
float [][] points = new float [numberOfPoints][2];
float[][] edges;
final color black = color(0);
final color white = color(255);

void setup () {
  cam = new PeasyCam (this, 1000);
  size(600, 600, P3D);
  stroke(255);

  pic = loadImage("1.jpg");

  // Initiate Dot position and color at random
  for (int x = 0; x < numberOfPoints; x++) {
    for (int y = 0; y < 2; y++) {
      points[x][y] = random(0, width); // Position
    }
  }

  delaunay = new Delaunay(points);
  edges = delaunay.getEdges();
}

void draw () {
  background(black);

  for (int i = 0; i < edges.length; i++) {
    float startX = edges[i][0];
    float startY = edges[i][1];
    float endX = edges[i][2];
    float endY = edges[i][3];

    line( startX, startY, endX, endY );
  }
}