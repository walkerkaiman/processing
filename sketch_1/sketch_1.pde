import peasy.*;

PeasyCam cam;
float time;
float radius = .1;
boolean recording = false;
int frame;

void setup(){
  size(600, 600, P3D);
  frameRate(30);
  
  cam = new PeasyCam(this, 1000);
 
  stroke(255);
  strokeWeight(2);
  noFill();
}

void draw(){
  background(0);
  
  for(float i = 0; i < 300; i += 5){
    
    float radius = cos(i/100 + time) * 100;
    
    pushMatrix();
      translate(0, 0, i);
      ellipse(0, 0, radius, radius);
    popMatrix();
  }
  
  time += .03;
  
  if(recording){
    frame++;
    println("Frame: " + frame);
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.png");
  }
}

void keyPressed(){
  if(key == 'r'){
    recording =! recording;
    println("Recording: " + recording);
  }
}