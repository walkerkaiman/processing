class Object{
  char letter;
  int size, fade;
  float topSpeed, topForce, gravity;
  PVector pos, vel, accel;
  
  Object(char temp_letter, float temp_x){
    letter = temp_letter;
    size = 30;
    fade = 255;
    
    topSpeed = 11;
    topForce = .3;
    gravity = .2;
    
    pos = new PVector(temp_x, height);
    vel = new PVector(random(-2, 2), -topSpeed);
    accel = new PVector(0, gravity);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    collide();
    gravity();
    
    fade--;
  }
  
  void collide(){
    for(int i = 0; i < objects.size(); i++){
      Object other = objects.get(i);
      float distance = dist(other.pos.x, other.pos.y, pos.x, pos.y);
      
      if(distance > 0 && distance < size){
        PVector collide = PVector.sub(other.pos, pos);
        collide.normalize();
        collide.mult(-1);
        accel.add(collide);
      }
    }
    
    //Left Wall
    if(pos.x < 1){
      PVector wallPos = new PVector(0, pos.y);
      wall(wallPos);
    }
    
    //Right Wall
    if(pos.x+size > width-1){
      PVector wallPos = new PVector(width, pos.y);
      wall(wallPos);
    }
    
    //Ceiling
    if(pos.y-size < 1){
      PVector wallPos = new PVector(pos.x, 0);
      wall(wallPos);
    }

  }
  
  void wall(PVector wallPos){
    PVector collide = PVector.sub(wallPos, pos);
    
    vel.mult(0);
    
    collide.normalize();
    collide.add(vel);
    collide.mult(-1);
    accel.add(collide);
  }
  
  void gravity(){
    accel.add(0, gravity);
    accel.limit(topForce);
    
    vel.add(accel);
    vel.limit(topSpeed);
    
    pos.add(vel);
    accel.mult(0);
  }
  
  void display(){
    float steer = vel.heading()+radians(90);
    
    textSize(size);
    fill(255);
    
    pushMatrix();
      translate(pos.x, pos.y);
      rotate(steer);
      
      text(letter, 0, 0);
    popMatrix();
  }
  
}