import codeanticode.syphon.*;
SyphonServer server;

ArrayList<Object>objects = new ArrayList<Object>();

void settings(){
  size(640, 480, P2D);
  PJOGL.profile = 1;
}

void setup(){
  server = new SyphonServer(this, "// Frames from Processing //");
}

void draw(){
  background(0);
  
  for(int i = 0; i < objects.size(); i++){
    Object current = objects.get(i);
    current.run();
    
    if(current.pos.y > height){
      objects.remove(i);
    }
  }
  
  server.sendScreen();
}

void keyPressed(){
  float initX;
  
  switch(key){
    case '1':
    case 'q':
    case 'Q':
    case 'a':
    case 'A':
    case 'z':
    case 'Z':
      initX = 30;
      break;
    case '2':
    case 'w':
    case 'W':
    case 's':
    case 'S':
    case 'x':
    case 'X':
      initX = width*.1;
      break;
    case '3':
    case 'e':
    case 'E':
    case 'd':
    case 'D':
    case 'c':
    case 'C':
      initX = width*.2;
      break;
    case '4':
    case 'r':
    case 'R':
    case 'f':
    case 'F':
    case 'v':
    case 'V':
      initX = width*.3;
      break;
    case '5':
    case 't':
    case 'T':
    case 'g':
    case 'G':
    case 'b':
    case 'B':
      initX = width*.4;
      break;
    case '6':
    case 'y':
    case 'Y':
    case 'h':
    case 'H':
    case 'n':
    case 'N':
      initX = width*.5;
      break;
    case '7':
    case 'u':
    case 'U':
    case 'j':
    case 'J':
    case 'm':
    case 'M':
      initX = width*.6;
      break;
    case '8':
    case 'i':
    case 'I':
    case 'k':
    case 'K':
      initX = width*.7;
      break;
    case '9':
    case 'o':
    case 'O':
    case 'l':
    case 'L':
      initX = width*.8;
      break;
    case '0':
    case 'p':
    case 'P':
      initX = width*.9;
      break;
    default:
      initX = random(30, width*.9);
      break;
  }
  
  objects.add(new Object(key, initX));
}