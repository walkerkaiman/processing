import controlP5.*;
import gab.opencv.*;
import java.io.*; 
import jp.nyatla.nyar4psg.*;
import processing.video.*;
import codeanticode.syphon.*;

Capture cam;
MultiMarker nya;
OpenCV opencv;
ControlP5 gui;
SyphonServer server;
Flock flock;

String camPara = "/Users/kaimanwalker/Dropbox/Processing/libraries/NyARToolkit/data/camera_para.dat";
String patternPath = "/Users/kaimanwalker/Dropbox/Processing/AR_4/data/patternMaker/examples/ARToolKit_Patterns";

int arWidth = 640;
int arHeight = 480;
int unitSize = 80;

int markerPopulation = 3;
int boidPopulation = 200;

ArrayList<Obsticle> obsticles;

int blur = 3;
int _brightness = 79;
float contrast = 1.54;
int threshold = 249;


void settings(){
  size(640, 480, P3D);
  
  //GUI --- CAN'T DISPLAY AR AND GUI PROPERLY TOGETHER
  //size(800, 480, P3D);
  
  PJOGL.profile = 1;
}

void setup(){  
  //LIST OF AVAILABLE CAMERAS
  String[] cameras = Capture.list();
 
 /*
  //PRINTS LIST OF AVAILABLE CAMERAS
  for(int i = 0; i < cameras.length; i++){
    println(cameras[i]);
  }
  */
  
  flock = new Flock();
  obsticles = new ArrayList<Obsticle>();
  
  cam = new Capture(this, cameras[16]);//16 and 3
  
  opencv = new OpenCV(this, arWidth, arHeight);
  nya = new MultiMarker(this, arWidth, arHeight, camPara, NyAR4PsgConfig.CONFIG_DEFAULT);
  server = new SyphonServer(this, "Frames from Processing");
  gui = new ControlP5(this);
  
  //LOADS PATTERN FILES
  String[] patterns = loadPatternFilenames(patternPath);
  
  nya.addARMarker("4x4_13.patt", unitSize);
  obsticles.add(new Obsticle(0, unitSize));
  
  nya.addARMarker("4x4_47.patt", unitSize);
  obsticles.add(new Obsticle(1, unitSize));
  
  nya.addARMarker("4x4_95.patt", unitSize);
  obsticles.add(new Obsticle(2, unitSize));
  
  //INITIATE MARKER PATTERNS
  /*
  for(int i = 0; i < markerPopulation; i++) {
    nya.addARMarker(patternPath + "/" + patterns[i], unitSize);
    obsticles.add(new Obsticle(i, unitSize));
  }
  */
  
  //INITIATE BOIDS
  for(int i = 0; i < boidPopulation; i++) {
    Boid b = new Boid(width/2, height/2);
    flock.addBoid(b);
  }
  
  initGUI();
  cam.start();
}

void draw() {
  PImage processed;
 
  background(0);
  
  //IF A NEW FRAME FROM CAMERA IS AVAILABLE 
  if(cam.available()){
    
    //UPDATE FRAME
    cam.read();
    
    //CONDITION IMAGE FOR PATTERN RECOGNITION.
    processed = preProcess(cam.get());

    //FIND IF ANY MARKERS ARE IN THE IMAGE BEING FED TO IT.
    nya.detect(processed);
    
    //SETS THE VIRTUAL (3D) CAMERA TO THE CAMERA'S
    nya.setARPerspective();
    
    //CYCLE THROUGH ALL MARKERS
    for(int i = 0; i < markerPopulation; i++){
      
      if( !nya.isExistMarker(i) ){ // IF THE MARKER DOES NOT EXIST
        continue; //ESCAPE THE CURRENT INDEX AND GO TO THE NEXT INDEX
      }
      
      obsticles.get(i).run();
    }
    
    //RUN BOIDS
    flock.run();

    //DISPLAY THE PROCESSED IMAGE THAT IS GETTING FED TO THE AR TOOLKIT.
    //image(processed, 0, 0, width, height);
    
    //GUI();
    server.sendScreen();
  }
}