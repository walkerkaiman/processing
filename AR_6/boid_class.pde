class Boid {
  PVector location, velocity, acceleration;
  float r, maxforce, maxspeed;

  Boid(float x, float y) {
    acceleration = new PVector(0, 0);
    velocity = new PVector(random(-1, 1),random(-1, 1));
    location = new PVector(x, y);
    r = 5;
    maxspeed = 5;
    maxforce = 0.02;
  }

  void run(ArrayList<Boid> boids) {
    flock(boids);
    update();
    borders();
    render();
  }

  void applyForce(PVector force) {
    acceleration.add(force);
  }

  void flock(ArrayList<Boid> boids) {
    PVector sep = separate(boids);   // Separation
    PVector ali = align(boids);      // Alignment
    PVector coh = cohesion(boids);   // Cohesion
    
    sep.mult(10);
    ali.mult(5);
    coh.mult(5);
   
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
  }
  
  void update() {
    velocity.add(acceleration);
    velocity.limit(maxspeed);
    location.add(velocity);
    acceleration.mult(0);
  }

  PVector seek(PVector target) {
    PVector desired = PVector.sub(target,location);  
    desired.normalize();
    desired.mult(maxspeed);
    PVector steer = PVector.sub(desired,velocity);
    steer.limit(maxforce);  
    
    return steer;
  }
  
  void render() {
    float theta = velocity.heading2D() + radians(90);
    
    noFill();
    stroke(255);
    strokeWeight(2);
    
    pushMatrix();
      translate(location.x,location.y);
      rotate(theta);
    
      beginShape();
         vertex(-r*2, 0);
         vertex(0, -r*2);
         vertex(r*2, 0);
      endShape();
      perspective();
    popMatrix();
  }

  void borders() {
    if (location.x < -r) location.x = width+r;
    if (location.y < -r) location.y = height+r;
    if (location.x > width+r) location.x = -r;
    if (location.y > height+r) location.y = -r;
  }

  // Separation
  PVector separate (ArrayList<Boid> boids) {
    float desiredseparation = 25;
    PVector steer = new PVector(0, 0, 0);
    int count = 0;
    
    for(Boid other : boids) {
      float d = PVector.dist(location, other.location);
      
      if((d > 0) && (d < desiredseparation)) {
       
        PVector diff = PVector.sub(location, other.location);
        diff.normalize();
        diff.mult(.05);
        diff.div(d);        // Weight by distance
        steer.add(diff);
        
        count++;
      }
    }
    
    for(Obsticle obs : obsticles) {
      float d = PVector.dist(location, obs.position);
      
      if(d < desiredseparation + obs.size) {
        PVector diff = PVector.sub(location, obs.position);
        
        diff.normalize();
        diff.div(d);        
        steer.add(diff);          
      }
    }
    
    if(count > 0) {
      steer.div((float)count);
    }

    if(steer.mag() > 0) {
      steer.normalize();
      steer.mult(maxspeed);
      steer.sub(velocity);
      steer.limit(maxforce);
    }
    
    return steer;
  }

  // Alignment
  PVector align (ArrayList<Boid> boids) {
    float neighbordist = 50;
    PVector sum = new PVector(0,0);
    int count = 0;
    
    for (Boid other : boids) {
      float d = PVector.dist(location,other.location);
      
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.velocity);
        count++;
      }
    }
    if (count > 0) {
      sum.div((float)count);
      sum.normalize();
      sum.mult(maxspeed);
      PVector steer = PVector.sub(sum,velocity);
      steer.limit(maxforce);
      
      return steer;
    } 
    else {
      return new PVector(0, 0);
    }
  }

  // Cohesion
  PVector cohesion (ArrayList<Boid> boids) {
    float neighbordist = 50;
    PVector sum = new PVector(0, 0); 
    int count = 0;
    
    for (Boid other : boids) {
      float d = PVector.dist(location,other.location);
      
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.location);
        
        count++;
      }
    }
    if (count > 0) {
      sum.div(count);
      
      return seek(sum);  // Steer towards the location
    } 
    else {
      return new PVector(0,0);
    }
  }
  
}