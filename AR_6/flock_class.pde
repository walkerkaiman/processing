/* 
___FLOCK CLASS___
THIS CLASS GROUPS THE BOIDS TOGETHER FOR EASIER
AND CLEANER CODE IN THE PROGRAM.
*/

class Flock {
  ArrayList<Boid> boids; 

  Flock() {
    boids = new ArrayList<Boid>();
  }

  void run() {
    for(Boid b : boids) {
      b.run(boids);
    }
  }

  void addBoid(Boid b) {
    boids.add(b);
  }
}