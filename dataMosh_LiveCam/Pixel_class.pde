class Pixel {
  PVector position;
  color fill;
  
  Pixel (PVector position, color fill) {
    this.position = position;
    this.fill = fill;
  }
  
  void display() {
    fill(fill);
    rect(position.x, position.y, PIXEL_SIZE, PIXEL_SIZE);
  }
}