import gab.opencv.*;
import processing.video.*;

OpenCV opencv;
Capture camera;

final float PIXEL_SIZE = 2;
final int IFRAME_COLLECTION_RATE = 60;
final float PFRAME_FACTOR = .5;

boolean recievedIFrame;
int numberOfPixels;
ArrayList<Pixel> moshedPixels = new ArrayList<Pixel>();

void setup() {
  size(640, 360);
  noStroke();

  String[] availableCameras = Capture.list();
  printArray(availableCameras);
  camera = new Capture(this, availableCameras[3]);
  camera.start();
  recievedIFrame = false;

  opencv = new OpenCV(this, width, height);
  numberOfPixels = int((width/PIXEL_SIZE) * (height/PIXEL_SIZE));
}

void draw() {
  if (recievedIFrame && moshedPixels.size() == numberOfPixels) {
    opencv.loadImage(camera);
    opencv.calculateOpticalFlow();

    int pixelIndex = 0; // Needed to calculate index of 1D array.

    for (int posY = 0; posY < height; posY += PIXEL_SIZE) {
      for (int posX = 0; posX < width; posX += PIXEL_SIZE) {

        PVector cameraLocalFlow = opencv.getFlowAt(posX, posY);
        cameraLocalFlow.mult(PFRAME_FACTOR);
        moshedPixels.get(pixelIndex).position.add(cameraLocalFlow);
        moshedPixels.get(pixelIndex).display();
        pixelIndex++;
      }
    }
  } else { // Plays camera feed until sketch can get first PFrame
    set(0, 0, camera);
  } 

  if (frameCount % IFRAME_COLLECTION_RATE == 0) {
    set(0, 0, camera); // Display new PFrame
    init_IFrame(camera); // Make Pixels
    recievedIFrame = true;
  }
}

void captureEvent(Capture c) {
  c.read();
}