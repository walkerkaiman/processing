void init_IFrame (PImage frame) {
  moshedPixels = new ArrayList<Pixel>();
  frame.loadPixels();
  
  for (int posY = 0; posY < frame.height; posY += PIXEL_SIZE) {
    for (int posX = 0; posX < frame.width; posX += PIXEL_SIZE) {
      PVector position = new PVector(posX, posY);
      color fill = frame.get(posX, posY);

      moshedPixels.add(new Pixel(position, fill));
    }
  }
}