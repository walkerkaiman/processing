import processing.video.*;
import gab.opencv.*;
import java.awt.*;

PImage dst;
Capture video;
OpenCV opencv;

ArrayList<Contour> contours;
ArrayList<Contour> polygons;

void setup() {
  size(1080, 360);
  
  video = new Capture(this, width/2, height);
  opencv = new OpenCV(this, video.width, video.height);
  
  video.start();
}

void draw() {
  opencv.gray();
  opencv.threshold(70);
  opencv.loadImage(video);
  dst = opencv.getOutput();

  contours = opencv.findContours();
  
  noFill();
  strokeWeight(3);
  
  for (Contour contour : contours) {
    stroke(0, 255, 0);
    contour.draw();
    
    stroke(255, 0, 0);
    beginShape();
    for (PVector point : contour.getPolygonApproximation().getPoints()) {
      vertex(point.x, point.y);
    }
    endShape();
  }
  image(video, 0, 0);
  image(dst, video.width, 0);
  
}

void captureEvent(Capture c){
  c.read();
}