import gab.opencv.*;
import processing.video.*;
import java.awt.*;

Capture video;
OpenCV opencv;

void setup() {
  size(640, 480);
  noFill();
  strokeWeight(3);
  
  video = new Capture(this, width/2, height/2);
  opencv = new OpenCV(this, width/2, height/2);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  

  video.start();
}

void draw() {
  background(0);
  
  scale(2);
  opencv.loadImage(video);

  image(video, 0, 0);
  
  Rectangle[] faces = opencv.detect();

  for(int i = 0; i < faces.length; i++){
    stroke(0, 255, 0);
    rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
    
    stroke(255, 0, 0);
    point(faces[i].x+faces[i].width/2, faces[i].y+faces[i].height/2);
  }
}

void captureEvent(Capture c){
  c.read();
}