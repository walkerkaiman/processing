import processing.video.*;
import codeanticode.syphon.*;

import gab.opencv.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Core;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.CvType;

import org.opencv.core.Point;
import org.opencv.core.Size;

//import java.util.list;

OpenCV opencv;
SyphonServer server;
Capture camera;

PGraphics canvas;
PImage  src, dst, markerImg;
ArrayList<MatOfPoint> contours;
ArrayList<MatOfPoint2f> approximations;
ArrayList<MatOfPoint2f> markers;

boolean[][] markerCells;

void settings() {
  size(1000, 365, OPENGL);
  PJOGL.profile=1;
}

void setup() {
  camera = new Capture(this, width, height);
  camera.start();
  opencv = new OpenCV(this, "marker_test.jpg");
  server = new SyphonServer(this, "Processing Syphon");
  canvas = createGraphics(width, height, OPENGL);
}

void draw() {
  
  if (camera.available()) {
    camera.read();
  }
  //opencv.loadImage(camera);

  // hold on to this for later, since adaptiveThreshold is destructive
  Mat gray = OpenCV.imitate(opencv.getGray());
  opencv.getGray().copyTo(gray);

  Mat thresholdMat = OpenCV.imitate(opencv.getGray());

  opencv.blur(5);
  
  Imgproc.adaptiveThreshold(opencv.getGray(), thresholdMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 451, -65);

  contours = new ArrayList<MatOfPoint>();
  Imgproc.findContours(thresholdMat, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);

  approximations = createPolygonApproximations(contours);

  markers = new ArrayList<MatOfPoint2f>();
  markers = selectMarkers(approximations);

  MatOfPoint2f canonicalMarker = new MatOfPoint2f();
  Point[] canonicalPoints = new Point[4];
  canonicalPoints[0] = new Point(0, 350);
  canonicalPoints[1] = new Point(0, 0);
  canonicalPoints[2] = new Point(350, 0);
  canonicalPoints[3] = new Point(350, 350);
  canonicalMarker.fromArray(canonicalPoints);

  Mat transform = Imgproc.getPerspectiveTransform(markers.get(0), canonicalMarker);
  Mat unWarpedMarker = new Mat(50, 50, CvType.CV_8UC1);  
  Imgproc.warpPerspective(gray, unWarpedMarker, transform, new Size(350, 350));

  Imgproc.threshold(unWarpedMarker, unWarpedMarker, 125, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);

  float cellSize = 350/7.0;

  markerCells = new boolean[7][7];

  for (int row = 0; row < 7; row++) {
    for (int col = 0; col < 7; col++) {
      int cellX = int(col*cellSize);
      int cellY = int(row*cellSize);

      Mat cell = unWarpedMarker.submat(cellX, cellX +(int)cellSize, cellY, cellY+ (int)cellSize); 
      markerCells[row][col] = (Core.countNonZero(cell) > (cellSize*cellSize)/2);
    }
  }

  dst  = createImage(350, 350, RGB);
  opencv.toPImage(unWarpedMarker, dst);
  
  
  
  
  
  
  
  canvas.beginDraw();
    canvas.pushMatrix();
      canvas.background(125);
      canvas.scale(0.5);
    
      canvas.noFill();
      canvas.smooth();
      canvas.strokeWeight(5);
      canvas.stroke(0, 255, 0);
      drawContours2f(markers);  
    canvas.popMatrix();
  
    canvas.pushMatrix();
      canvas.translate(width/2, 0);
      canvas.strokeWeight(1);
      canvas.image(dst, 0, 0);
    
      cellSize = dst.width/7.0;
      
      for (int col = 0; col < 7; col++) {
        for (int row = 0; row < 7; row++) {
          if(markerCells[row][col]){
            canvas.fill(255);
          } else {
            canvas.fill(0);
          }
          canvas.stroke(0,255,0);
          canvas.rect(col*cellSize, row*cellSize, cellSize, cellSize);
        }
      }
    
    canvas.popMatrix();
    canvas.rotate(radians(180));
  canvas.endDraw();
  
  image(canvas, 0, 0);
  
  server.sendImage(canvas);
}



//Methods

ArrayList<MatOfPoint2f> selectMarkers(ArrayList<MatOfPoint2f> candidates) {
  
  float minAllowedContourSide = 50;
  minAllowedContourSide = minAllowedContourSide * minAllowedContourSide;

  ArrayList<MatOfPoint2f> result = new ArrayList<MatOfPoint2f>();

  for(MatOfPoint2f candidate : candidates) {

    if (candidate.size().height != 4) {
      continue;
    } 

    if (!Imgproc.isContourConvex(new MatOfPoint(candidate.toArray()))) {
      continue;
    }

    // eliminate markers where consecutive
    // points are too close together
    float minDist = width * width;
    
    Point[] points = candidate.toArray();
    
    for(int i = 0; i < points.length; i++) {
      
      Point side = new Point(points[i].x - points[(i+1)%4].x, points[i].y - points[(i+1)%4].y);
      float squaredLength = (float)side.dot(side);
      // println("minDist: " + minDist  + " squaredLength: " +squaredLength);
      minDist = min(minDist, squaredLength);
    }


    if (minDist < minAllowedContourSide) {
      continue;
    }

    result.add(candidate);
  }

  return result;
}

ArrayList<MatOfPoint2f> createPolygonApproximations(ArrayList<MatOfPoint> cntrs) {
  ArrayList<MatOfPoint2f> result = new ArrayList<MatOfPoint2f>();

  double epsilon = cntrs.get(0).size().height * 0.01;
  println(epsilon);

  for (MatOfPoint contour : cntrs) {
    MatOfPoint2f approx = new MatOfPoint2f();
    Imgproc.approxPolyDP(new MatOfPoint2f(contour.toArray()), approx, epsilon, true);
    result.add(approx);
  }

  return result;
}

void drawContours(ArrayList<MatOfPoint> cntrs) {
  for (MatOfPoint contour : cntrs) {
    canvas.beginShape();
    Point[] points = contour.toArray();
    for (int i = 0; i < points.length; i++) {
      canvas.vertex((float)points[i].x, (float)points[i].y);
    }
    canvas.endShape();
  }
}

void drawContours2f(ArrayList<MatOfPoint2f> cntrs) {
    for (MatOfPoint2f contour : cntrs) {
      canvas.beginShape();
      Point[] points = contour.toArray();
  
      for (int i = 0; i < points.length; i++) {
        canvas.vertex((float)points[i].x, (float)points[i].y);
      }
      canvas.endShape(CLOSE);
    }
}