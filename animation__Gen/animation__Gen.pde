float xstart, xnoise, ystart, ynoise, xstartNoise, ystartNoise;

void setup(){
  size(500, 500);
  //frameRate(24);
  
  xstartNoise = random(20);
  ystartNoise = random(20);
  xstart = random(10);
  ystart = random(10);
}

void draw(){
  background(255);
  
  xstartNoise += .01;
  ystartNoise += .01;
  
  xstart += (noise(xstartNoise) * .5 ) - .25;
  ystart += (noise(ystartNoise) * .5 ) - .25;
  
  xnoise = xstart;
  ynoise = ystart;
  
  for(int y = 0; y <= height; y += 10){
    ynoise += .1;
    xnoise = xstart;
    
    for(int x = 0; x <= width; x += 10){
      xnoise += .1;
      drawPoint(x, y, noise(xnoise, ynoise));
    }
  }
}

void drawPoint(float x, float y, float noiseFactor){
  
  pushMatrix();
    translate(x, y);
    rotate(radians(360) * noiseFactor);
    stroke(0, 150);
    line(0, 0, 10, 0);
  popMatrix();
}
