import processing.serial.*;
import cc.arduino.*;
import themidibus.*;

MidiBus midi;
Arduino teensy;

float minResist = 800;
float maxResist = 1023;
int minNote = 0;
int maxNote = 127;

final color white = color (255);
final color black = color (0);
final int sensorPin = 0;
final int serialDelay = 200;
final int noteLength = 100;
int currentNote, prevNote;

void setup() {
  size(300, 150);

  println(Arduino.list());
  teensy = new Arduino(this, "/dev/tty.usbmodem1411", 57600);
  println("USB Device Connected");
  teensy.pinMode(sensorPin, Arduino.INPUT);
  
  midi = new MidiBus(this, -1, 1);
  
  stroke(white);
  fill(white);
  textSize(20);
}

void draw() {
  update();
  
  // If shoe is at rest...
  if (currentNote == maxNote) {
    for (int note = 0; note < 127; note++) {
      midi.sendNoteOff(0, note, 127);
    }
  }
  else{
    if (prevNote != currentNote) {
      sendMIDI();
    }
  }
  
  prevNote = currentNote;
  display();
}

void update() {
  float sensorValue = teensy.analogRead(sensorPin);
  //println(sensorValue);
  
  float mappedValue = map(sensorValue, minResist, maxResist, minNote, maxNote);
  currentNote = int(mappedValue);
  delay(serialDelay);
}

void display () {
  background(black);
  text("Shoe MIDI Note: " + currentNote, 60, height/2);
}

void sendMIDI () {
  midi.sendNoteOn(0, currentNote, 127);
}