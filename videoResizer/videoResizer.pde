import processing.video.*;
Movie movie;

void setup(){
  size(640, 480);
  frameRate(24);
  
  movie = new Movie(this, "2.mov");
  movie.loop();
  movie.volume(0);
}

void draw(){
  
  if(movie.available()){
    movie.read();
  }
  
  image(movie, 0, 0, width, height);
  
  if(frameCount < movie.duration()*24){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.png");
  }
}