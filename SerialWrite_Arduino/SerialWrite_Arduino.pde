/*
*___SERIAL SERVER___
 *This sketch is used to send discrete positions for a 
 *servo motor via serial communication. 
 *
 *It's the sketch's job to format the numbers in a range of
 *5-140 if used with a HiTech HS-785HB.
 *
 *The mouse's y position streams values between 5 and 140.
 *The bottom of the sketch has a place to set direct positions
 *to the servo motor.
 */

import processing.serial.*;

Serial serial;  
int servoPos;
//float time;

void setup() {
  size(150, 300);

  String portName = Serial.list()[2];
  printArray(Serial.list());

  serial = new Serial(this, portName, 9600);
}

void draw() {
  background(0);
  
  if (mouseY != pmouseY) {
    servoPos = (int)map(mouseY, 0, height, 5, 140);
  }
  
  //servoPos = int(noise(time)*140);
  
  serial.write(servoPos);
  displayPercent();
  
  time += .01;
}

void keyPressed() {
  switch(key) {
  case 'q':
    servoPos = 20;
    break;
  case 'w':
    servoPos = 40;
    break;
  case 'e':
    servoPos = 60;
    break;
  case 'r':
    servoPos = 80;
    break;
  }
}

void displayPercent() {
  stroke(255);

  int percent = int(100 - map(servoPos, 5, 140, 0, 100));
  text(percent + " %", width/2, height/2);
}