import peasy.*;
PeasyCam cam;
Mountain range;

ArrayList<newBuilding> building = new ArrayList<newBuilding>();
int buildingPopulation = 200;
float rabbitX;

void setup(){
  size(displayWidth-100, displayHeight-100, P3D);
  
  // Mountain(xStart, xEnd, zStart, zEnd, resolution, peakHeight);
  range = new Mountain(0, width*10, -5000, -6000, 50, 5000);
  cam = new PeasyCam(this, 1000);
  //cam.setMaximumDistance(-6000);

  for(int i = 0; i < buildingPopulation; i++){
    building.add(new newBuilding(random(width*10)));
  }
  
  for(newBuilding block : building){
    block.generateBuilding();
    block.generateDoor();
  }
  
  range.compute();
}

void draw(){
  
  background(0);
  
  translate(-width/5, -500, 0);
  range.display();
  displayFloor();
  
  for(newBuilding block : building){
    block.displayDoor();
    block.displayBuilding();
  }
  cam.pan(rabbitX, 0);
  rabbitX += 1;
  
  //saveFrame("/Users/kaimanwalker/Desktop/city/cityFrame-####.png");
}

void displayFloor(){
  fill(0);
  noStroke();

  beginShape();
    vertex(0, 1001, -5000);
    vertex(0, 1001, 0);
    vertex(width*10, 1001, 0);
    vertex(width*10, 1001, -5000);
  endShape(CLOSE);
  
  stroke(255);
  
  for(int i = 0; i < width*10; i+=100){
    line(i, 1000, -5000, i, 1000, 1000);
  }
}
