class newBuilding {

  float buildingWidth, buildingHeight, buildingX, buildingY, doorX, doorY, z;
  float doorWidth = 12;
  float doorHeight = 20;
  
  ArrayList <PVector> doorPos = new ArrayList<PVector>();
  
  newBuilding(float xPos) {
     buildingX = xPos;
     z = random(-5000, 0);
  }
  
  void generateBuilding(){
    buildingWidth = random(100, 400);
    buildingHeight = random(400, 1000);
    buildingY = buildingY+(1000-buildingHeight);
  }
  
  void generateDoor(){

    doorX = 20 + buildingX;
    doorY = 100 + buildingY;
    
      while(doorX + doorWidth + 20 < buildingX + buildingWidth){
         doorPos.add(new PVector(doorX, doorY, z));
         doorX += doorWidth + random(50);
         
         if(doorX + doorWidth + 20 > buildingX + buildingWidth){
           doorX = 20 + random(20) + buildingX;
           doorY += doorHeight + 10;
         }
         
         if(doorY+doorHeight > buildingY+buildingHeight){
            break;
         }
      } 
  }
  
  void displayDoor(){
    
    strokeWeight(1);
    stroke(255);
    fill(0);
    
    for(PVector door : doorPos){
      
      // Door
      beginShape();
        vertex(door.x, door.y, door.z);
        vertex(door.x + doorWidth, door.y, door.z);
        vertex(door.x + doorWidth, door.y + doorHeight, door.z);
        vertex(door.x, door.y + doorHeight, door.z);
      endShape(CLOSE);
    }
  }
  
  void displayBuilding(){
    
  strokeWeight(1);
  stroke(255);
  fill(0);
  
    // Building Border
    beginShape();
      vertex(buildingX, buildingY, z);
      vertex(buildingX + buildingWidth, buildingY, z);
      vertex(buildingX + buildingWidth, buildingY + buildingHeight, z);
      vertex(buildingX, buildingY + buildingHeight, z);
    endShape(CLOSE);
    
    // Antenna
    line(buildingX + 50, buildingY, z, buildingX + 50, buildingY - 100, z);
}


}
