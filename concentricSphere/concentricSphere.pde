import peasy.*;
PeasyCam cam;

float rotX_step, rotY_step, rotZ_step;
float rotX, rotY, rotZ;
float min, max;
color a, b;
boolean recording;

void setup(){
  size(500, 500, P3D);
  frameRate(30);
  
  cam = new PeasyCam(this, 1000);
  
  noFill();
  strokeWeight(2);
  
  rotX_step = random(.1);
  rotY_step = random(.1);
  rotZ_step = random(.1);
  
  min = 3;
  max = 700;
  
  a = color(8, 255, 241);
  b = color(255, 8, 252);
  
  recording = false;
}

void draw(){
  background(0);
  
  for(float i = min; i <= max; i += 100){
    int detail = 23 - int(map(i, min, max, min, 20));
    float R = map(i, min, max, red(a), red(b));
    float G = map(i, min, max, green(a), green(b));
    float B = map(i, min, max, blue(a), blue(b));
    
    sphereDetail(detail);
    stroke(R, G, B, 150);
    
    pushMatrix();
      rotateX(rotX);
      rotateY(rotY);
      rotateZ(rotZ);
      sphere(i);
    popMatrix();
  }
  rotX += rotX_step;
  rotY += rotY_step;
  rotZ += rotZ_step;
  
  if(recording){
    record();
  }
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}

void record(){
  saveFrame("/Users/kaimanWalker/Desktop/frames/frame-#####.tiff");
}