class Range {
  ArrayList<PVector> vertex;
  float xStart, xEnd, yStart, yEnd;
  float resolution, speed;

  Range(float temp_xStart, float temp_xEnd, float temp_yStart, float temp_yEnd, float temp_resolution) {
    
    xStart = temp_xStart;
    xEnd = temp_xEnd;
    yStart = temp_yStart;
    yEnd = temp_yEnd;
    resolution = temp_resolution;
    
    init();
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    
    for(int i = 0; i < vertex.size(); i ++){
      PVector v = vertex.get(i);
      v.y -= resolution;
      
      if(v.y < yStart){
        float z = noise(v.x/100, yStart/100, time) * scale;
        
        vertex.add(new PVector(v.x, yEnd, z));
        vertex.remove(i);
        
        continue;
      }
      
    }
  }
  
  void display(){
    for(PVector v : vertex){
      point(v.x, v.y, v.z);
    }
  }
  
  void init(){
    vertex = new ArrayList<PVector>();
      
    for (float y = yStart; y < yEnd; y += resolution) {
      for (float x = xStart; x < xEnd; x += resolution) {
        float z = noise(x/100, y/100, time) * scale;

        vertex.add(new PVector(x, y, z));
      }
    }
  }
  
  
  
}