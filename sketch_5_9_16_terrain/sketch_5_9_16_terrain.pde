import peasy.*;

PeasyCam cam;
Range range;

float scale = 150;
float time;
boolean recording;

void setup() {
  size(600, 600, P3D);
  frameRate(24);
  stroke(255, 150);
  strokeWeight(2);
  
  cam = new PeasyCam(this, 1000);
  range = new Range(0, 600, 0, 600, 4);
  recording = false;
}

void draw() {
  background(0);
  
  range.run();
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_######.tiff");
  }

  time += .1;
}

void keyPressed(){
  if(key == 'r' || key == 'R'){
    recording =! recording;
    println("Recording: " + recording);
  }
}