class Edge{
  PVector startPos, endPos;
  int animationSeed;
  
  Edge(PVector temp_startPos, PVector temp_endPos, int temp_animationSeed){
    
    startPos = temp_startPos;
    endPos = temp_endPos;
    animationSeed = temp_animationSeed;
  }

  void display(){
    float weight = abs(cos(rabbit + animationSeed) * girth);
    
    canvas.beginDraw();
    canvas.pushStyle();
      canvas.stroke(255);
      canvas.strokeWeight(weight);
      
      canvas.line(startPos.x, startPos.y, endPos.x, endPos.y);
    canvas.popStyle();
    canvas.endDraw();
  }
}
