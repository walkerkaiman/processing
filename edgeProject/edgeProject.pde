import codeanticode.syphon.*;
SyphonServer server;
PGraphics canvas;

ArrayList<Edge> edgeList = new ArrayList<Edge>();
PVector mousePos, startPos;
boolean edgy;
float rabbit, girth;

void setup(){
  size(640, 480, P2D);
  server = new SyphonServer(this, "Fine Time");
  canvas = createGraphics(width, height, P2D);
  girth = 4;
}

void draw(){
  mousePos = new PVector(mouseX, mouseY); 
  
  canvas.beginDraw();
  canvas.background(0);
  
  for(Edge currentEdge : edgeList){
    currentEdge.display();
  }
  
  if(edgy){
    canvas.stroke(255);
    canvas.strokeWeight(2);
    
    canvas.line(startPos.x, startPos.y, mousePos.x, mousePos.y);
  }
  canvas.endDraw();
  
  displayCrosshair();
  rabbit += .01;
  
  image(canvas, 0, 0);
  server.sendImage(canvas);
}

void mousePressed(){
  startPos = mousePos;
  edgy = true;
}

void mouseReleased(){
  PVector endPos = mousePos;
  edgy = false;
  
  // Edge(point A, point B, Animation Seed)
  edgeList.add(new Edge(startPos, endPos, edgeList.size()));
}

void keyPressed(){
  if(key == BACKSPACE && edgeList.size() > 0){
    edgeList.remove(edgeList.size()-1);
  }
  if(key == CODED){
    if(keyCode == UP){
      girth += .2;
    }
    if(keyCode == DOWN && girth > 0){
      girth -= .2;
    }
  }
}

void displayCrosshair(){
  canvas.beginDraw();
  canvas.pushStyle();
    canvas.stroke(255);
    canvas.strokeWeight(2);
    
    canvas.line(mousePos.x-10, mousePos.y-10, mousePos.x+10, mousePos.y+10);
    canvas.line(mousePos.x+10, mousePos.y-10, mousePos.x-10, mousePos.y+10);
  canvas.popStyle();
  canvas.endDraw();
}
