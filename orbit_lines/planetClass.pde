class Planet{
  PVector localPos;
  float xPos, yPos, mag, angle, angleOffset, speed;
  
  Planet(float temp_x, float temp_y, float temp_mag, float temp_angOffset, float temp_speed){
    xPos = temp_x;
    yPos = temp_y;
    mag = temp_mag;
    angleOffset = temp_angOffset;
    speed = temp_speed;
    
    localPos = new PVector(cos(radians(angle + angleOffset)) * mag + xPos, sin(radians(angle + angleOffset)) * mag + yPos);
  }
  
  void update(){
    
    localPos.x = cos(radians(angle + angleOffset)) * mag + xPos;
    localPos.y = sin(radians(angle + angleOffset)) * mag + yPos;
    
    if(angle < 360 && angle > 0){
    angle += speed;
    }
    else{
    angle = 0;
    angle += speed;
    }

  }
  
  void displayPlanet(){
    noFill();
    stroke(0);
    strokeWeight(1);
    
    ellipse(localPos.x, localPos.y, 5, 5);
  }
    
  void displayLines(){
    
    for(PVector i : planetsPosition){
      
      float distance = dist(i.x, i.y, localPos.x, localPos.y);
      float c = map(distance, 0, lineThresh, 0, 255);
      
      stroke(c, 200);
      
      if(distance < lineThresh && distance > 1){
        line(i.x, i.y, localPos.x, localPos.y);
      }
    } 
  }

}
