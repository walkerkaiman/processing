ArrayList <Planet> planets = new ArrayList<Planet>();
ArrayList <PVector> planetsPosition = new ArrayList<PVector>();

int planetPopulation = 20;
float lineThresh = 70;

void setup(){
  size(400, 400);
  background(255);
  
  for(int i = 0; i < planetPopulation; i++){ 
    planets.add(new Planet(random(width), random(height), random(1, 60), random(90), random(-2, 2)));
    planetsPosition.add(new PVector());
  }
}

void draw(){
  background(255);
  
  for(Planet i : planets){
    i.update();
    i.displayLines();
    i.displayPlanet();
  }
  // Updates planet position and stores to array list.
  for(int i = 0; i < planets.size(); i++){
    planetsPosition.set(i, planets.get(i).localPos);
  }
}