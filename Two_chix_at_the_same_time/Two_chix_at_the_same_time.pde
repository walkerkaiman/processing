import peasy.*;
import SimpleOpenNI.*;

PeasyCam peasy;
SimpleOpenNI cam1;
SimpleOpenNI cam2;

int step = 5;

void setup()
{
  size(640, 480, P3D); 

  SimpleOpenNI.start();
  
  peasy = new PeasyCam(this, width);
  cam1 = new SimpleOpenNI(0, this);
  cam2 = new SimpleOpenNI(1, this);
  cam1.enableDepth();
  cam2.enableDepth();
  
  background(0);
  stroke(255);
}

void draw(){
  
  frame.setTitle("/// MultiCamera Depth Cloud ///    " + int(frameRate) + " fps");
  
  cam1.update();
  cam2.update();
  
  int[] depthMap1 = cam1.depthMap();
  int[] depthMap2 = cam2.depthMap();
  
  PVector realWorldPoint1;
  PVector realWorldPoint2;
  
  int index2;
  int index1;
  
  background(0);
  
  rotateX(radians(180.0));
  rotateY(radians(0.0));
  translate(0, 0, -500);
 
  
//Depth Image from Camera 1
  beginShape(POINTS);
    
    for(int x = 0; x < cam1.depthWidth(); x += step){
      for(int y = 0; y < cam1.depthHeight(); y += step){
        
        index1 = x + y * cam1.depthWidth();
        
        if(depthMap1[index1] > 0){
          
          realWorldPoint1 = cam1.depthMapRealWorld()[index1];
          
          //if(realWorldPoint1.z < 1000){
            stroke(255, 0, 0);
            vertex(realWorldPoint1.x, realWorldPoint1.y, realWorldPoint1.z);
          //}
        }
      }
    }
    
  endShape();
  
  rotateY(radians(-90.0));
  translate(750, -70, -800);
//Depth Image from Camera 2
  beginShape(POINTS);
    
    for(int x = 0; x < cam2.depthWidth(); x += step){
      for(int y = 0; y < cam2.depthHeight(); y += step){
        
        index2 = x + y * cam2.depthWidth();
        
        if(depthMap2[index2] > 0){
          
          realWorldPoint2 = cam2.depthMapRealWorld()[index2];
          
          //if(realWorldPoint2.z < 1000){
            stroke(0, 255, 0);
            vertex(realWorldPoint2.x, realWorldPoint2.y, realWorldPoint2.z);
          //}
        }
      }
    }
    
  endShape();
  }
