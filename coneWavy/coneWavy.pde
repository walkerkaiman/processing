import peasy.*;
PeasyCam cam;

float zEnd = -1000;
float time;

void setup(){
  size(600, 600, P3D);
  cam = new PeasyCam(this, 1000);
  
  noFill();
  stroke(255, 10);
  strokeWeight(2);
}
void draw(){
  background(0);
  
  for(float z = 0; z > zEnd; z -= 2){
     beginShape();
     for(float angle = 0; angle <= 360; angle += 5){
       float mag = map(z, 0, zEnd, 300, 0);
       float x = cos(radians(angle)) * mag;
       float y = sin(radians(angle)) * mag;
       float offset = noise(time + x/100 + y/100)*100;
       
       vertex(x, y, z+offset);
     } 
     endShape();
  }
  
  time += .1;
  
  if(frameCount > 500 && frameCount < 1000){
    println("Recording...");
    saveFrame("/Users/Kman/Desktop/frames/frame-####.png");
  }
  else{println("Not Recording...");}
  
}
