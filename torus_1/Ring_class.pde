class Ring{
  PVector pos;
  float diameter, size;
  int index;
  
  Ring(int temp_index){
    index = temp_index;
    
    size = 600;
    pos = new PVector(width/2, height/2, 0);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    float preScale = cos(time + index/3);
    
    diameter = cos(preScale) * size-300;
    pos.z = cos(sin(preScale) + sin(time + index/3)) * 200;
  }
  
  void display(){
    float weight = map(abs(diameter), 24, size-300, 4, 10);
    float r = map(abs(diameter), 24, size-300, 255, 0);
    float g = map(abs(diameter), 24, size-300, 255, 0);
    float b = map(abs(diameter), 24, size-300, 255, 0);
    float a = map(abs(diameter), 24, size-350, 255, 0);
    
    strokeWeight(weight);
    stroke(255, a);
    noFill();
    
    pushMatrix();
      translate(pos.x, pos.y, pos.z);
      ellipse(0, 0, diameter, diameter);
    popMatrix();
  }
}