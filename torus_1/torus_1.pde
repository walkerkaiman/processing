import peasy.*;
PeasyCam cam;

ArrayList<Ring> rings; 
ArrayList<Orbit> orbits;
int ringPop, orbitPop;
float time;
boolean recording;

void setup(){
  size(640, 480, P3D);
  frameRate(30);
  sphereDetail(5);
  
  cam = new PeasyCam(this, 1000);
  rings = new ArrayList <Ring>();
  orbits = new ArrayList <Orbit>();
  ringPop = 70;
  orbitPop = 10;
  
  for(int i = 0; i < orbitPop; i++){
    float cx = width/2;
    float cy = height/2;
    float cz = random(50, 150);
    
    orbits.add(new Orbit(cx, cy, cz, i));
  }
  
  for(int i = 0; i < ringPop; i++){
    rings.add(new Ring(i));
  }
}

void draw(){
  background(0);
  
  for(Ring r : rings){
    r.run();
  }
  
  for(Orbit o : orbits){
    o.run();
  }

  time += .03;
  record();
}

void record(){
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.tiff");
  }
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}