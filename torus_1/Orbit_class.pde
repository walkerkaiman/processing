class Orbit{
  PVector pos, center;
  int index;
  float orbitSize, size, spinAmount, spin;
  
  Orbit(float temp_x, float temp_y, float temp_z, int temp_index){
    index = temp_index;
    center = new PVector(temp_x, temp_y, temp_z);
    orbitSize = random(50, 100);
    size = random(5, 20);
    spinAmount = radians(random(10));
    pos = new PVector(0, 0, 0);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    pos.x = center.x + orbitSize * cos(time + index);
    pos.y = center.y + orbitSize * sin(time + index);
    spin += spinAmount;
  }
  
  void display(){
    fill(255);
    stroke(0);
    strokeWeight(2);
    
    pushMatrix();
      translate(pos.x, pos.y, center.z);
      rotateZ(spin);
      sphere(size);
    popMatrix();
  }
}