import processing.video.*;
Movie video;

ArrayList<PImage> frames = new ArrayList<PImage>();
int widthMin, heightMin, framePop;

void setup(){
  size(1280, 720);
  //frameRate(24);
  background(0);
  
  video = new Movie(this, "bridge1.mov");
  video.play();
  
  widthMin = 320;
  heightMin = 240;
  
  framePop = int((width-widthMin)/100)*5;
}

void movieEvent(Movie video) {
  video.read();
  
  if(frameCount < framePop && frameCount % 5 == 0){
    frames.add(video);
  }
  else{
    if(frameCount % 5 == 0){
      frames.remove(0);
      frames.add(video);
    }
  }
}

void draw(){
  if(frames.size() > 0){
    image(frames.get(frames.size()-1), 0, 0, width/2, height/2);
    image(frames.get(0), width/2, 0, width/2, height/2);
  }
  
}