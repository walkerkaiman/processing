PImage img1, img2;
ArrayList<PImage>effected;

int bars, marginX, marginY;

int barHeight = 20;
int spaceDist = 15; 

void setup(){
  size(570, 693);
  
  img1 = loadImage("8.jpg");
  img2 = loadImage("8.jpg");
  effected = new ArrayList<PImage>();
  
  img1.resize(width, height);
  img2.resize(width-50, 0);
  img1.filter(GRAY);
  img2.filter(GRAY);
  
  bars = img2.height/barHeight;
  marginX = (img1.width-img2.width)/2;
  marginY = (img1.height-(img2.height+(bars-1)*spaceDist))/2;
  
  for(int i = 0; i < bars; i++){
    effected.add(new PImage(img2.width, barHeight));
  }
}

void draw(){
  noLoop();
  
  //Displays background img1.
  background(img1);
  
  //Cuts img2 into slices and puts it into the ArrayList, effected.
  int index = 0;
  
  for(int barCurrent = 0; barCurrent < bars; barCurrent++){
    int barY = barCurrent*barHeight;
    
    effected.get(index).loadPixels();

    for(int y = 0; y < barHeight; y++){
      for(int x = 0; x < img2.width; x++){
        effected.get(index).pixels[x+y*img2.width] = img2.pixels[x+(y+barY)*img2.width];
      }
    }
    
    effected.get(index).updatePixels();
    index ++;
  }
  
  //Displays all of the images in effected with a Y-axis offset.
  for(int i = 0; i < effected.size(); i++){
    int offset = i * spaceDist;
    int barY = i * barHeight;
   
    image(effected.get(i), marginX, offset+barY+marginY);
  }
  
  saveFrame("/Users/kaimanwalker/Desktop/frame"+second()+".png");
  exit();
}