import peasy.*;
PeasyCam cam;

ArrayList<Shape> shapes = new ArrayList<Shape>();

int pop = 20;

void setup(){
  size(640, 480, P3D);
  frameRate(40);
  noFill();
  
  cam  = new PeasyCam(this, 1000);
  
  for(int i = 0; i < pop; i++){
    float temp_size = i*20;
    float ang = i * 20;

    shapes.add(new Shape(temp_size, ang));
  }
}

void draw(){
  background(0);
  
  for(Shape current : shapes){
    current.display();
  }
  //saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.tiff");
}