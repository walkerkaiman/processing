class Shape{
  float size, rotate;
  
  Shape(float temp_size, float temp_rotate){
    size = temp_size;
    rotate = temp_rotate;
  }
  
  void display(){
    rotate += 5;
    
    pushMatrix();
      translate(width/2, height/2);
      rotateY(radians(rotate));
      
      strokeWeight(3);
      stroke(255);
  
      ellipse(0, 0, size, size);
    popMatrix();
  }
  
}