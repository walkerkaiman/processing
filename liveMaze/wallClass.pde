class Wall{
  PVector pos;
  float size, speed;
  color fill;
  
  Wall(float temp_x, float temp_y){
    pos = new PVector(temp_x, temp_y);
    size = .5;
    fill = img.get(int(pos.x), int(pos.y));
    speed = random(.1, size);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    if(keyPressed && key == CODED){
      switch(keyCode){
        case UP:
          pos.y -= speed;
          break;
        case DOWN:
          pos.y += speed;
          break;
        case LEFT:
          pos.x -= speed;
          break;
        case RIGHT:
          pos.x += speed;
          break;
      }
    }
    
    checkEdge();
  }
  
  void display(){
    noStroke();
    fill(fill);
    rect(pos.x, pos.y, size, size);
  }
  
  void checkEdge(){
    if(pos.x < 0){pos.x = width;}
    if(pos.x > width){pos.x = 0;}
    if(pos.y < 0){pos.y = height;}
    if(pos.y > height){pos.y = 0;}
  }
}