PImage img;
int population = 10000;
Wall[] walls = new Wall[population];

void setup(){
  size(640, 480);
  frameRate(24);
  
  img = loadImage("3.jpg");
  image(img, 0, 0, width, height);
  
  for(int index = 0; index < walls.length; index++){
    walls[index] = new Wall(random(width), random(height));
  }
}

void draw(){
  
  if(keyPressed){
    for(Wall current : walls){
      current.run();
    }
  }
  
  record();
}

void record(){
  if(frameCount < 360){
    println("Recording...");
    //saveFrame("/Users/kaimanwalker/Desktop/frames/frame-####.tiff");
  }
  else{
    println("Not Recording...");
  }
}