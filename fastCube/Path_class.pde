class Path{
  Orb[] orbs;
  PVector[] vertex;
  PVector uniquePos;
  int edgeLength;
  float orbSize;
  
  Path(int temp_edgeLength, float temp_orbSize, PVector temp_uniquePos){
    edgeLength = temp_edgeLength;
    orbSize = temp_orbSize;
    uniquePos = temp_uniquePos;
    
    orbs = new Orb[8];
    vertex = new PVector[8];
    
    initVertex();
    
    for(int i = 0; i < orbs.length; i++){
      PVector vertexPos = vertex[i];
      orbs[i] = new Orb(orbSize, vertexPos);
    }
  }
  
  void run(){
    initVertex();
    runOrbs();
    //displayEdges();
  }
  
  void initVertex(){
    vertex[0] = new PVector(0, 0, 0);
    vertex[1] = new PVector(edgeLength, 0, 0);
    vertex[2] = new PVector(edgeLength, edgeLength, 0);
    vertex[3] = new PVector(0, edgeLength, 0);
    vertex[4] = new PVector(0, 0, -edgeLength);
    vertex[5] = new PVector(edgeLength, 0, -edgeLength);
    vertex[6] = new PVector(edgeLength, edgeLength, -edgeLength);
    vertex[7] = new PVector(0, edgeLength, -edgeLength);
    
    for(PVector v : vertex){
      v.add(uniquePos);
    }
  }
  
  void runOrbs(){
    for(Orb i : orbs){
      float d = i.targetDist();
        
      if(d < i.size && d < i.prevDist){
        int randomIndex = int(random(8));
        PVector randomVertex = vertex[randomIndex];
        
        i.target = randomVertex;
      }
      
      i.update();
      //i.display();
      i.displayTarget();
      
      d = i.prevDist;
    }
  }
  
  void displayVertex(){
    stroke(255, 0, 0);
    strokeWeight(10);
    
    for(PVector v : vertex){
      point(v.x, v.y, v.z);
    }
  }
  
  void displayEdges(){
    strokeWeight(2);
    stroke(255, 150);
    noFill();
    
    pushMatrix();
      translate(uniquePos.x+edgeLength/2, uniquePos.y+edgeLength/2, uniquePos.z-edgeLength/2);
      box(edgeLength);
    popMatrix();
  }
  
}