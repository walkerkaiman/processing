class Orb{
  PVector pos, vel, accel, target;
  float size, topSpeed, topForce, prevDist;
  
  Orb(float temp_size, PVector initPos){
    size = temp_size;
    pos = initPos;
    
    vel = new PVector(0, 0, 0);
    accel = new PVector(0, 0, 0);
    target = new PVector(0, 0, 0);
    
    topSpeed = 10;
    topForce = 50;
    prevDist = 100000;
  }
  
  void update(){
    PVector desired = PVector.sub(target, pos);
    PVector steer = PVector.sub(desired, vel);
    steer.limit(topForce);
    accel.add(steer);
    vel.add(accel);
    vel.limit(topSpeed);
    pos.add(vel);
    accel.mult(0);
  }
  
  void display(){
    stroke(255, 200);
    strokeWeight(1);
    fill(20);
  
    pushMatrix();
      translate(pos.x, pos.y, pos.z);
      sphere(size);
    popMatrix();
  }
  
  void displayTarget(){
    float stroke = dist(target.x, target.y, target.z, pos.x, pos.y, pos.z);

    stroke(255, stroke);
    line(pos.x, pos.y, pos.z, target.x, target.y, target.z);
  }
  
  float targetDist(){
    return dist(pos.x, pos.y, pos.z, target.x, target.y, target.z); 
  }
}