import peasy.*;

Path cube;
PeasyCam cam;
boolean recording;

ArrayList <Path> cubes;

void setup(){
  size(600, 600, P3D);
  sphereDetail(5);
  
  recording = false;
  cubes = new ArrayList<Path>();
  cam = new PeasyCam(this, 1000);
  
  int edgeLength = 300;
  int col = 10;
  int row = 10;
  
  for(int x = 0; x < col; x++){
    for(int y = 0; y < row; y++){
      float range = 1000;
      float xPos = x * edgeLength;
      float yPos = y * edgeLength;
      
      PVector unique = new PVector(xPos, yPos);
      //Path(int edgeLength, float orbSize, PVector uniquePos)
      cubes.add(new Path(edgeLength, 20, unique));
    }
  }
}

void draw(){
  background(0);
  
  for(Path i : cubes){
    i.run();
  }
  
  record();
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}

void record(){
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_######.tiff");
  }
}