import peasy.*;
PeasyCam cam;

ArrayList<Node> nodes = new ArrayList<Node>();

float size = 80;
float time;

void setup(){
  size(400, 400, P3D);
  cam = new PeasyCam(this, 1000);
  
  for(float level = 0; level <= size; level += 10){
    float temp_level = map(level, 0, size, 1, 360);
    float temp_radius = abs(cos(radians(temp_level)))*size;
    float step = map(level, 0, size, 15, 45);
    
    for(float deg = 0; deg < 360; deg += step){
      
      float temp_x = cos(radians(deg))*temp_radius;
      float temp_y = sin(radians(deg))*temp_radius;
      
      nodes.add(new Node(temp_x, temp_y, temp_radius, deg));
      //nodes.add(new Node(temp_x, temp_y, -temp_radius, deg));
    }
  }
  println(nodes.size());
}

void draw(){
  background(0);
  
  for(Node current : nodes){
    current.run();
  }
  
  //time += .01;
}