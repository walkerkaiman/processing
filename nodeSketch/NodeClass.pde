class Node{
  PVector pos1a, pos1b, pos2a, pos2b, center;
  float deg;
  
  Node(float x, float y, float z, float temp_deg){
    center = new PVector(x, y, z);
    
    pos1a = new PVector(center.x-10, center.y, z);
    pos1b = new PVector(center.x+10, center.y, z);
    
    pos2a = new PVector(center.x, center.y-10, z);
    pos2b = new PVector(center.x, center.y+10, z);
    
    deg = temp_deg;
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){

  }
  
  void display(){
    stroke(255);
    strokeWeight(3);
   
    point(center.x, center.y, center.z);
    //line(pos1a.x, pos1a.y, pos1a.z, pos1b.x, pos1b.y, pos1b.z);
    //line(pos2a.x, pos2a.y, pos2a.z, pos2b.x, pos2b.y, pos2b.z);
  }
  
  
}