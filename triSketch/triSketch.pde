ArrayList<Tri>triangles = new ArrayList<Tri>();
color white = color(255);
color black = color(0, 100);

float startRadius = 30;
float startAngle = 90;
float angStep = 2;
float radiusStep = 20;

void setup() {
  size(600, 600);
  stroke(black);
  noFill();
  
  for(float radius = startRadius; radius < width/2; radius += radiusStep) {
    for (float angle = startAngle; angle < 360; angle += angStep) {
      float degrees = radians(angle);
      float posX = cos(degrees)*radius + (width/2);
      float posY = sin(degrees)*radius + (height/2);
      
      triangles.add(new Tri(new PVector(posX, posY), radius/15));
    }
  }
}

void draw() {
  background(white);
 
  for(Tri t : triangles){
    t.display(); 
  }
}