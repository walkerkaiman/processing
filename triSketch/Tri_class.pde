class Tri{
  PVector center, a, b, c;
  float offset;
  
  Tri(PVector _center, float _offset){
    center = _center;
    offset = random(_offset);
    
    a = new PVector(center.x + random(-offset, offset), center.y + random(-offset, offset));
    b = new PVector(center.x + random(-offset, offset), center.y + random(-offset, offset));
    c = new PVector(center.x + random(-offset, offset), center.y + random(-offset, offset));
  }
  
  void display(){
    beginShape();
      vertex(a.x, a.y);
      vertex(b.x, b.y);
      vertex(c.x, c.y);
    endShape(CLOSE);
  }
}