import processing.video.*;
Capture cam;

float time;

int artifactPopulation = 500;
Artifact[] artList = new Artifact[artifactPopulation];

void setup(){
  size(640, 480);
  background(0);
  
  cam = new Capture(this, 640, 480, "FaceTime HD Camera", 24);
  cam.start();
  
  for(int i = 0; i < artList.length; i++){
    artList[i] = new Artifact();
  }
}

void draw(){
  if(cam.available()){
    cam.read();
  }
  
  set(0, 0, cam);
  
  cam.loadPixels();
    for(Artifact current : artList){
      current.run();
    }
  cam.updatePixels();
  
  if(frameCount < 360){
   //saveFrame("/Users/kaimanwalker/Desktop/frames/frame-####.tiff");
  }
  
  time += .1;
}