class Artifact{
  int wide, tall, xPos, yPos;
  PImage img;
  
  Artifact(){
    xPos = int(random(width));
    yPos = int(random(height));
    
    wide = int(random(5, 100));
    tall = int(random(5, 10));
    
    img = createImage(wide, tall, RGB);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    if(frameCount % 3 == 0){
      
      //Load pixels from video frame to Artifact's img
      img.loadPixels();
      int index = 0;
      for(int y = 0; y < tall; y++){
        for(int x = 0; x < wide; x++){
          img.pixels[index] = cam.get(xPos + x, yPos + y);
          index++;
        }
      }
      
      img.updatePixels();
      
      //Displace Artifact
      xPos = int(random(-wide, width));
      yPos = int(random(-tall, height));
    }
  }
  
  void display(){
    image(img, xPos, yPos);
  }
  
}