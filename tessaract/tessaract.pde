import peasy.*;
PeasyCam cam;

ArrayList<Side> sides;

void setup(){
  size(400, 400, P3D);
  
  cam = new PeasyCam(this, 1000);
  sides = new ArrayList<Side>();
  
  //Side(Sides, z, radius)
  sides.add(new Side(3, 0, 50));
}

void draw(){
  background(0);
  
  stroke(255);
  strokeWeight(2);
  noFill();
    
  for(Side i : sides){
    i.display();
  }
}