import processing.video.*;

Movie video;
ArrayList<PImage> frames = new ArrayList<PImage>();
int barHeight;

void setup() {
  size(1280, 960);
  frameRate(24);

  video = new Movie(this, "1.mov");
  video.loop();
  video.play();
  
  barHeight = 30;
}

void draw(){
  newFrame();
  video2Bar();
  //saveFrame("/Users/kaimanwalker/Desktop/framesTime/frame-####.tiff");
}

void newFrame(){
  int currentImage = 0;
  PImage img = createImage(width, height, RGB);
  
  video.read();
  video.loadPixels();
  arrayCopy(video.pixels, img.pixels);
  
  frames.add(img);
  
  if(frames.size() > height/barHeight){
    frames.remove(0);
  }
}

void video2Bar(){
  loadPixels();
  
  for(int y = video.height-barHeight; y >= 0; y -= barHeight){
    if(currentImage < frames.size()){
      PImage img = frames.get(currentImage);
      
      if(img != null){
        img.loadPixels();

        for(int x = 0; x < video.width; x++){
          for(int i = 0; i < barHeight; i++){
            pixels[x + ((y+i) * width)] = img.pixels[x + ((y+i) * video.width)];
          }
        }  
      }
      currentImage++;
    } 
    else{
      break;
    }
  }
  
  updatePixels();
}
