import codeanticode.syphon.*;
import processing.video.*;

Capture camera;
PGraphics canvas;
SyphonServer server;

float time;
int nodes;
ArrayList<Hair>hairs = new ArrayList();

void settings(){
  size(640, 480, P2D);
  PJOGL.profile = 1;
}

void setup(){
  camera = new Capture(this, width, height);
  canvas = createGraphics(width, height, P2D);
  server = new SyphonServer(this, "Processing Frames");
  
  nodes = 10;
  camera.start();
}

void draw(){
  if(camera.available()){
    camera.read();
  }
  
  //set(0, 0, camera);
  image(canvas, 0, 0);
  
  canvas.beginDraw();
    canvas.background(0);
    
    for(Hair current : hairs){
      current.run();
    }
    displayCrosshairs();
  canvas.endDraw();
  
  server.sendImage(canvas);
  time += .02;
}

void mouseDragged(){
  hairs.add(new Hair(mouseX, mouseY));
  println(hairs.size());
}

void keyPressed(){
  if(key == BACKSPACE || key == DELETE){
    int pop = hairs.size();
    for (int i = 0; i < pop; i++) {
      hairs.remove(0);
    }
  }
  
}

void displayCrosshairs(){
  PVector crossPos = new PVector(mouseX, mouseY);
  
  canvas.stroke(255, 0, 0);
  canvas.line(crossPos.x, crossPos.y-10, crossPos.x, crossPos.y+10);
  canvas.line(crossPos.x-10, crossPos.y, crossPos.x+10, crossPos.y);
}