class Hair{
  PVector pos;
  color stroke;
  float offset;
  
  Hair(int temp_x, int temp_y){
    pos = new PVector(temp_x, temp_y);
    stroke = camera.get(temp_x, temp_y);
  }
  
  void run(){
    canvas.noFill();
    canvas.stroke(255, 150);
    canvas.strokeWeight(1);
    
    canvas.beginShape();
      for(int x = 0; x < nodes; x++){
        offset = cos(time + pos.x+(x*50) + pos.y)*50 + noise(pos.x + (x*50), pos.y, time) * 20;
        if(x > 0){
          canvas.curveVertex(pos.x + (x*50), pos.y + offset);
        }else{canvas.vertex(pos.x, pos.y);}
        
      }
    canvas.endShape();
  }
  
}