import gab.opencv.*;
import processing.video.*;

OpenCV opencv;
Capture camera;
Movie clip;

final float PIXEL_SIZE = 2;
final int PFRAME_COLLECTION_RATE = 60;
final float IFRAME_FACTOR = .5;

boolean recievedPFrame;
int numberOfPixels;
ArrayList<Pixel> moshedPixels = new ArrayList<Pixel>();

void setup() {
  size(640, 360);
  noStroke();

  String[] availableCameras = Capture.list();
  printArray(availableCameras);
  camera = new Capture(this, availableCameras[3]);
  camera.start();
  recievedPFrame = false;
  
  clip = new Movie(this, "sprayPaint.mov");
  clip.loop();
  
  opencv = new OpenCV(this, width, height);
  numberOfPixels = int((width/PIXEL_SIZE) * (height/PIXEL_SIZE));
}

void draw() {
  if (recievedPFrame && moshedPixels.size() == numberOfPixels) {
    opencv.loadImage(clip);
    opencv.calculateOpticalFlow();

    int pixelIndex = 0; // Needed to calculate index of 1D array.

    for (int posY = 0; posY < height; posY += PIXEL_SIZE) {
      for (int posX = 0; posX < width; posX += PIXEL_SIZE) {

        PVector cameraLocalFlow = opencv.getFlowAt(posX, posY);
        //cameraLocalFlow.mult(IFRAME_FACTOR);
        moshedPixels.get(pixelIndex).position.add(cameraLocalFlow);
        moshedPixels.get(pixelIndex).display();
        pixelIndex++;
      }
    }
  } else { // Plays camera feed until sketch can get first PFrame
    set(0, 0, camera);
  } 

  if (frameCount % PFRAME_COLLECTION_RATE == 0) {
    image(camera, 0, 0); // Display new PFrame
    init_PFrame(camera); // Make Pixels
    recievedPFrame = true;
  }
}

void movieEvent (Movie m) {
  m.read();
  m.resize(width, height);
}

void captureEvent(Capture c) {
  c.read();
}