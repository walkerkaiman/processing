class Layer{
  ArrayList<PVector> points;
  int scale, res;
  
  Layer(int temp_scale, int temp_res){
    scale = temp_scale;
    res = temp_res;
    points = new ArrayList<PVector>();
    
    float angStep = 360/res;
    
    for(float angle = 0; angle < 360; angle += angStep){
      float x = cos(radians(angle)) * scale + width/2;
      float y = sin(radians(angle)) * scale + height/2;
      
      PVector vertex = new PVector(x, y);
      
      points.add(vertex);
    } 
  }
  
  void display(){
    float weight = tan(time + scale)*10;
    stroke(weight-255);
    strokeWeight(abs(weight));
    noFill();
    
    
    beginShape(); 
      for(PVector i : points){
        float offsetX = cos(i.y + time) * noiseScale;
        float offsetY = sin(i.x + time) * noiseScale;
        
        vertex(i.x + offsetX, i.y + offsetY);
      }
    endShape(CLOSE);
  }
}