ArrayList<Layer> layers;
float time;
int noiseScale;

void setup(){
  size(600, 600);
  frameRate(24);
  
  noiseScale = 20;
  
  layers = new ArrayList<Layer>();
  
  for(int i = 40; i < width*2; i += 50){
    layers.add(new Layer(i, 10));
  }
}

void draw(){
  smooth();
  background(0);
  
  for(Layer i : layers){
    i.display();
  }
  
  time += .2;
  
  //saveFrame("/Users/kaimanwalker/Desktop/frames/frame_####.png");
}