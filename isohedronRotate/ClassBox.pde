class Box {
  float size, speed, rotationX, rotationY, rotationZ;
  
  Box(float temp_size){
    size = temp_size;
    speed = radians(random(1, 3));
    rotationX = radians(random(360));
    rotationY = radians(random(360));
    rotationZ = 0;
  }
  
  void run(){
    update();
    run();
  }
  
  void update(){
    rotationZ += speed;
  }
  
  void display(){
    pushMatrix();
      translate(0, 0, 700);
      rotateX(rotationX);
      rotateY(rotationY);
      rotateZ(rotationZ);
      
      box(size, size, size);
    popMatrix(); 
  }
}