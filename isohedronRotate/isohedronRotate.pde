import peasy.*;
PeasyCam cam;

ArrayList<Box> boxes = new ArrayList<Box>();

int population;
float size;

void setup(){
  size(600, 600, P3D);
  frameRate(24);
  
  cam = new PeasyCam(this, 1000);
  
  population = 10;
  size = 100;
  
  for(int i = 0; i < population; i++){
    boxes.add(new Box(size));
  }
  
  stroke(255);
  strokeWeight(3);
  fill(0);
}

void draw(){
  background(0);
  
  for(Box i : boxes){
    i.run();
  }
}