class Arc {
  PVector pos;
  float radius, speed, currentRot;
  int arcLength, thick;
  
  Arc(PVector _pos, float _radius, int _arcLength, float _speed, int _thick){
    pos = _pos;
    radius = _radius;
    arcLength = _arcLength;
    speed = _speed;
    thick = _thick;
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    currentRot += speed;
  }
  
  void display(){
    stroke(WHITE);
    noFill();
    
    pushMatrix();
      translate(pos.x, pos.y);
      rotate(radians(currentRot));
      
      for(int i = 0; i < layers; i++){
        drawArc(i * thick);
      }
    popMatrix();
  }
  
  void drawArc(int zPos){
    float degree_A = radians(currentRot);
    float degree_B = radians(currentRot+arcLength);
    float r_A = radius;
    float r_B = radius - thick;
    
    PVector pointA = new PVector(cos(degree_A) * r_A, sin(degree_A) * r_A);
    PVector pointB = new PVector(cos(degree_A) * r_B, sin(degree_A) * r_B);
    
    PVector pointC = new PVector(cos(degree_B) * r_B, sin(degree_B) * r_B);
    PVector pointD = new PVector(cos(degree_B) * r_A, sin(degree_B) * r_A);
    
    fill(BLACK);
    
    beginShape();
        vertex(pointA.x, pointA.y, zPos);
        vertex(pointB.x, pointB.y, zPos);
        
        for(int ang = 0; ang <= arcLength; ang += 10){ // arc from B to C
          float angle = radians(ang+currentRot);
          vertex(cos(angle)*r_B, sin(angle)*r_B, zPos);
        }
        vertex(pointC.x, pointC.y, zPos);
        vertex(pointD.x, pointD.y, zPos);
        
        for(int ang = arcLength; ang >= 0; ang -= 10){ // arc from D to A
          float angle = radians(ang+currentRot);
          vertex(cos(angle)*r_A, sin(angle)*r_A, zPos);
        }
    endShape(CLOSE);
  }
}