import peasy.*;
PeasyCam cam;

final int arcPop = 5;
final int arcThickness = 50;
final int FPS = 30;

final color WHITE = color(255);
final color BLACK = color(0);

ArrayList <Arc> arcs = new ArrayList<Arc>();
boolean recording = false;
int layers = 1;

void setup(){
  size(700, 700, P3D);
  frameRate(FPS);
  
  for(int i = arcThickness; i < 1600; i += arcThickness){
    PVector position = new PVector(width/2, height/2);
    arcs.add(new Arc(position, i, int(random(30, 180)), random(1), arcThickness));
  }
  
  cam = new PeasyCam(this, 1000);
}

void draw(){
  background(BLACK);

  for(Arc a : arcs){
    a.run();
  }
  
  if(recording){
    saveFrame("data/frame_#####.png");
  }
}

void keyPressed(){
  if(key == 'r'){
    recording =! recording;
  }
}

void mouseReleased(){
  layers++; 
}