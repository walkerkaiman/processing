PImage binaryConvert(PImage input, float threshold){
  PImage output = new PImage(input.width, input.height);
  
  int w = input.width;
  int h = input.height;
  
  input.loadPixels();
  output.loadPixels();
  
  for(int i = 0; i < w*h; i++){
    if(brightness(input.pixels[i]) > threshold){
      output.pixels[i] = color(255, 255, 255);
    }
    else{
      output.pixels[i] = color(0, 0, 0);
    }
  }
  
  input.updatePixels();
  output.updatePixels();
  
  return output;
}