import processing.video.*;

import gab.opencv.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Core;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.CvType;

import org.opencv.core.Point;
import org.opencv.core.Size;

Capture camera;
OpenCV opencv;

Mat gray, unWarpedMarker;

PImage input, markerImage;

ArrayList<MatOfPoint> contours;
ArrayList<MatOfPoint2f> approximations;
ArrayList<MatOfPoint2f> markers;

boolean[][] markerCells;

void settings(){
  size(640, 480, P3D);
  PJOGL.profile = 1;
}

void setup(){
  
  //INITIATE OBJECTS
  camera = new Capture(this, 640, 480, "FaceTime HD Camera", 30);
  input = new PImage(camera.width, camera.height);
  opencv = new OpenCV(this, input);
  unWarpedMarker = new Mat(50, 50, CvType.CV_8UC1);
  
  contours = new ArrayList<MatOfPoint>();
  approximations = new ArrayList<MatOfPoint2f>();
  markers = new ArrayList<MatOfPoint2f>();
  
  markerCells = new boolean[7][7];
  
  //INITIATE METHODS
  camera.start();
  
  //SET STYLE
  noFill();
}

void draw(){
  //RUNS FOR EVERY NEW CAMERA FRAME
  if(camera.available()){

    //CLEAR LAST FRAME
    background(0);
    
    //READS NEW FRAME FROM THE CAMERA.
    camera.read();
    input = camera;
    
    //LOADS PIMAGE INTO OPENCV TO PROCESS.
    opencv.loadImage(input);
    
    //CREATES GRAYSCALE IMAGE FROM 'CAMERA'.
    gray = OpenCV.imitate(opencv.getGray());
    opencv.getGray().copyTo(gray);
    
    //CREATES A THRESHOLD IMAGE FROM 'GRAY'.
    Mat thresholdMat = OpenCV.imitate(opencv.getGray());
    opencv.blur(5);
    
    Imgproc.adaptiveThreshold(opencv.getGray(), thresholdMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 451, -65);
    
    //FINDS CONTOURS OF THRESHOLD IMAGE.
    contours = new ArrayList<MatOfPoint>();
    Imgproc.findContours(thresholdMat, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);

    //MAKE APPROXIMATIONS FROM CONTOURS
    approximations = createPolygonApproximations(contours);
    
    PImage binary = binaryConvert(opencv.getOutput(), mouseX);
    opencv.loadImage(binary);
   
    setMarkers();
    set(0, 0, opencv.getOutput());
    //SUSPECT (NOT BINARY IMAGE?)
    float cellSize = 350/7;

    markerCells = new boolean[7][7];
    /*
    for(int row = 0; row < 7; row++){
      for(int col = 0; col < 7; col++){
        int cellX = int(col*cellSize);
        int cellY = int(row*cellSize);
  
        Mat cell = unWarpedMarker.submat(cellX, cellX + int(cellSize), cellY, cellY + int(cellSize)); 
        markerCells[row][col] = (Core.countNonZero(cell) > (cellSize*cellSize)/2);
      }
    }
    */
    //printMarker();
  }
  //image(opencv.getOutput(), 0, 0, width, height);
  //set(0, 0, opencv.getOutput());
  //displayContours();
  //displayMarkers(); 
  //PImage dst  = createImage(350, 350, RGB);
  //opencv.toPImage(unWarpedMarker, dst);
}
  