void displayMarkers(){
  
  pushMatrix();
    translate(input.width/2, 0);
    stroke(0, 255, 0);
    strokeWeight(1);
  
    float cellSize = width/2/7.0;
    
    for (int col = 0; col < 7; col++) {
      for (int row = 0; row < 7; row++) {
        if(markerCells[row][col]){
          fill(255);
        } 
        else {
          fill(0);
        }
        stroke(0, 255,0);
        rect(col*cellSize, row*cellSize, cellSize, cellSize);
      }
    }
  
  popMatrix();
}