void setMarkers(){
  
  markers = new ArrayList<MatOfPoint2f>();
  markers = selectMarkers(approximations);
    
  MatOfPoint2f canonicalMarker = new MatOfPoint2f();
  
  Point[] canonicalPoints = new Point[4];
  canonicalPoints[0] = new Point(0, 350);
  canonicalPoints[1] = new Point(0, 0);
  canonicalPoints[2] = new Point(350, 0);
  canonicalPoints[3] = new Point(350, 350);
  
  canonicalMarker.fromArray(canonicalPoints);
  Mat transform = Imgproc.getPerspectiveTransform(markers.get(0), canonicalMarker); 
  Mat unWarpedMarker = new Mat(50, 50, CvType.CV_8UC1); 
  Imgproc.warpPerspective(gray, unWarpedMarker, transform, new Size(350, 350));
  Imgproc.threshold(unWarpedMarker, unWarpedMarker, 125, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
}