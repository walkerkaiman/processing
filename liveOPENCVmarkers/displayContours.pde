 void displayContours(){
   
  //STYLE
  noFill();
  smooth();
  strokeWeight(2);
  stroke(255, 0, 0);
  
  
  //DRAWS CONTOURS
  for (MatOfPoint contour : contours) {
    beginShape();
    Point[] points = contour.toArray();
    for (int i = 0; i < points.length; i++) {
      vertex((float)points[i].x, (float)points[i].y);
    }
    endShape();
  }
 }