ArrayList<Tri>triangles = new ArrayList<Tri>();
boolean recording = false;
color white = color(255);
color black = color(0, 100);
float startRadius = 30;
float startAngle = 90;
float angStep = 5;
float radiusStep = 20;
float amp = 30;
float pong, time;

void setup() {
  size(600, 600);

  stroke(black);
  noFill();
  
  for(float tier = 50; tier < height-100; tier += radiusStep) {
    for (float posX = 50; posX < width-50; posX += angStep) {
      float posY = tier;
      
      triangles.add(new Tri(new PVector(posX, posY), tier));
    }
  }
  
  println("Recording: " + recording);
}

void draw() {
  background(white);
  pong = cos(time/10)*width;
  float wavelength = map(pong, -width, width, 1, 300);
 
  for(Tri t : triangles){
    t.update(wavelength);
    t.display(); 
  }
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.png");
  }
  
  time += .1;
}