class Tri{
  PVector center, a, b, c;
  float offset, tier;
  
  Tri(PVector _center, float _tier){
    center = _center;
    tier = _tier;
  }
  
  void update(float _wavelength){
    offset = noise(center.x/10, center.y/100) * center.y/30;
    center.y = cos(center.x / _wavelength) * amp + tier;
    
    a = new PVector(center.x, center.y - offset);
    b = new PVector(center.x + offset, center.y + offset);
    c = new PVector(center.x - offset, center.y + offset);
  }
  
  void display(){
    beginShape();
      vertex(a.x, a.y);
      vertex(b.x, b.y);
      vertex(c.x, c.y);
    endShape(CLOSE);
  }
}