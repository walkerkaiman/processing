import processing.svg.*;

ArrayList<Hole> holes = new ArrayList<Hole>();

int population = 30;
int frame; 
float time;

void setup() {
  size(400, 600);
  //size(4960, 7016);
  stroke(0);
  fill(0);

  for (int i = 0; i < population; i++) {
    holes.add(new Hole());
  }
}

void draw() {
  //beginRecord(SVG, "modelA_"+frame+".svg");

  background(255);

  for (Hole h : holes) {
    h.run();
  }

  //endRecord();
  time += .05;
  frame ++;
}

void keyPressed() {
  if (key == 'q') {
    exit();
  }
}