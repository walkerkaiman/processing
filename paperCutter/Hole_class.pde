class Hole {
  PVector position;
  float xSize, ySize, xDelta, yDelta, xDeltaPos, yDeltaPos;

  Hole() {
    position = new PVector(random(width), random(height));

    xSize = random(-5, 5);
    ySize = random(-5, 5);

    xDelta = random(-5, 5);
    yDelta = random(-5, 5);

    xDeltaPos = random(-1, 5);
    yDeltaPos = random(-1, 5);
  }

  void run() {
    update();
    display();
  }

  void update() {
    float offsetX_size = noise(position.x/100, position.y/100, time) * xDelta;
    float offsetY_size = noise(position.x/100, position.y/100, time) * yDelta;

    xSize += offsetX_size;
    ySize += offsetY_size;

    float offsetX_pos = noise(position.x/100, position.y/100, time) * xDeltaPos;
    float offsetY_pos = noise(position.x/100, position.y/100, time) * yDeltaPos;

    position.x += offsetX_pos;
    position.y += offsetY_pos;
  }

  void display() {
    ellipse(position.x, position.y, xSize, ySize);
  }
}