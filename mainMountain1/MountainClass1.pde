class Mountain{
  
  float xStart, xEnd, zStart, zEnd, resolution, peakHeight;
  int cols, rows;
  
  float[][] peak;
  
  Mountain(float temp_xStart, float temp_xEnd, float temp_zStart, float temp_zEnd, float temp_resolution, float temp_peakHeight){
    xStart = temp_xStart;
    xEnd = temp_xEnd;
    zStart = temp_zStart;
    zEnd = temp_zEnd;
    resolution = temp_resolution;
    peakHeight = temp_peakHeight;
    
    cols = int((xEnd-xStart)/resolution);
    rows = int((zEnd-zStart)/resolution);
    rows = abs(rows);
  }
  
  void compute(){
    float rabbitX = random(3000);
    float rabbitZ = random(3000);
    
    peak = new float[cols][rows];
    
    for(int x = 0; x < cols; x++){
      for(int z = 0; z < rows; z++){
        float y = noise(rabbitX + rabbitZ) * peakHeight-z;
        
        peak[x][z] = y;
        rabbitX += .0005;
        rabbitZ += .0005;
      }
    }
  }
  
  void display(){
    noFill();
    stroke(255, 200);
    strokeWeight(2);

    for(int x = 0; x < cols; x++){
      beginShape();
        for(int z = 0; z < rows; z++){
          curveVertex((x*resolution)+xStart, peak[x][z], (z*-resolution)+zStart);
        }
      endShape();
    }
    
    for(int z = 0; z < rows; z++){
      beginShape();
        for(int x = 0; x < cols; x++){
          curveVertex((x*resolution)+xStart, peak[x][z], (z*-resolution)+zStart);
        }
      endShape();
    }
  }
  
}
