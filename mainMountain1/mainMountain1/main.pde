import peasy.*;
PeasyCam cam;
Mountain range;

void setup(){
  size(800, 600, P3D);
  
  // xStart, xEnd, zStart, zEnd, resolution, peakHeight
  range = new Mountain(0, width, -500, -600, 50, 200);
  cam = new PeasyCam(this, 1000);

  range.compute();
}

void draw(){
  background(0);
  range.display();
  
  /*
  // Diagnosis print.
  println("xStart: "+range.xStart);
  println("xEnd: "+range.xEnd);
  println("zStart: "+range.zStart);
  println("zEnd: "+range.zEnd);
  println("Resolution: "+range.resolution);
  println("peakHeight: "+range.peakHeight);
  println("cols: "+range.cols);
  println("rows: "+range.rows);
  */
}
