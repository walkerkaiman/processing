import peasy.*;
PeasyCam cam;

// Mountain(xStart, xEnd, zStart, zEnd, resolution, peakHeight);
Mountain range = new Mountain(0, 3000, 0, 5000, 50, 2000);

void setup(){
  size(800, 600, P3D);
  cam = new PeasyCam(this, 1000);
  
  background(0);
  range.compute();
}

void draw(){
  background(0);
  
  rotateX(radians(-25));
  range.display();
}
