class Brush{
  ArrayList<PVector> effected = new ArrayList<PVector>();
  PVector pos;
  int size;
  
  Brush(int temp_size){
    size = temp_size;
    pos = new PVector(-size, -size);
  }
  
  void update(int x, int y){
    pos = new PVector(x, y);
    effect();
  }
  
  void effect(){
    //Updates array with position of pixels that will be effected
    effected = new ArrayList<PVector>();
    
    for(int y = 0; y < img.height; y++){
      for(int x = 0; x < img.width; x++){
        float localDist = dist(x, y, pos.x, pos.y);
        
        if(localDist < size/2){
          
          int distance = int(dist(x, y, x, height));
          for(int i = 0; i < distance; i++){
            PVector temp_pos = new PVector(x, y+i);
            effected.add(temp_pos);
          }
    
        }
      }
    }
    
    //Actual effect of the pixels
    color[] unsorted = new color[effected.size()];
    color[] sorted = new color[unsorted.length];
    int index = 0;
    
    
    for(PVector current : effected){
      unsorted[index] = img.pixels[int(current.y)*width + int(current.x)];
      index++;
    }
    
    sorted = sort(unsorted);
    index = 0;
    
    for(PVector current : effected){
      img.pixels[int(current.x)+int(current.y)*width] = sorted[index];
      index++;
    }
  }
  
  void display(){
    //Displays effected area
    rectMode(CENTER);
    rect(pos.x, pos.y, size, size);
  }
}