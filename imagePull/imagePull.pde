PImage img;
int size = 2;
Brush brush;

void setup(){
  size(800, 450);
  noFill();
  
  img = loadImage("4.jpg");
  brush = new Brush(size);
  
  image(img, 0, 0, width, height);
}

void draw(){
  image(img, 0, 0, width, height);
}

void mouseDragged(){
  img.loadPixels();
    brush.update(mouseX, mouseY);
  img.updatePixels();
  
  //saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.tiff");
}

void mouseReleased(){
    int arraySize = brush.effected.size();
    
    for(int i = 0; i < arraySize; i++){
      brush.effected.remove(0);
    }
  }