import processing.video.*;
Capture cam;

PImage current, previous;
ArrayList<PImage>effected;

int bars, marginX, marginY;

int barHeight = 20;
int spaceDist = 15; 

void setup() {
  size(640, 480);
  cam = new Capture(this, 640, 480);
  
  current = new PImage(width, height);
  previous = new PImage(width, height);
  effected = new ArrayList<PImage>();

  bars = height/barHeight;

  for (int i = 0; i < bars; i++) {
    effected.add(new PImage(current.width, barHeight));
  }
}

void draw() {
  //Displays background as previous.
  background(previous);

  for (int barCurrent = 0; barCurrent < bars; barCurrent++) {
    int barY = barCurrent*barHeight;

    effected.get(barCurrent).loadPixels();

    for (int y = 0; y < barHeight; y++) {
      for (int x = 0; x < previous.width; x++) {
        effected.get(barCurrent).pixels[x+y*previous.width] = previous.pixels[x+(y+barY)*previous.width];
      }
    }

    effected.get(barCurrent).updatePixels();
  }

  //Displays all of the images in effected with a Y-axis offset.
  for (int i = 0; i < effected.size(); i++) {
    int offset = i * spaceDist;
    int barY = i * barHeight;

    image(effected.get(i), marginX, offset+barY+marginY);
  }

  current = previous;
}