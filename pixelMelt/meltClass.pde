class Melt{
  PVector pos;
  float size, speed;
  color fill;
  
  Melt(int temp_x, int temp_y){
    pos = new PVector(temp_x, temp_y);
    size = 2;
    fill = img.get(temp_x, temp_y);
    speed = random(.1, size);
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    if(keyPressed && key == CODED){
      switch(keyCode){
        case UP:
          pos.y -= speed;
          break;
        case DOWN:
          pos.y += speed;
          break;
        case LEFT:
          pos.x -= speed;
          break;
        case RIGHT:
          pos.x += speed;
          break;
      }
    }
    
    checkEdge();
  }
  
  void display(){
    noStroke();
    fill(fill);
    rect(pos.x, pos.y, size, size);
  }
  
  void checkEdge(){
    if(pos.x + size < 0){pos.x = width;}
    if(pos.x > width){pos.x = 0;}
    if(pos.y + size < 0){pos.y = height;}
    if(pos.y > height){pos.y = 0;}
  }
}