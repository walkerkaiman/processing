PImage img;
int population = 5000;
Melt[] melts = new Melt[population];

void setup(){
  size(640, 480);
  frameRate(24);
  
  img = loadImage("3.png");
  img.resize(width, height);
  image(img, 0, 0);
  
  for(int index = 0; index < melts.length; index++){
    melts[index] = new Melt(int(random(width)), int(random(height)));
  }
}

void draw(){
  
  if(keyPressed){
    for(Melt current : melts){
      current.run();
    }
  }
  
  //record();
}

void record(){
  if(frameCount < 360){
    println("Recording...");
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-####.tiff");
  }
  else{
    println("Not Recording...");
  }
}