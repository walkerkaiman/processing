import processing.video.*;
Capture cam;

ArrayList<PImage> frames = new ArrayList<PImage>();
int margin, population, recordingFrame;
boolean recording;

void setup(){
  size(640, 480);
  imageMode(CENTER);
  
  cam = new Capture(this, width, height);
  cam.start();
  
  margin = 30;
  population = 15;
  recording = false;
}

void draw(){
  for(int i = 0; i < frames.size(); i++){
    int imgWidth = width - (i*margin);
    int imgHeight = height - (i*margin);
    PImage frame = frames.get(i);

    image(frame, width/2, height/2, imgWidth, imgHeight);
  }
  record();
}

void captureEvent(Capture camera){
  PImage img = createImage(width, height, RGB);
  
  camera.read();
  camera.loadPixels();
  arrayCopy(camera.pixels, img.pixels);
  frames.add(img);
  
  if(frames.size() > population){
    frames.remove(0);
  }
}

void record(){
  if(recording && recordingFrame < 360){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.tiff");
    println(recordingFrame);
    recordingFrame++;
  }
  else{
    recording = false;
    recordingFrame = 0;
  }
}

void keyPressed(){
 switch(key){
   case 'r':
     recording =! recording;
     break;
 }
}