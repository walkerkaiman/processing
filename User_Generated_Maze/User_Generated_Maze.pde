import SimpleOpenNI.*;

SimpleOpenNI kinect;

int closestValue;
int closestX;
int closestY;

int averagex = 0;
int sumx = 0;
int [] storedValuesx;
int countx = 0;

int averagey = 0;
int sumy = 0;
int [] storedValuesy;
int county = 0;

int avex = 0;
int avey = 0;

int sizeX = 640;
int sizeY = 480;

int circleSize = sizeY / 4;

int wallRandomGen;
int randomValue;

int[] wallGenArray = new int[sizeX * sizeY];

void setup(){
  
  frame.setTitle("Digital Grafitti Maze");
  size(sizeX, sizeY);
  noStroke();
  
  kinect = new SimpleOpenNI(this);
 
  
  kinect.enableDepth();
  
  //Tracker Responsiveness
  storedValuesx = new int[30];
  storedValuesy = new int[30];
  
  //Stores random initial numbers to generate maze. 1s or -1s.
  for(int i = 0; i < wallGenArray.length; i++){
   
    randomValue = int(random (2));
      if(randomValue == 0){
        randomValue = -1; 
     }
     
    wallGenArray[i] = randomValue;
  } 
}



void draw(){
  
  smooth();
  background(0);
  
  closestValue = 8000;

  kinect.update();

  int[] depthValues = kinect.depthMap();

  int newValueFromStreamx = closestX;
  int newValueFromStreamy = closestY;
  
  AddNewValuex(newValueFromStreamx);
  AddNewValuey(newValueFromStreamy);
  
  
  if(countx > 0){
    avex = sumx / countx;
  }
 
  if(county > 0){
    avey = sumy / county;
  }

  for(int y = 0; y < 480; y++){
    for(int x = 0; x < 640; x++){
      
      int reverseX = 640 - x - 1;
      int i = reverseX + y * 640;
      int currentDepthValue = depthValues[i];
  
       if(currentDepthValue > 0 && currentDepthValue < closestValue){
        
        closestValue = currentDepthValue;
        closestX = x;
        closestY = y;
      }
    }
  }


    //Indexed location of circle.
  int mouseIndex = int(map(avex, 0, 640, 0, width)) + (int((map(avey, 0, 480, 0, height)) * width));
    
    
   //Makes sure that the full circle is in the window.
  if(int(map(avex, 0, 640, 0, width)) > circleSize/2 && int(map(avex, 0, 640, 0, width)) < width - (circleSize/2) && map(avey, 0, 480, 0, height) > circleSize/2 && map(avey, 0, 480, 0, height) < height - (circleSize/2)){
 
    //Flips X values in array where mouse is located.
    for(int x = mouseIndex - (circleSize/2); x < mouseIndex + (circleSize/2); x++){
      wallGenArray[x] *= -1; 
   }
   //Flips Y values in array where mouse is located.
    for(int y = mouseIndex - (width * (circleSize/2)); y < mouseIndex + (width * (circleSize/2)); y += width){    
      wallGenArray[y] *= -1;
   }
 }
 
  
  //cycles through every 30 pixels 
 for(int g = 0; g < height; g += 30){
 for(int i = 0; i < width; i += 30 ){
   
  //Draws maze shapes.
  if(wallGenArray[i + (g * sizeX)] == 1)
 {
   fill(255);
  beginShape();
    vertex(25 + i, 0 + g);
    vertex(30 + i, 0 + g);
    vertex(30 + i, 5 + g);
    vertex(5 + i, 30 + g);
    vertex(0 + i, 30 + g);
    vertex(0 + i, 25 + g);
  endShape();
  
 }
 
 if(wallGenArray[i + (g * sizeX)] == -1)
 {
   
   fill(255);
  beginShape();
    vertex(0 + i, 0 + g);
    vertex(5 + i, 0 + g);
    vertex(30 + i, 25 + g); 
    vertex(30 + i, 30 + g);
    vertex(25 + i, 30 + g);
    vertex(0 + i, 5 + g);
  endShape();
 }  
}
}


//Interaction circle locator.
fill(255, 170);
ellipse(int(map(avex, 0, 640, 0, width)), map(avey, 0, 480, 0, height), circleSize, circleSize);




}

void AddNewValuex(int val){
  if(countx < storedValuesx.length)
  {
    storedValuesx[countx++] = val;
    sumx += val; 
  }
  else{
    sumx += val; 
    sumx -= storedValuesx[0];
    
     
    for(int i = 0; i < storedValuesx.length-1; i++){
      storedValuesx[i] = storedValuesx[i+1] ;
    }
    
    storedValuesx[storedValuesx.length-1] = val;
  }
}

void AddNewValuey(int val)
{
  if(county < storedValuesy.length){
    storedValuesy[county++] = val;
    sumy += val; 
  }
  else{
    sumy += val; 
    sumy -= storedValuesy[0];
    
    for(int i = 0; i < storedValuesy.length-1; i++){
      storedValuesy[i] = storedValuesy[i+1] ;
    }
    
    storedValuesy[storedValuesy.length-1] = val;
  }
}
