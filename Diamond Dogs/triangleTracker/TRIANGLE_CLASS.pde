class Tri{
  PVector location, velocity, acceleration;
  float size, maxForce, maxSpeed, angle;
  int seed;
  
  Tri(int temp_seed){
    location = new PVector(width/2, height);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    
    size = random(5, 10);
    maxForce = random(.3, .5);
    maxSpeed = 5;
    seed = temp_seed;
    angle = 0;
  }
  
  void applyForce(PVector force){
    acceleration.add(force);
  }
  
  void applyBehaviors(ArrayList<Tri> triangleList){
    PVector seekForce = seek(new PVector(mouseX, mouseY));
    applyForce(seekForce);
  }
  
  PVector seek(PVector target){
    
    PVector desired = PVector.sub(target, location);
    PVector steer = PVector.sub(desired, velocity);
    
    desired.normalize();
    desired.mult(maxSpeed);
    
    steer.limit(maxForce);
    
    return steer;
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    location.add(velocity);
    acceleration.mult(0);
    
    angle += .04;
  }
  
  void display(){
    canvas.beginDraw();
      canvas.noFill();
      canvas.stroke(255);
      canvas.strokeWeight(2);
      
      for(Tri t : triangleList){
        canvas.pushMatrix();
          canvas.translate(location.x + cos(angle+seed)*spinSize, location.y + sin(angle+seed)*spinSize);
          canvas.rotate(angle+seed);
          
          canvas.beginShape(TRIANGLES);
            canvas.vertex(0, -size);
            canvas.vertex(-size, size);
            canvas.vertex(size, size);
          canvas.endShape();
        canvas.popMatrix();
      }
    canvas.endDraw();
  }
  
  void run(){
    applyBehaviors(triangleList);
    update();
    display();
  }
  
  
}
