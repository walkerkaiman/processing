import codeanticode.syphon.*;

int population = 30;
float spinSize = 100;

ArrayList<Tri> triangleList;
PGraphics canvas;
SyphonServer server;

void setup(){
  size(640, 480, P2D);
  
  triangleList = new ArrayList<Tri>();
  canvas = createGraphics(width, height);
  server = new SyphonServer(this, "Triangles");
  
  for(int seed = 0; seed < population; seed++){
    triangleList.add(new Tri(seed));
  }
}
void draw(){
  canvas.beginDraw();
    canvas.background(0);
    //canvas.ellipse(mouseX, mouseY, spinSize, spinSize);
  canvas.endDraw();
  
  for(Tri currentTri : triangleList){
    currentTri.run();
  }
  
  image(canvas, 0, 0);
  server.sendImage(canvas);
}
