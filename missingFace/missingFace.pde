import gab.opencv.*;
import processing.video.*;
import java.awt.*;

Capture video;
OpenCV opencv;
PImage backgroundImg;
boolean captured;
final int padding = 20;
final String backgroundFileName = "backgroundImage.png";

void setup() {
  size(640, 480);

  video = new Capture(this, width/2, height/2);
  opencv = new OpenCV(this, width/2, height/2);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  
  captured = false;
  video.start();
}

void draw() {
  scale(2);
  opencv.loadImage(video);
  
  image(video, 0, 0);

  if (captured) {
    Rectangle[] faces = opencv.detect();
    
    for (int i = 0; i < faces.length; i++) {
      int x = faces[i].x - padding;
      int y = faces[i].y - padding;

      image(updateSample(faces[i], x, y), x, y);
    }
  }
}

PImage updateSample (Rectangle detectedFace, int x, int y) {
  return loadImage(backgroundFileName).get(x, y, detectedFace.width+padding*2, detectedFace.height+padding*2);
}

void captureEvent(Capture c) {
  c.read();

  if (!captured && frameCount > 50) {
    backgroundImg = video;
    backgroundImg.save("data/" + backgroundFileName);
    println("Background Captured!");
    captured = true;
  }
}