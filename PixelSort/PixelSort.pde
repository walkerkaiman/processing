//MODES:
//0 -> black
//1 -> bright
//2 -> white

int mode = 2;
int framePop = 560;

PImage[] frames = new PImage[framePop];
PImage currentFrame;

int blackValue = -10000000;
int brigthnessValue = 100;
int whiteValue = -6000000;

int row = 0;
int column = 0;

void setup() {
  size(640, 480);
  //frameRate(24);
  
  for(int i = 1; i < frames.length-1; i++){
    String currentFrameFileName = str(i);
    
    if(i < 10){
      frames[i] = loadImage("frame-0000" + currentFrameFileName + ".png");
    }
    else if(i < 100){
      frames[i] = loadImage("frame-000" + currentFrameFileName + ".png");
    }
    else if(i < 1000){
      frames[i] = loadImage("frame-00" + currentFrameFileName + ".png");
    }
  }
}


void draw(){
  int frame = frameCount % frames.length;
  
  currentFrame = frames[frame];
  
  while(column < width-1){
    currentFrame.loadPixels(); 
    sortColumn();
    column++;
    currentFrame.updatePixels();
  }
  
  while(row < height-1){
    currentFrame.loadPixels(); 
    sortRow();
    row++;
    currentFrame.updatePixels();
  }
  
  image(currentFrame, 0, 0);
  
  if(frameCount < frames.length) {
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.png");
    println("Saving: "+frameCount+" of "+framePop);
  }
  
  column = 0;
  row = 0;
}

void sortRow(){
  int x = 0;
  int y = row;
  int xend = 0;
  
  while(xend < width-1){
    switch(mode){
      case 0:
        x = getFirstNotBlackX(x, y);
        xend = getNextBlackX(x, y);
        break;
      case 1:
        x = getFirstBrightX(x, y);
        xend = getNextDarkX(x, y);
        break;
      case 2:
        x = getFirstNotWhiteX(x, y);
        xend = getNextWhiteX(x, y);
        break;
      default:
        break;
    }
    
    if(x < 0) break;
    
    int sortLength = xend-x;
    
    color[] unsorted = new color[sortLength];
    color[] sorted = new color[sortLength];
    
    for(int i = 0; i < sortLength; i++){
      unsorted[i] = currentFrame.pixels[x + i + y * currentFrame.width];
    }
    
    sorted = sort(unsorted);
    
    for(int i = 0; i < sortLength; i++){
      currentFrame.pixels[x + i + y * currentFrame.width] = sorted[i];      
    }
    
    x = xend+1;
  }
}


void sortColumn(){
  int x = column;
  int y = 0;
  int yend = 0;
  
  while(yend < height-1){
    switch(mode){
      case 0:
        y = getFirstNotBlackY(x, y);
        yend = getNextBlackY(x, y);
        break;
      case 1:
        y = getFirstBrightY(x, y);
        yend = getNextDarkY(x, y);
        break;
      case 2:
        y = getFirstNotWhiteY(x, y);
        yend = getNextWhiteY(x, y);
        break;
      default:
        break;
    }
    
    if(y < 0) break;
    
    int sortLength = yend-y;
    
    color[] unsorted = new color[sortLength];
    color[] sorted = new color[sortLength];
    
    for(int i=0; i<sortLength; i++) {
      unsorted[i] = currentFrame.pixels[x + (y+i) * currentFrame.width];
    }
    
    sorted = sort(unsorted);
    
    for(int i = 0; i < sortLength; i++){
      currentFrame.pixels[x + (y+i) * currentFrame.width] = sorted[i];
    }
    
    y = yend+1;
  }
}


//BLACK
int getFirstNotBlackX(int _x, int _y){
  int x = _x;
  int y = _y;
  color c;
  
  while((c = currentFrame.pixels[x + y * currentFrame.width]) < blackValue){
    x++;
    if(x >= width) return -1;
  }
  
  return x;
}

int getNextBlackX(int _x, int _y){
  int x = _x+1;
  int y = _y;
  color c;
  
  while((c = currentFrame.pixels[x + y * currentFrame.width]) > blackValue){
    x++;
    if(x >= width) return width-1;
  }
  
  return x-1;
}

//BRIGHTNESS
int getFirstBrightX(int _x, int _y){
  int x = _x;
  int y = _y;
  color c;
  
  while(brightness(c = currentFrame.pixels[x + y * currentFrame.width]) < brigthnessValue){
    x++;
    if(x >= width) return -1;
  }
  
  return x;
}

int getNextDarkX(int _x, int _y){
  int x = _x+1;
  int y = _y;
  color c;
  
  while(brightness(c = currentFrame.pixels[x + y * currentFrame.width]) > brigthnessValue){
    x++;
    if(x >= width) return width-1;
  }
  
  return x-1;
}

//WHITE
int getFirstNotWhiteX(int _x, int _y){
  int x = _x;
  int y = _y;
  color c;
  
  while((c = currentFrame.pixels[x + y * currentFrame.width]) > whiteValue){
    x++;
    if(x >= width) return -1;
  }
  
  return x;
}

int getNextWhiteX(int _x, int _y){
  int x = _x+1;
  int y = _y;
  color c;
  
  while((c = currentFrame.pixels[x + y * currentFrame.width]) < whiteValue){
    x++;
    if(x >= width) return width-1;
  }
  
  return x-1;
}


//BLACK
int getFirstNotBlackY(int _x, int _y){
  int x = _x;
  int y = _y;
  color c;
  
  if(y < height){
    while((c = currentFrame.pixels[x + y * currentFrame.width]) < blackValue){
      y++;
      if(y >= height) return -1;
    }
  }
  
  return y;
}

int getNextBlackY(int _x, int _y){
  int x = _x;
  int y = _y+1;
  color c;
  
  if(y < height){
    while((c = currentFrame.pixels[x + y * currentFrame.width]) > blackValue){
      y++;
      if(y >= height) return height-1;
    }
  }
  
  return y-1;
}

//BRIGHTNESS
int getFirstBrightY(int _x, int _y){
  int x = _x;
  int y = _y;
  color c;
  
  if(y < height){
    while(brightness(c = currentFrame.pixels[x + y * currentFrame.width]) < brigthnessValue){
      y++;
      if(y >= height) return -1;
    }
  }
  
  return y;
}

int getNextDarkY(int _x, int _y){
  int x = _x;
  int y = _y+1;
  color c;
  
  if(y < height){
    while(brightness(c = currentFrame.pixels[x + y * currentFrame.width]) > brigthnessValue){
      y++;
      if(y >= height) return height-1;
    }
  }
  
  return y-1;
}

//WHITE
int getFirstNotWhiteY(int _x, int _y){
  int x = _x;
  int y = _y;
  color c;
  
  if(y < height) {
    while((c = currentFrame.pixels[x + y * currentFrame.width]) > whiteValue){
      y++;
      if(y >= height) return -1;
    }
  }
  
  return y;
}

int getNextWhiteY(int _x, int _y){
  int x = _x;
  int y = _y+1;
  color c;
  
  if(y < height) {
    while((c = currentFrame.pixels[x + y * currentFrame.width]) < whiteValue){
      y++;
      if(y >= height) return height-1;
    }
  }
  
  return y-1;
}
