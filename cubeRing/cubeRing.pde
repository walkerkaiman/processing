import peasy.*;
PeasyCam cam;

final color black = color(0);
final color white = color(255);
final float strokeWeight = 2;
final int boxSize = 20;
final float angStep = 20;
final float ringStep = 30;
final int rings = 10;
final float neighbor = 200;
final int popPerRing = int(360/angStep);
final int population =  popPerRing * rings;

ArrayList<PVector>center;
float time;
boolean recording;

void setup () {
  center = new ArrayList<PVector>();
  recording = false;
  
  for (int ring = 1; ring < rings+1; ring++) {
    float radius = ring * ringStep;
    
    for (int angle = 0; angle < 360; angle += angStep/ring) {
      float degree = radians(angle);
      float x = cos(degree) * radius;
      float y = sin(degree) * radius;
      float z = 0;
      
      center.add(new PVector(x, y, z));
    }
  }
  
  size(600, 600, P3D);
  stroke(white);
  strokeWeight(strokeWeight);
  fill(black);
  
  cam = new PeasyCam(this, 1000);
}

void draw () {
  background(black);
  
  for (PVector c : center) {
    float offset = c.x/neighbor + c.y/neighbor + time;
    float sizeOffset = abs(cos(offset) * boxSize);
    
    pushMatrix();
      translate(c.x, c.y, c.z);
      rotateY(offset);
      box(sizeOffset);
    popMatrix();
  }
  
  if (recording) {
    saveFrame(dataPath("frame_#####.png"));
  }
  
  time += .03;
}

void keyPressed () {
  switch(key) {
    case 'r':
    case 'R':
    recording =! recording;
    println("Recording: " + recording);
    break;
  }
}