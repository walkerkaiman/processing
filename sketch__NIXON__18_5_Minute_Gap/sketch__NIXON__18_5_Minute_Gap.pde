import processing.serial.*;
import ddf.minim.*;

String incoming = "";

// Is the handset on the cradle?
boolean active_A = false;
boolean active_B = false;

// Index of current audio clip.
int clip_A = 0;
int clip_B = 0;
int clipPop = 9;

color white = color(255);
color black = color(0);

// Incoming messages
String message_recieverA = "";
String message_recieverB = "";
String message_forwardA = "";
String message_backwardA = "";
String message_forwardB = "";
String message_backwardB = "";

// Outgoing LED trigger messages
String LED_A_0 = "";
String LED_A_1 = "";
String LED_A_2 = "";
String LED_A_4 = "";
String LED_A_5 = "";
String LED_A_6 = "";
String LED_A_7 = "";
String LED_A_8 = "";
String LED_A_9 = "";

String LED_B_0 = "";
String LED_B_1 = "";
String LED_B_2 = "";
String LED_B_4 = "";
String LED_B_5 = "";
String LED_B_6 = "";
String LED_B_7 = "";
String LED_B_8 = "";
String LED_B_9 = "";

void setup() {
  size(640, 480);
}

void draw() {
  // Update variable states

  // Display debug information
  // debug();

  // Incoming message logic
  if (incoming.equals(message_recieverA)) {
    // Reciever A action
    active_A =! active_A;

    if (active_A) {
      // Play first audio clip on reciever A
      // Trigger LED
    } else {
      // Stop playing audio on reciever A
      // Trigger LED
    }
  } else if (incoming.equals(message_recieverB)) {
    // Reciever B action
    active_B =! active_B;

    if (active_B) {
      // Play first audio clip on reciever B
      // Trigger LED
    } else {
      // Stop playing audio on reciever B
      // Trigger LED
    }
  } else if (incoming.equals(message_backwardA)) {
    // Station A. Backwards.
    clip_A --;
    clip_A = constrain(clip_A, 0, clipPop);
    // Trigger Audio A at index
    // Trigger LED at index
  } else if (incoming.equals(message_forwardA)) {
    // Station A. Forwards.
    clip_A ++;
    clip_A = constrain(clip_A, 0, clipPop);
    // Trigger Audio A at index
    // Trigger LED at index
  } else if (incoming.equals(message_backwardB)) {
    // Station B. Backwards.
    clip_B --;
    clip_B = constrain(clip_B, 0, clipPop);
    // Trigger Audio B at index
    // Trigger LED B at index
  } else if (incoming.equals(message_forwardB)) {
    // Station B. Forwards.
    clip_B ++;
    clip_B = constrain(clip_B, 0, clipPop);
    // Trigger Audio B at index
    // Trigger LED B at index
  }

  incoming = "";
}