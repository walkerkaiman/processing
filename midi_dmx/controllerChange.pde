void controllerChange(int channel, int number, int value) {

  if (number == 55) {
    int dmxOut = int(constrain(map(value, 0, 127, 0, 255), 0, 255));
    
    if (value > 40){
      dmx.set(1, dmxOut);
    }
    
  
    println("MIDI In: " + value);
    println("DMX Out: " + dmxOut);
    println();
  }
}