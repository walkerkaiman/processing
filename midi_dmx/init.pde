void init() {
  midi = new MidiBus(this, "APC MINI", 0);
  dmx = new DmxP512(this, 1, false);

  println();
  dmx.setupDmxPro("/dev/tty.usbserial-6AVH1FM6", 9600);
  println(); 
}