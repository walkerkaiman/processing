PImage glitch;
int off = 2;
int frame = 30;
int count = frame;

void setup(){
  size(600, 600);
  frameRate(frame/2);
  glitch = loadImage("images.jpg");
}

void draw(){
  
  effect();
  display();
  //record();
}

void effect(){
  
  int dimension = glitch.width * glitch.height;
  
  glitch.loadPixels();
  
  for(int i = 0; i < dimension; i +=1){
    int barLength = int(random(12));
    
    if(i - width * barLength+off > 0 && i - width * barLength+off < dimension){
      glitch.pixels[i] = glitch.pixels[i - width * barLength+off];
  }}
  
  glitch.updatePixels();
  
}

void display(){
  image(glitch, 0, 0);
}

void record(){
  if(frameCount <= frame){
    saveFrame("glitched-###.jpg");
    
    println("Frame Saved..." + count);
    count --;
  }
}
