class Letters {
  PVector pos;
  String[]chars = {"!", "@", "#", "$", "%", "^", "&", "*"};
  int index;
  int counter;
  
  Letters (PVector _pos, int _counter){
    pos = _pos;
    counter = _counter;
  }
  
  void display(){
    index = int(noise(counter/10 % chars.length)*chars.length);
    
    stroke(255);
    textSize(fontSize);
    text(chars[index], pos.x, pos.y);
  }
}