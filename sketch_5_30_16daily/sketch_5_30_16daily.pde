ArrayList<Letters> place = new ArrayList<Letters>();
int fontSize = 15;
boolean recording = false;

void setup(){
    size(600, 600);
    frameRate(30);
    
    for (int x = 0; x < width; x += fontSize) {
      for (int y = 0; y < height; y += fontSize) {
        PVector pos = new PVector(x, y);
        place.add(new Letters(pos, x/30+y/20));
      }
    }
}

void draw(){
  background(0);
  
  for (int i = 0; i < place.size(); i++) {
    Letters current = place.get(i);
    
    current.display();
    current.counter ++;
  }
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.png"); 
  }
}

void keyPressed(){
  if(key == 'r'){
    recording =! recording; 
  }
}