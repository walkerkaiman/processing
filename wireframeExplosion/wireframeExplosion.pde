import peasy.*;
PeasyCam cam;

float timeA, timeB;
boolean recording;

void setup(){
  size(600, 600, P3D);
  frameRate(30);
  
  cam = new PeasyCam(this, 1000);
  recording = false;
}

void draw(){
  background(0);
  
  sphereDetail(int(cos(timeA/10)*100));
  
  //Object
  
  fill(240);
  stroke(30);
  strokeWeight(4);
  
  pushMatrix();
    translate(width/2, 0, 0);
    rotate(radians(90));
    rotate(timeB*3);
   // box(100, 100, 100);
  popMatrix();
  
  //Explosion
  float explode = cos(timeB) * 800;
  float spin = radians(timeA);
  
  noFill();
  stroke(255, 150);
  strokeWeight(2);
  
  pushMatrix();
    translate(width/2, 0, 0);
    rotateY(timeB);
    sphere(800);
  popMatrix();
  
  timeA += .5;
  timeB += .05;
  
  record();
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
      break;
  }
}

void record(){
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.tiff"); 
  }
}