class Obsticle{
  float size;
  PVector pos;
  ArrayList<PVector>bounds;
  
  Obsticle(float temp_size){
    size = temp_size;
    pos = new PVector(0, 0);
  }
  
  void display(){
    canvas.stroke(255);
    canvas.strokeWeight(5);
    canvas.fill(0);
    
    canvas.ellipse(0, 0, size, size);
  }
  
  void updateBounds(){
    
  }
}