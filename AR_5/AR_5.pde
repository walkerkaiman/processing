import controlP5.*;
import gab.opencv.*;
import java.io.*; 
import jp.nyatla.nyar4psg.*;
import processing.video.*;
import codeanticode.syphon.*;

Capture cam;
MultiMarker nya;
OpenCV opencv;
ControlP5 gui;
SyphonServer server;

Flock flock;

String camPara = "/Users/kaimanwalker/Dropbox/Processing/libraries/NyARToolkit/data/camera_para.dat";
String patternPath = "/Users/kaimanwalker/Dropbox/Processing/AR_4/data/patternMaker/examples/ARToolKit_Patterns";

int arWidth = 640;
int arHeight = 480;

int numMarkers = 100;
boolean initialized = false;

float contrast;
int threshold, blur;

ArrayList<Obsticle> obsticles;
PGraphics canvas;

void settings(){
  size(640, 480, P3D);
  
  //GUI --- CAN'T DISPLAY AR AND GUI PROPERLY TOGETHER
  //size(800, 480, P3D);
  
  PJOGL.profile = 1;
}

void setup(){
  frameRate(120);
  
  //SELECTING A CAMERA
  String[] cameras = Capture.list();
 
  /*
  //PRINTS LIST OF AVAILABLE CAMERAS
  for(int i = 0; i < cameras.length; i++){
    println(cameras[i]);
  }
  */
  
  flock = new Flock();
  canvas = createGraphics(width, height, P3D);
  server = new SyphonServer(this, "Processing Frames");
  obsticles = new ArrayList<Obsticle>();
  cam = new Capture(this, cameras[16]);
  opencv = new OpenCV(this, arWidth, arHeight);
  nya = new MultiMarker(this, arWidth, arHeight, camPara, NyAR4PsgConfig.CONFIG_DEFAULT);
  gui = new ControlP5(this);
  
  //LOADS PATTERN FILES
  String[] patterns = loadPatternFilenames(patternPath);
  
  for(int i = 0; i < numMarkers; i++) {
    float size = 80;
    
    nya.addARMarker(patternPath + "/" + patterns[i], size);
    obsticles.add(new Obsticle(size));
  }
  
  for(int i = 0; i < 100; i++) {
    Boid b = new Boid(width/2,height/2);
    flock.addBoid(b);
  }
  
  initControls();
  cam.start();
}

void draw() {
  canvas.beginDraw();
  canvas.background(0);
  
  //IF A NEW FRAME FROM CAMERA IS AVAILABLE
  if(cam.available()){
    cam.read();
    initialized = true;
  }
  
  if(initialized){
    
    //CONDITION AND FILTER IMAGE FOR PATTERN RECOGNITION.
    PImage processed = preProcess(cam.get());
    
    //set(0, 0, cam.get());
    //image(processed, 0, height-200, 320, 200);
    
    nya.detect(processed);
    
    for (int i = 0; i < numMarkers; i++) { 
      // if the marker does NOT exist, escape current index and go to next index
      if( !nya.isExistMarker(i) ){ 
        continue;
      }
      
      canvas.pushMatrix();
        nya.setARPerspective();
        canvas.setMatrix(nya.getMarkerMatrix(i));
        
        canvas.translate(-80, 80);
        
        canvas.strokeWeight(5);
        canvas.stroke(255, 0, 0);
        canvas.noFill();
        
        canvas.ellipse(0, 0, 80, 80);
      canvas.popMatrix();
      
      canvas.perspective();
    }
    
    //RUN BOIDS
    flock.run();
  }
  
  canvas.endDraw();
  
  image(canvas, 0, 0, width, height);
  server.sendImage(canvas);
}















void initControls(){
  color textColor = color(255);
  contrast = 1.5;
  threshold = 125;
  blur = 2;
  
  // Slider for blur size
  gui.addSlider("blur")
     .setLabel("blur")
     .setPosition(650, 10)
     .setRange(1, 10)
     .setColorValue(textColor)
     ;
     
     // Slider for contrast
  gui.addSlider("contrast")
     .setLabel("contrast")
     .setPosition(650, 60)
     .setRange(.5, 1.5)
     .setColorValue(textColor)
     ;
     
  // Slider for threshold
  gui.addSlider("threshold")
     .setLabel("threshold")
     .setPosition(650, 110)
     .setRange(0,255)
     .setColorValue(textColor)
     ;
}

// this function loads .patt filenames into a list of Strings based on a full path to a directory (relies on java.io)
String[] loadPatternFilenames(String path) {
  File folder = new File(path);
  
  FilenameFilter pattFilter = new FilenameFilter() {
    public boolean accept(File dir, String name) {
      return name.toLowerCase().endsWith(".patt");
    }
  };
  
  return folder.list(pattFilter);
}

PImage preProcess(PImage input){ 
  input.resize(arWidth, arHeight); //DOWN SAMPLE
  opencv.loadImage(input); //LOAD IMAGE TO BE PREPROCESSED
  
  opencv.blur(blur); //REMOVE NOISE
  opencv.contrast(contrast);//SEPERATE IMAGE VALUES
  opencv.threshold(threshold); //MAKE A BINARY IMAGE
  opencv.erode(); //SMOOTH CONTOURS
  //opencv.dilate(); //CLOSE HOLES
  
  return opencv.getSnapshot();
}