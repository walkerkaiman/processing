void initGUI() {
  
  // Slider for blur size
  gui.addSlider("blur")
     .setLabel("blur size")
     .setPosition(450, 50)
     .setRange(1, 20)
     ;
     
  // Slider for contrast
  gui.addSlider("_brightness")
     .setLabel("brightness")
     .setPosition(450, 100)
     .setRange(-255, 255)
     ;
     
     // Slider for contrast
  gui.addSlider("contrast")
     .setLabel("contrast")
     .setPosition(450, 150)
     .setRange(0.0, 10.0)
     ;
     
  // Slider for adaptive threshold block size
  gui.addSlider("thresholdBlockSize")
     .setLabel("block size")
     .setPosition(450, 200)
     .setRange(1,700)
     ;
     
  // Slider for adaptive threshold constant
  gui.addSlider("thresholdConstant")
     .setLabel("Threshold")
     .setPosition(450, 250)
     .setRange(-100, 100)
     ;
}