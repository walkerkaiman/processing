void drawMarkerBounds(int i){
 
// DRAW MARKER SHAPE
noFill();
stroke(0, 255, 0);
strokeWeight(3);

beginShape();
  for(int v = 0; v < 4; v++){
    vertex(nya.getMarkerVertex2D(i)[v].x, nya.getMarkerVertex2D(i)[v].y);
  }
endShape(CLOSE);
}