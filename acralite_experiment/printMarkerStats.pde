void printStats(int i){ 
  PVector v1 = new PVector(nya.getMarkerVertex2D(i)[0].x, nya.getMarkerVertex2D(i)[0].y);
  PVector v2 = new PVector(nya.getMarkerVertex2D(i)[2].x, nya.getMarkerVertex2D(i)[2].y);
  
  int lengthX = (int)abs( v1.x - v2.x ); 
  int lengthY = (int)abs( v1.y - v2.y );
  
  String confidence = nf((float)nya.getConfidence(i) * 100, 0, 2);
  
  println();
  println("ID: " + i);
  println("x Length: " + lengthX + "px");
  println("y Length: " + lengthY + "px");
  println("Area: " + lengthX*lengthY + "px");
  println("Confidence: "+ confidence + "%");
  println();
 
  lengthX = 0;
  lengthY = 0;
}