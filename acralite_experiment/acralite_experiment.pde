// LIBRARIES
import controlP5.*;
import processing.video.*;
import gab.opencv.*;
import jp.nyatla.nyar4psg.*;

// NAME INSTANCES
Capture cam;
OpenCV opencv;
ControlP5 gui;
MultiMarker nya;

// NAME AND INITIATE GLOBAL VARIABLES
int blur = 12;
int _brightness = -131;
float contrast = .7;
int thresholdBlockSize = 248;
int thresholdConstant = -1;

int arWidth = 640;
int arHeight = 480;
int unitSize = 80;

int markerPopulation = 3;
String camPara = "/Users/kaimanwalker/Dropbox/Processing/libraries/NyARToolkit/data/camera_para.dat";

PImage processedImage;

void setup(){
  // INITIATE CLASS INSTANCES
  cam = new Capture(this, 640, 480, "Microsoft LifeCam VX-800", 30);
  opencv = new OpenCV(this, cam);
  nya = new MultiMarker(this, arWidth, arHeight, camPara, NyAR4PsgConfig.CONFIG_DEFAULT);
  gui = new ControlP5(this);
  
  // SKETCH PROPERTIES
  size(640, 480, P2D); //(640 + GUI width, camera.height, P3D for tracking);
  
  // INITIAL FUNCTIONS
  
  // Pattern A
  nya.addARMarker("4x4_14.patt", unitSize); // recognized and actual pattern
  
  //Pattern B
  nya.addARMarker("4x4_16.patt", unitSize); // recognized and actual pattern
  
  // Pattern C
  nya.addARMarker("4x4_95.patt", unitSize); // recognized and actual pattern
 
  initGUI();
  cam.start();
}

void draw(){
  background(0);
  
  if(cam.available()){
    cam.read();
    
    processedImage = imageProcess(cam);
    
    image(processedImage, 0, 0, 640, 480); 
    
    nya.detect(processedImage);
    
    //CYCLE THROUGH ALL MARKERS
    for(int i = 0; i < markerPopulation; i++){
      
      if(nya.isExistMarker(i)){
        // PATTERN FOUND INDICATOR
        noStroke();
        fill(0, 255, 0);
        
        ellipse(30, 30, 30, 30);
        
        drawMarkerBounds(i);
        printStats(i);
        
        break;
      }
      else{
        // PATTERN NOT FOUND INDICATOR
        noStroke();
        fill(255, 0, 0);
        
        ellipse(30, 30, 30, 30);
      }
    }
  }
}