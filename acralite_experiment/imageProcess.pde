PImage imageProcess(PImage input){
  
  opencv.loadImage(input);
  
  opencv.blur(blur);
  opencv.brightness(_brightness);
  opencv.contrast(contrast);
  //opencv.invert();
  adaptiveThreshold();
  opencv.dilate();
  opencv.erode();
  
  PImage processedImg = opencv.getOutput();
  
  return processedImg;
}

void adaptiveThreshold(){
  // Block size must be odd and greater than 3
  if(thresholdBlockSize % 2 == 0){
    thresholdBlockSize++;
  }
  if(thresholdBlockSize < 3){
    thresholdBlockSize = 3;
  }
    
  opencv.adaptiveThreshold(thresholdBlockSize, thresholdConstant);
}