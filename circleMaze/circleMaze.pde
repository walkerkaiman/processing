float xPos, yPos, rotateAng;
float size = 3;

void setup(){
  size(600, 600);

  noStroke();
  fill(255);
  background(0);
}

void draw(){
  smooth(4);

  rotateAng = random(2);
  translate(width/2, height/2);
  

 if(xPos < width/2){
  
  for(float ang = 0; ang < rotateAng; ang += .001){
    
    ellipse(xPos, yPos, size, size);
    rotate(radians(ang));
  }
  
  for(float expand = 0; expand < 6; expand ++){
    
    ellipse(xPos, yPos, size, size);
    xPos += 2;
  }
 }
}

void mousePressed(){
 saveFrame("maze-###.png"); 
  
}