import peasy.*;

PeasyCam cam;

final color BLACK = color(0);
final color WHITE = color(255);
float time;

void setup(){
  size(600, 600, P3D);
  stroke(WHITE);
  fill(BLACK);
  
  cam = new PeasyCam(this, 100);
}

void draw(){
  background(BLACK);
  
  
  for(int j = 0; j < 10; j++){
    int jube = j*20;
    for(int ring = 0; ring < 100; ring++){
      PVector pos = new PVector(jube, ring*3, 0);
      float dia = pow(cos(pos.y+time+j)*2, 2)*6;
      
      pushMatrix();
      translate(pos.x, pos.y, pos.z);
      rotateX(radians(90));
      ellipse(0, 0, dia, dia);
      popMatrix();
      
    }
  }
  time += .01;
}