import peasy.*;
PeasyCam cam;

int size;
float rotX, rotY, rotZ, stepX, stepY, stepZ;
boolean recording;

void setup(){
  cam = new PeasyCam(this, 1000);
  
  size(500, 500, P3D);
  
  frameRate(30);
  ortho();
  stroke(255);
  strokeWeight(5);
  //noFill();
  fill(0);
 
  size = 50;
  stepY = 2;
  recording = false;
}

void draw(){
  background(0);
  
  float trans = map(rotY, 0, 90, 0, size*2);
  
  for(int x = 0; x <= width*4; x += size*2){
    for(int y = 0; y <= height*4+size; y += size*2){
      pushMatrix();
        translate(x, y-trans, size);
        rotateY(radians(rotY));
        
        box(size);
      popMatrix();
    }
  }

  rotY += stepY;

  if(rotY >= 90){
    rotY = stepY;
  }
  
  if(recording){
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.png");
  }
}

void keyPressed(){
  switch(key){
    case 'r':
      recording =! recording;
    break;
  }
}