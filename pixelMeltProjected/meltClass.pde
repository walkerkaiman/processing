class Melt{
  PVector pos;
  color fill;
  float size, speed;
  
  Melt(int temp_x, int temp_y, color temp_color){
    pos = new PVector(temp_x, temp_y);
    fill = camera.get(temp_x, temp_y);
    size = random(1, 2);
    speed = random(.1, size);
    
    float c = hue(temp_color)+180 % 360;
    fill = color(int(c), 1, brightness(temp_color));
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    if(keyPressed && key == CODED){
      switch(keyCode){
        case UP:
          pos.y -= speed;
          break;
        case DOWN:
          pos.y += speed;
          break;
        case LEFT:
          pos.x -= speed;
          break;
        case RIGHT:
          pos.x += speed;
          break;
      }
    }
  }
  
  void display(){
    canvas.noStroke();
    canvas.fill(255, 150);
    
    canvas.rect(pos.x, pos.y, size, size);
  }
}