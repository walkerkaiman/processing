//pixelMelt
//Drag mouse where you want the melt to begin.
//Use arrows to control the direciton on the melt.
//Backspace/Delete will clear the screen.

import processing.video.*;
import codeanticode.syphon.*;

Capture camera;
SyphonServer server;

PGraphics canvas;
ArrayList<Melt> melts = new ArrayList();

void settings() {
  size(640, 480, P2D);
  PJOGL.profile = 1;
}

void setup() {
  frameRate(24);
  colorMode(HSB, 360, 1.0, 1.0);

  camera = new Capture(this, width, height);
  camera.start();

  canvas = createGraphics(width, height, P2D);
  canvas.beginDraw();
  canvas.background(0);
  canvas.endDraw();

  server = new SyphonServer(this, "Processing Frames");
}

void draw() {
  if (camera.available()) {
    camera.read();
  }

  if(keyPressed){
    canvas.beginDraw();

    for (int i = 0; i < melts.size(); i++) {
      melts.get(i).run();
    }

    if (key == BACKSPACE || key == DELETE) {
      int pop = melts.size();
      for (int i = 0; i < pop; i++) {
        melts.remove(0);
      }
      canvas.background(0);
    }
    canvas.endDraw();
  }
  
  set(0, 0, camera);
  server.sendImage(canvas);
}

void mouseDragged() {
  melts.add(new Melt(mouseX, mouseY, camera.get(mouseX, mouseY)));
  println(melts.size());
}