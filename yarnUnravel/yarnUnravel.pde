ArrayList<PVector> points = new ArrayList<PVector>();
float speed = 50;

void setup(){
  size(5700, 1080);
  frameRate(24);
  
  stroke(255);
  strokeWeight(10);
  noFill();
  
  for(float x = 0; x < width; x += 12){
    for(float y = 0; y < height; y += speed){
      float offset = noise(x, y)*10;
      points.add(new PVector(x+offset, y));
    }
    for(float y = height; y > 0; y -= speed){
      float offset = noise(x, y)*10;
      points.add(new PVector(x+offset, y));
    }
  }
}

void draw(){
  smooth();
  background(0);
 
  beginShape();
    for(int i = points.size()-1; i > 0; i--){
       PVector current = points.get(i);
      curveVertex(current.x, current.y);
    }
  endShape();
  
  if(points.size() > 1){
    points.remove(0);
    line(-5, height/2, points.get(1).x, points.get(1).y);
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame-#####.png");
  }
}