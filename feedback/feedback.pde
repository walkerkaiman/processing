import processing.video.*;
Capture cam;

ArrayList<Child> children = new ArrayList<Child>();
int margin, population, frameIndex;

void setup(){
  size(640, 480, P2D);
  
  margin = 20;
  population = 50;
  frameIndex = children.size()-1;
  
  String[] cameras = Capture.list();
  cam = new Capture(this, cameras[0]);
  cam.start();
  
  for(int i = 0; i < population; i++){
    int X = i*margin;
    int Y = i*margin;
    
    //Child(index, X, Y)
    children.add(new Child(i, X, Y));
  }
}

void draw(){
  
  if(cam.available()){
    cam.read(); 
   
    if(frameIndex > 0){
      frameIndex--;
    }else{frameIndex = children.size()-1;}
    children.get(frameIndex).updateFrame(cam);
  }
  
  for(Child current : children){
    current.displayFrame();
  }
}