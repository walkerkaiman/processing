class Child{
  int index, imgWidth, imgHeight;
  PVector pos;
  
  PImage img;
  
  Child(int temp_i, int temp_x, int temp_y){
    index = temp_i;
    pos = new PVector(temp_x, temp_y);
    img = createImage(width, height, RGB);
  }
  
  void updateFrame(PImage frame){
    img = frame;
  }
  
  void displayFrame(){
    image(img, pos.x, pos.y, width - (index*margin), height - (index*margin));
  }
}