class Sun {
  PVector center;
  float strokeWeight, diameter;
  color c;

  Sun (PVector _center, float _strokeWeight, color _c, float _diameter) {
    center = _center;
    strokeWeight = _strokeWeight;
    c = _c;
    diameter = _diameter;
  }

  void display() {
    strokeWeight(strokeWeight);
    stroke(c);
    fill(black);

    pushMatrix();
    translate(center.x, center.y, center.z);
    ellipse(0, 0, diameter, diameter);
    popMatrix();
  }
}