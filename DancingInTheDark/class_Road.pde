class Road {
  float startZ, endZ, yPos, step, gridWidth, wallHeight;

  Road (float _startZ, float _endZ, float _yPos, float _step, float _gridWidth) {
    startZ = _startZ;
    endZ = _endZ;
    yPos = _yPos;
    step = _step;
    gridWidth = _gridWidth;
    
    wallHeight = 50;
  }

  void run() {
    update();
    display();
  }

  void update() {
  }

  void display() {
    float xBegin = sun.center.x - gridWidth/2;
    float xEnd = sun.center.x + gridWidth/2;
    float zOffset = time % step;

    fill(black);
    noStroke();

    beginShape();
    vertex(xBegin, yPos, startZ);
    vertex(xEnd, yPos, startZ);
    vertex(xEnd, yPos, endZ);
    vertex(xBegin, yPos, endZ);
    endShape(CLOSE);
    
    beginShape();
    vertex(xBegin, yPos, startZ);
    vertex(xBegin, yPos - wallHeight, startZ);
    vertex(xBegin, yPos - wallHeight, endZ);
    vertex(xBegin, yPos, endZ);
    endShape(CLOSE);
    
    beginShape();
    vertex(xEnd, yPos, startZ);
    vertex(xEnd, yPos - wallHeight, startZ);
    vertex(xEnd, yPos - wallHeight, endZ);
    vertex(xEnd, yPos, endZ);
    endShape(CLOSE);

    stroke(white);
    strokeWeight(1);
    
    for (float z = startZ; z < endZ; z += step) {
      line (xBegin, yPos, z + zOffset, xEnd, yPos, z + zOffset);
    }
    
    for (float z = startZ; z < endZ; z += step) {
      line (xBegin, yPos, z + zOffset, xBegin, yPos-wallHeight, z + zOffset);
    }
    
    for (float z = startZ; z < endZ; z += step) {
      line (xEnd, yPos, z + zOffset, xEnd, yPos-wallHeight, z + zOffset);
    }
    
    if (zOffset == 0) {
      recording = false;
    }
  }
}