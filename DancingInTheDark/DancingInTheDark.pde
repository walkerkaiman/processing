import peasy.*;

PeasyCam cam;
Sun sun;
Road road;

final color black = color(0);
final color white = color(255);
final color green = color(0, 255, 0);
float time;
boolean recording = false;

void setup() {
  size(1920, 1080, P3D);
  frameRate(30);
  
  cam = new PeasyCam(this, 1000);

  final PVector sunCenter = new PVector(width/2, height/2, -400);
  final float strokeThickness = 5;
  final color strokeColor = white;
  final float sunDiameter = 600;

  final float roadBegin = sunCenter.z - 1;
  final float roadEnd = 5000;
  final float yPos = sunCenter.y;
  final float gridStep = 30;
  final float roadWidth = sunDiameter;

  sun = new Sun(sunCenter, strokeThickness, strokeColor, sunDiameter);
  road = new Road(roadBegin, roadEnd, yPos, gridStep, roadWidth);
}

void draw() {
  background (green);

  sun.display();
  road.run();
  
  if(recording) {
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.png");  
    println("Recording frame: " + frameCount);
  }
  
  time += 6;
}