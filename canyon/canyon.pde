import peasy.*;
PeasyCam cam;

int step = 5;
int girth = 600;
int deep = 600;
float rabbit;

void setup(){
  size(displayHeight-100, displayHeight-100, P3D);
  cam = new PeasyCam(this, 1000);
  
  noFill();

}
void draw(){

  smooth();
  background(255);
  
  PVector[] vertexPos = new PVector[(girth/step)*(deep/step)];

//POPULATES vertexPos[] WITH POSITION VALUES
  for(int i = 0; i < vertexPos.length; i++){
    
    //GIVES GENERAL SHAPE (GRID)
    float xPos = (i*step) % girth;
    float zPos = -(i*step*step)/deep;
    float yPos = 0;
    
    //GIVES INTERESTING OFFSET TO POSITION
    float offsetX = 0;
    float offsetY = cos(rabbit+2*xPos/zPos)*200;
    float offsetZ = 0;
    
    float canyon = 0;
    
    //STORES VERTEX POSITION TO ARRAY AS A 3D VECTOR
    vertexPos[i] = new PVector(xPos + offsetX, yPos + offsetY + canyon, zPos + offsetZ);
    
    //position = mouseX + (mouseY * girth)
    
    
    rabbit += .000005;
  }
  
 rotateX(radians(-60));
 translate(-width/2, -height/2, 500);
 
//DRAWS VERTEX POINTS FROM STORED VALUES IN vertexPos[] 

    for(PVector currentVertex : vertexPos){
      float colorMap = map(currentVertex.y, -100, 100, 255, 0);
      stroke(255-colorMap, 0, 255, colorMap);
      strokeWeight(noise(rabbit+currentVertex.x+currentVertex.z)*10);
      point(currentVertex.x, currentVertex.y, currentVertex.z);
    }
 
    
}
