class Leaf {
  PVector orbit, center;
  float radius;
  int birth;
  
  Leaf (PVector temp_center) {
    center = temp_center;
    orbit = new PVector();
    radius = random(orbitSize);
    birth = frameCount;
  }
  
  void run () {
    updateOrbit();
    display();
  }
  
  void display () {
    
    for (Leaf neighbor : leafs) {
      float dist = dist(neighbor.orbit.x, neighbor.orbit.y, orbit.x, orbit.y);
      
      if (dist < lineSize && dist > 0) {
        float stroke = map (dist, 0, lineSize, 0, 255);
       
        stroke(255, 255 - stroke);
        line(neighbor.orbit.x, neighbor.orbit.y, orbit.x, orbit.y);
      }
    }
  }
  
  void updateOrbit () {
    center.x -= speed;
    
    float angle = radians(time + birth);
    float tempX = cos(angle) * radius + center.x;
    float tempY = sin(angle) * radius + center.y;
    
    orbit = new PVector(tempX, tempY);
  }
}