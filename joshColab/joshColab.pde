final float drift = 10;
final float speed = .9;
final float motion = 10;
final float orbitSize = 50;
final float lineSize = 20;
PImage background; 

ArrayList<Leaf> leafs;
float time;

void setup () {
  size(600, 600);
  stroke(255);
  
  leafs = new ArrayList<Leaf>();
  
  background = loadImage("pix.JPG");
  background.resize(width, height);
}

void draw () {
  image(background, 0, 0);
  
  for (Leaf current : leafs) {
    current.run();
  }
  
  saveFrame(dataPath("frames/frame_#####.png"));
  destroyLeafs();
  time += 2;
}

void destroyLeafs () {
  for (int i = leafs.size()-1; i >= 0; i--) {
    float xPos = leafs.get(i).center.x;
    
    if (xPos < 0) {
      leafs.remove(i);
      println(leafs.size());
    }
  }
}

void mouseDragged () {
  if (dist(mouseX, mouseY, pmouseX, pmouseY) > 10) {
    PVector newCenter = new PVector(mouseX, mouseY);
    leafs.add(new Leaf(newCenter));
  }
}