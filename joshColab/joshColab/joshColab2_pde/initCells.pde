void initCells () {
  int index = 0;
  
  for (int z = 0; z < rings; z += size) {
    blocksPerRing = 0;
    
    for(int angle = 0; angle < 360; angle += angleStep) {
      float degree = radians(angle);
      float x = cos(degree) * radius;
      float y = sin(degree) * radius;
      float zPos = z * 3;
      PVector pos = new PVector(x, y, zPos);
      boolean state = random(100) < aliveChance ? true : false;
      
      cells.add(new Cell(index, pos, size, state));
      index ++;
      blocksPerRing++;
    }
  } 
  
  println(blocksPerRing);
}