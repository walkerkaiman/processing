class Cell {
  PVector pos;
  boolean state;
  float size;
  int index;
  
  Cell(int index, PVector pos, float size, boolean state){
    this.pos = pos;
    this.size = size;
    this.state = state;
    this.index = index;
  }
  
  int countNeighbors () {
    int neighborCount = 0;
    
    // NW
    try {
      Cell nw = cells.get(index - blocksPerRing - 1);
      
      if (nw.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    // N
    try {
      Cell n = cells.get(index - blocksPerRing);
      
      if (n.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    
    // NE
    try {
      Cell ne = cells.get(index - blocksPerRing + 1);
      
      if (ne.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    
    // W
    try {
      Cell w = cells.get(index - 1);
      
      if (w.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    // E
    try {
      Cell e = cells.get(index + 1);
      
      if (e.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    
    // SW
    try {
      Cell sw = cells.get(index + blocksPerRing - 1);
      
      if (sw.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    // S
    try {
      Cell s = cells.get(index + 1);
      
      if (s.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    // SE
    try {
      Cell se = cells.get(index + blocksPerRing + 1);
    
      if (se.state) {
        neighborCount ++;
      }
    }catch (IndexOutOfBoundsException e) {
      state = false;
    }
    
    return neighborCount;
  }
 
  
  void update () {

  }
  
  void display () {
    fill(state ? alive : dead);
    rotateZ(time);
    pushMatrix();
    translate(pos.x, pos.y, pos.z);
    box(size);
    popMatrix();
  }
  
  void run () {
    update();
    display();
  }
}