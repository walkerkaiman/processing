import peasy.*;
PeasyCam cam;

float speed = 30;
float size = 5;
int angleStep = 5;
float radius = 80;
int rings = 600;
float aliveChance = 35;
float time = 0;

final color black = color (0);
final color white = color (255);
final color alive = white;
final color dead = black;
ArrayList <Cell> cells = new ArrayList<Cell>();
int blocksPerRing;

void setup () {
  initCells();
  cam = new PeasyCam(this, 1000);
  
  size(600, 600, P3D);
}

void draw() {
  background(black);
  
  for (Cell c : cells) {
    c.pos.z += speed;
    c.run();
  }
  time += .00001;
}