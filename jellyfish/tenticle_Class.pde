class Tenticle{
  Node[] nodes;
  PVector anchor;
  float nodeDist, forceMax;
  
  Tenticle(PVector temp_anchor){
    anchor = temp_anchor;
    nodes = new Node[15];
    nodeDist = 50;
    forceMax = 5;
    
    for(int initNode = 0; initNode < nodes.length; initNode++){
      nodes[initNode] = new Node(new PVector(anchor.x, anchor.y));
    }
  }

  void run(){
   update();
   display();
  } 
  
  void update(){
    for(int current = 1; current < nodes.length; current++){
      int neighbor = current - 1;
      float currentDistance = dist(nodes[neighbor].position.x, nodes[neighbor].position.y, nodes[current].position.x, nodes[current].position.y);
      
      if(currentDistance > nodeDist){
        seek();
      }
      
      nodes[current].run();
    }
    
    nodes[0].position = anchor;
    nodes[1].position = anchor;
  }
  
  void seek(){
    for(int current = 1; current < nodes.length; current++){
      
      int neighbor = current - 1;
      PVector neighborPos = nodes[neighbor].position;
      PVector currentPos = nodes[current].position;
      
      PVector seek = PVector.sub(currentPos, neighborPos);
      seek.normalize();
      seek.mult(forceMax);
      
      nodes[current].acceleration.sub(seek);
    }
  }
  
  void display(){
    tenticleStyle();
    
    beginShape();
      for(int current = 0; current < nodes.length; current++){
        curveVertex(nodes[current].position.x, nodes[current].position.y);
      }
    endShape();
  }
  
  void tenticleStyle(){
   noFill();
   stroke(0, 80); 
   strokeWeight(1);
  }
 
}
