class Node{
  PVector position, velocity, acceleration;
  float speed, topSpeed, topForce;
  
 Node(PVector temp_position){
  position = temp_position;
  velocity = new PVector(0, 0);
  acceleration = new PVector(0, 0);
  speed = .8;
  topSpeed = 3;
  topForce = .3;
 }
 
 void run(){
   update();
   
   if(mousePressed){
     display();
   }
 }
 
 void follow(Force field){
   PVector desired = field.lookup(position);
   desired.mult(speed);
   
   PVector steer = PVector.sub(desired, velocity);
   steer.limit(topForce);
   acceleration.add(steer);
 }
 
 void update(){
   follow(field);
   
   velocity.add(acceleration);
   velocity.limit(topSpeed);
   position.add(velocity);
   
   acceleration.mult(0);
 }
 
 void display(){
   nodeStyle();
   ellipse(position.x, position.y, 5, 5);
 }
 
 void nodeStyle(){
  fill(255);
  stroke(0, 100);
  strokeWeight(1); 
 }
 
}
