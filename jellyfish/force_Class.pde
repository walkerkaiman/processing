class Force{
  PVector[][] field;
  PVector angleOffset;
  int cols, rows;
  int resolution;
  float forceRabbit;
  
 Force(int temp_resolution){
  resolution = temp_resolution;
  cols = width / resolution;
  rows = height / resolution;
  field = new PVector[cols][rows];
  angleOffset = new PVector();
  
  initiate();
 } 

 void run(){
   update();
   forceStyle();
   
   if(mousePressed){
     display();
   }
 }
 
 void update(){
   for (int x = 0; x < cols; x++) {
     float seedY = 100;
     
      for (int y = 0; y < rows; y++) {
        float theta = map(noise(forceRabbit + seedY + float(x)/10), 0, -1, 0, -PI);
       
        field[x][y] = new PVector(cos(theta), sin(theta));
        forceRabbit += .00001;
        seedY += .1;
      }
      
    }
 }
 
 void display(){
   for(int currentCol = 0; currentCol < cols; currentCol++) {
      for(int currentRow = 0; currentRow < rows; currentRow++) {
        drawVector(field[currentCol][currentRow], currentCol * resolution, currentRow * resolution, resolution-2);
      }
    }
 }
 
 void drawVector(PVector v, float x, float y, float scayl) {
    pushMatrix();
      float len = v.mag() * scayl;
      
      translate(x, y);
      rotate(v.heading2D());
      
      line(0, 0, len, 0);
    popMatrix();
  }
  
  PVector lookup(PVector lookup) {
    int column = int(constrain(lookup.x / resolution, 0, cols-1));
    int row = int(constrain(lookup.y / resolution, 0, rows-1));
    
    return field[column][row].get();
  }
  
  void forceStyle(){
   noFill();
  //noStroke();
  stroke(0, 100);
  strokeWeight(1); 
  }
  
  void initiate() {
    for (int x = 0; x < cols; x++) {
      for (int y = 0; y < rows; y++) {
        float theta = map(-noise(forceRabbit + x + y), 0, 1, 0, -PI);
       
        field[x][y] = new PVector(cos(theta), sin(theta));
        forceRabbit += 0.1;
      }
    } 
  }
  
}
