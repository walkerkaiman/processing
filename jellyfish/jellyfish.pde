int population = 1;
Force field;
Jelly[] jellyList; 

void setup(){
  size(600, 600);
  background(255);
  
  field = new Force(20);
  jellyList = new Jelly[population];
  
  for(int initialJelly = 0; initialJelly < jellyList.length; initialJelly++){
    jellyList[initialJelly] = new Jelly(initialJelly);
  }
}

void draw(){
  background(255);
  
  field.run();
  
  for(Jelly currentJelly : jellyList){
    currentJelly.run();
  }
  // FRAME RECORDER
  
  if(frameCount % 3 == 0 && frameCount < 1600 && frameCount > 700){
    println("RECORDING FRAME: " + frameCount);
    //saveFrame("/Users/Kman/Desktop/jellyfish/frame-####.png");
  }

}
