class Jelly{
  PVector[] anchors = new PVector[100];
  Tenticle[] tenticles = new Tenticle[anchors.length];
  
  PVector position;
  int seed;
  float bellContract, rabbit, size, anchorStep;
  
  Jelly(int temp_seed){
    seed = temp_seed;
    position = new PVector(width/2, 200);
    size = 200;
    
    for(int initTent = 0; initTent < anchors.length; initTent++){
      anchors[initTent] = new PVector(0, 0);
      tenticles[initTent] = new Tenticle(new PVector(anchors[initTent].x, anchors[initTent].y));
    }
  }  
  
  void run(){
    
    for(Tenticle current : tenticles){
      current.run();
    }
    
    update();
    display();
  }
  
  void display(){
   bellContract = map(cos(radians(rabbit*3+seed*100)), -1, 1, .5, 1);
   
   styleBell();
   
   beginShape();
     for(float angle = 0; angle > -180; angle -= 10){
       vertex( cos(radians(angle)) * size/2 * bellContract + position.x, sin(radians(angle)) * size/2 + position.y );
     }
     for(PVector currentAnchor : anchors){
       vertex(currentAnchor.x, currentAnchor.y);
     }
   endShape();
 }
 
 void update(){
   float anchorRabbit = rabbit/20;
    
   anchorStep = size/(anchors.length-1) * bellContract;
   
   anchors[0] = new PVector(position.x - (size/2 * bellContract), position.y);
   anchors[anchors.length-1] = new PVector(position.x + (size/2 * bellContract), position.y);
    
   for(int current = 1; current < anchors.length-1; current++){
      anchors[current] = new PVector((position.x - (size/2 * bellContract)) + (current * anchorStep), position.y + cos(anchorRabbit+current/2+seed*100)*5);
   }
   
   for(int index = 0; index < tenticles.length; index++){
      tenticles[index].anchor = anchors[index];
   }
   position.y += cos(seed+rabbit/30)*.8;
   rabbit ++;
 }

 void styleBell(){
   //noStroke();
   noFill();
   stroke(0, 100);
   strokeWeight(1);
   fill(255, 150);
 }

}

