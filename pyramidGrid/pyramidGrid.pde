import peasy.*;

PeasyCam cam;

int amount = 20;
int step = 40;
float edgeSize = step/2;
float time;

Pyramid[] pyramids = new Pyramid[amount * amount];

void setup () {
  cam = new PeasyCam(this, 1000);
  
  size(600, 600, P3D);
  stroke(255);
  strokeWeight(2);
  fill(0);
  
  int i = 0;
  
  for (int x = 0; x < amount; x++) {
    for (int y = 0; y < amount; y++) {
      PVector offset = new PVector(x * step, y * step);
      pyramids[i] = new Pyramid(offset);
      i++;
    }
  }
}

void draw () {
  background(0);
  
  for(Pyramid p : pyramids){
    p.update();
    p.display();
  }
  
  time += .05;
}