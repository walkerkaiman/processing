class Pyramid {
  PVector a, b, c, d, point;
  
  Pyramid (PVector offset) {
    a = new PVector(offset.x - edgeSize, offset.y - edgeSize, 0);
    b = new PVector(offset.x + edgeSize, offset.y - edgeSize, 0);
    c = new PVector(offset.x + edgeSize, offset.y + edgeSize, 0);
    d = new PVector(offset.x - edgeSize, offset.y + edgeSize, 0);
    point = new PVector(offset.x, offset.y, edgeSize);
  }
  
  void update () {
    point.z = cos(time + point.x + point.y) * edgeSize;
  }
  
  void display () {
    /*
    line(a.x, a.y, a.z, b.x, b.y, b.z); // A to B
    line(b.x, b.y, b.z, c.x, c.y, c.z); // B to C
    line(c.x, c.y, c.z, d.x, d.y, d.z); // C to D
    line(d.x, d.y, d.z, a.x, a.y, a.z); // D to A
    
    line(a.x, a.y, a.z, point.x, point.y, point.z); // A to Point
    line(b.x, b.y, b.z, point.x, point.y, point.z); // B to Point
    line(c.x, c.y, c.z, point.x, point.y, point.z); // C to Point
    line(d.x, d.y, d.z, point.x, point.y, point.z); // D to Point
    */
    
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(b.x, b.y, b.z);
      vertex(point.x, point.y, point.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(b.x, b.y, b.z);
      vertex(c.x, c.y, c.z);
      vertex(point.x, point.y, point.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(c.x, c.y, c.z);
      vertex(d.x, d.y, d.z);
      vertex(point.x, point.y, point.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(d.x, d.y, d.z);
      vertex(a.x, a.y, a.z);
      vertex(point.x, point.y, point.z);
    endShape(CLOSE);
  }
}