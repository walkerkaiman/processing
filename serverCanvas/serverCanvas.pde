import processing.net.*;

Server s;
Client c;

PVector[] clientPos = new PVector[2];
int[] data = new int[3];
String input;


void setup(){
  size(640, 480);
  background(0);

  s = new Server(this, 9000);
  
  for(int i = 0; i < clientPos.length; i++){
    clientPos[i] = new PVector(-10, -10);
  }
}

void draw(){
  background(0);
  
  c = s.available();
    
  if(c != null){
    input = c.readString();
    input = input.substring(0, input.indexOf("\n"));
    
    // Split values into an array
    data = int(split(input, ' ')); 
    
    // Save position in array
    clientPos[data[0]] = new PVector(data[1], data[2]);
  }
  
  for(int i = 0; i < clientPos.length; i++){
    // Draw circle
    if(i == 0){
      fill(255, 0, 0);
    }
    else if(i == 1){
      fill(0, 255, 0);
    }
    
    ellipse(clientPos[i].x, clientPos[i].y, 10, 10);
  }
}