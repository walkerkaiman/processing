/*
Boundary Class

*/

class Boundary{
  int padding, onTime, offTime, wallPos;
  PVector nw, ne, sw, se;
  boolean outside, riverR_gate, riverL_gate, caveR_gate, caveL_gate, caveBACK_gate, torchFORWARD_gate, torchBEHIND_gate, collect_gate;
  OscMessage riverR, riverL, caveR, caveL, caveBACK, torchFORWARD, torchBEHIND, collect, posMessage;
  
  Boundary(int temp_padding, int temp_onTime, int temp_offTime){
    padding = temp_padding;
    onTime = temp_onTime;
    offTime = temp_offTime;
    
    nw = new PVector(padding, padding);
    ne = new PVector(width - padding, padding);
    sw = new PVector(padding, height);
    se = new PVector(width - padding, height);
    
    outside = false;
    riverR_gate = false;
    riverL_gate = false;
    caveR_gate = false;
    caveL_gate = false;
    caveBACK_gate = false;
    torchFORWARD_gate = true;
    torchBEHIND_gate = true;
    collect_gate = true;
    
    //OSC Messages
    riverR = new OscMessage("/river_R");
    riverL = new OscMessage("/river_L");

    caveR = new OscMessage("/cave_R");
    caveL = new OscMessage("/cave_L");
    caveBACK = new OscMessage("/cave_BACK");
    
    torchFORWARD = new OscMessage("/torch_FORWARD");
    torchBEHIND = new OscMessage("/torch_BEHIND");
    
    posMessage = new OscMessage("/pos");
    collect = new OscMessage("/collect");
  } 
  
  void checkOutside(PVector pos){
    char region = 'x';
    
    //North Boundary
    if(pos.y < nw.y){   
      if(outside == false){
        timer.startTimer();
      }
      outside = true;
      region = 'n';
    }
    
    //West Boundary
    if(pos.x < nw.x){   
      if(outside == false){
        timer.startTimer();
      }
      outside = true;
      region = 'w';
    }
    
    //East Boundary
    if(pos.x > ne.x){   
      if(outside == false){
        timer.startTimer();
      }
      outside = true;
      region = 'e';
    }
    
    //NW Corner
    if(pos.y < nw.y && pos.x < nw.x){
      if(pos.y > pos.x){
        region = 'w';
      }
      else{
        region = 'n';
      }
    }
    
    //NE Corner
    if(pos.y < nw.y && pos.x > ne.x){
      float boundary = -pos.y + width;
      
      if(boundary > pos.x){
        region = 'n';
      }
      else{
        region = 'e';
      }
    }
    
    //Inside Boundaries
    if(pos.y > nw.y && pos.x < ne.x && pos.x > nw.x){  
      outside = false;
      collect_gate = true;
      torchBEHIND_gate = true;
      torchFORWARD_gate = true;
      timer.stopTimer();
      //println("Inside boundries...");
    }
    
    wallPos = convert_1D(pos, region);
    
    
    //checks to see if tracker is in the RIVER LEFT region
    if(wallPos > projectorRes.x && wallPos < projectorRes.x*2 - projectorRes.x/2){
      riverL_gate = true;
    }
    
    //checks to see if it is in the RIVER RIGHT region
    if(wallPos > projectorRes.x*2 - projectorRes.x/2 && wallPos < projectorRes.x*2){
      riverR_gate = true;
    }
    
    //checks to see if it is in the CAVE LEFT region
    if(wallPos > projectorRes.x*2 && wallPos < projectorRes.x*3 - projectorRes.x/2){
      caveL_gate = true;
    }
    
    //checks to see if it is in the CAVE RIGHT region
    if(wallPos > projectorRes.x*3 - projectorRes.x/2 && wallPos < projectorRes.x*3){
      caveR_gate = true;
    }
    
    //checks to see if it is in the CAVE BACK region
    if(wallPos > projectorRes.x/3 && wallPos < projectorRes.x-projectorRes.x/3){
      caveBACK_gate = true;
    }
    
    trigger(pos, region);
  }
  
  void trigger(PVector pos, char region){
    
    if(outside == true){
      int elapsed = timer.getElapsedTime();
      wallPos = convert_1D(pos, region);
      
      //Sometimes a value of 0 is triggered if the tracker registers on a boundry line.
      if(wallPos > 0){
        //position is streamed when the tracker is outside the boundary
        posMessage = new OscMessage("/pos");
        posMessage.add(wallPos);
        osc.send(posMessage, broadcast);
        println("/pos " + posMessage.get(0).intValue());
      }
      
      //collect message is sent when tracker is outside for 2 seconds
      if(elapsed > 2000 && collect_gate == true){
        osc.send(collect, broadcast);
        collect_gate = false;
        println("/collect");
      }
      
      //checks to see if the tracker is close to the torch regions for two seconds then sends message.
      if(torchBEHIND_gate == true && dist(pos.x, pos.y, 0, 0) < padding && elapsed > 2000){
        osc.send(torchBEHIND, broadcast);
        torchBEHIND_gate = false;
        println("/torchBEHIND");
      }
      
      if(torchFORWARD_gate == true && dist(pos.x, pos.y, width, 0) < padding && elapsed > 2000){
        osc.send(torchFORWARD, broadcast);
        torchFORWARD_gate = false;
        println("/torchFORWARD");
      }
    }
    
    //sends riverL message as tracker leaves the region
    if(riverL_gate == true && outside == false){
        osc.send(riverL, broadcast);
        riverL_gate = false;
        println("/riverL");
      }
    
    //sends riverR message as tracker leaves the region
    if(riverR_gate == true && outside == false){
      osc.send(riverR, broadcast);
      riverR_gate = false;
      println("/riverR");
    }
    
    //sends caveL message as tracker leaves the region
    if(caveL_gate == true && outside == false){
      osc.send(caveL, broadcast);
      caveL_gate = false;
      println("/caveL");
    }
    
    //sends caveR message as tracker leaves the region
    if(caveR_gate == true && outside == false){
      osc.send(caveR, broadcast);
      caveR_gate = false;
      println("/caveR");
    }
    
    //sends caveBACK message as tracker leaves the region
    if(caveBACK_gate == true && outside == false){
      osc.send(caveBACK, broadcast);
      caveBACK_gate = false;
      println("/caveBACK");
    }
  }
  
  int convert_1D(PVector pos, char boundary){
    float wall = 0;
    
    switch(boundary){
      case 'w':
        wall = map(height - pos.y, 0, height, 0, projectorRes.x);
        break; 
      case 'n':
        wall = map(pos.x, 0, width, 0, 1920);
        wall += projectorRes.x;
        break;
      case 'e':
        wall = map(pos.y, 0, height, 0, projectorRes.x);
        wall += projectorRes.x*2;
        break;
    }
  
    return int(wall);
}
  
  void displayBounds(){
    //Trigger bounds in red
    noFill();
    strokeWeight(5);
    stroke(255, 0, 0);
    
    line(width/2, 0, width/2, padding);
    line(width-padding, height/2, width, height/2);
    line(padding, height/3, 0, height/3);
    line(padding, height-height/3, 0, height-height/3);
    
    //torch trigger boundaries
    ellipse(0, 0, padding*2, padding*2);
    ellipse(width, 0, padding*2, padding*2);
    
    //Boundries in white
    stroke(255);
    
    //North Wall
    line(nw.x, nw.y, ne.x, ne.y);
    
    //West Wall
    line(nw.x, nw.y, sw.x, sw.y);
    
    //East Wall
    line(ne.x, ne.y, se.x, se.y);
  }
}