import oscP5.*;
import netP5.*;

OscP5 osc;
NetAddress broadcast;
Tracker centroid;
Boundary bounds;
Timer timer;

PVector projectorRes;

void setup(){
  size(400, 400);
  
  projectorRes = new PVector(1920, 1080);
  centroid = new Tracker(30);
  
  //padding, onTime, offTime
  bounds = new Boundary(50, 2000, 2100);
  timer = new Timer();
  broadcast = new NetAddress("127.0.0.1", 12000);
  osc = new OscP5(this, "127.0.0.1", 12000);
}

void draw(){
  background(0);
  
  //uncomment to test tracker as mouse position
  centroid.pos = new PVector(mouseX, mouseY);

  bounds.checkOutside(centroid.pos);
  bounds.displayBounds();
  centroid.display();
}

//Receives messages from TSPS and updates the tracker's position when appropriate
void oscEvent(OscMessage incoming){
  boolean centroidMessage = incoming.checkAddrPattern("/TSPS/personUpdated/");

  if(centroidMessage == true){
    float x = incoming.get(3).floatValue()*width;
    float y = incoming.get(4).floatValue()*height;
    
    centroid.pos = new PVector(x, y);
  }
}