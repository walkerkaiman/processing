class Timer{
  int startTime, stopTime;
  boolean running;
  
  Timer(){
    startTime = 0;
    stopTime = 0;
    running = false;
  }
  
  void startTimer(){
    startTime = int(millis());
    running = true;
  }
  
  void stopTimer(){
    stopTime = int(millis());
    running = false;
  }
  
  int getElapsedTime(){
    int elapsed;
    
    if(running){
      elapsed = (millis()) - startTime; 
    }
    else{
      elapsed = 0; 
    }
    
    return elapsed;
  }
}
