class Tracker{
  PVector pos;
  int size;
 
  Tracker(int temp_size){
    pos = new PVector(width/2, height/2);
    size = temp_size;
  }
 
  void display(){
    noStroke();
    fill(0, 255, 0);
    
    ellipse(pos.x, pos.y, size, size);
  }
}
