import peasy.*;
PeasyCam cam;

float radius = 100;
float rabbit;

void setup(){
  size(displayHeight, displayHeight, P3D);
  cam = new PeasyCam(this, 100);
  
  strokeWeight(1);
}

void draw(){ 
  background(255);
  smooth(4);
  
  for(int depth = 0; depth <= 100; depth += 1){

    fill(0, 0, 255, 10);
    stroke(0);
    rotateZ(rabbit*18);
    
    beginShape();
      
      for(float ang = 0; ang <= 360; ang += 45){
        
        float variance = cos(noise(ang)) * 100;
        float currentX = cos(ang) * (radius-depth*5) + variance;
        float currentY = sin(ang) * (radius-depth*5) + variance;
        
        curveVertex(currentX, currentY, depth * 10);
      }
    endShape(CLOSE);
    rabbit += .000005;
  }
}

