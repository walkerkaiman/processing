class Pixel {
  PVector pos, size;
  color fill;
  
  Pixel(PVector _pos, PVector _size){
    pos = _pos;
    size = _size;
  }
  
  void run(){
    update();
    display();
  }
  
  void update(){
    fill = color(random(255));
    
    /*
    float wave = cos(time + pos.x/50 + pos.y/10) * 10;
    size.y = noise(time/10, pos.x/100, pos.y/10) * 40 + wave;
    */
  }
  
  void display(){
    noStroke();
    //strokeWeight(1);
    fill(fill);
    
    rect(pos.x, pos.y, size.x, size.y);
  }
}