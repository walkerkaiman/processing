ArrayList<Pixel>pixelz = new ArrayList<Pixel>();
float temp_x, temp_y, time, size;

void setup() {
  size(640, 480);
  frameRate(24);
  
  size = 10;
  
  for (int x = 0; x < width; x += size) {
    pixelz.add(new Pixel(new PVector(x, 0), new PVector(size, height)));
  
    //temp_x += size;
  }
}

void draw() {
  background(0);
 
  for (Pixel p : pixelz) {
    p.run();
  }
  
  if (frameCount < 240) { 
    saveFrame("/Users/kaimanwalker/Desktop/frames/frame_#####.png");
  }

  time += .1;
}