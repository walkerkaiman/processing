import oscP5.*;
OscP5 oscP5;

int found;
float speed = 100;
float radius;
PVector posePosition;

void setup() {
  size(1080, 720);
  
  oscP5 = new OscP5(this, 8338);
  oscP5.plug(this, "found", "/found");
  oscP5.plug(this, "posePosition", "/pose/position");
  oscP5.plug(this, "mouthHeight", "/gesture/mouth/height");
  
  posePosition = new PVector();
  
  background(0);
  noFill();
  stroke(255, 150);
  strokeWeight(2);
}

void draw() {  
  fade();
  
  if (found > 0) {
    float x = map(posePosition.x, 146, 194, 0, width);
    float y = map(posePosition.y, 123, 346, 0, height);
    
    ellipseMode(CENTER);
    
    ellipse(x, y, radius, radius);
  }
}

public void found(int i) {
  found = i;
}

public void posePosition(float x, float y) {
  println("pose position\tX: " + x + " Y: " + y);
  posePosition.set(x, y);
}

public void mouthHeight(float h) {
  radius = h * 100;
}

// all other OSC messages end up here
void oscEvent(OscMessage m) {
  if (m.isPlugged() == false) {
    
  }
}

void fade(){
  noStroke();
  fill(0, 20);
  rect(0, 0, width, height);
}