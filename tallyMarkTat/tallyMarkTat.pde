int picture = 0;

void setup(){
  size(400, 800);
}

void draw(){
  background(255);
  stroke(0);
  noFill();
  
  strokeWeight(4);
  smooth();
  
  float space = 20;
  float markHeight = 51;
  
  for(float y = space; y < height-markHeight; y += space+markHeight){
    for(float x = space; x < width; x += 5*space){
    float xpos = 0;
    
    for(float tick = 0; tick < 4; tick++){
      xpos = tick * space;
      
      line(xpos+x, y, xpos+x, y+markHeight);
    }
    
    line(x, y, xpos+x, y+markHeight);
    }
  }
  rect(5, 5, width-10, height-10);
}

void mouseReleased(){
  saveFrame("/Users/kaimanwalker/Desktop/tattoo/"+picture+".png"); 
  picture++;
}