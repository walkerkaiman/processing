import peasy.*;
PeasyCam cam;

float[] peakHeight = new float[1600];
float rabbit = 0;

void setup(){
  size(displayHeight, displayHeight, P3D);
  cam = new PeasyCam(this, 1000);
  
  strokeWeight(2);
  stroke(255, 200);
}

void draw(){
  smooth(4);
  background(0);
  
  calc_Peak();
  rotateY(radians(90));
  rotateZ(radians(-25));
  translate(-600, -300, width);
  draw_Waves();
  rabbit += .01;
}


void calc_Peak(){
  int indexPeak = 0;
  
  for(int x = 0; x < sqrt(peakHeight.length); x++){

    
    for(int y = 0; y < sqrt(peakHeight.length); y++){
      float offset = noise(x + rabbit, y) * 100;
      
      if(indexPeak < peakHeight.length){
        peakHeight[indexPeak] = offset;
        indexPeak ++;
      }
    }
  }
}
  
void draw_Waves(){
  int indexHeight = 0;
  float Height = 50;
  
  for(int x = 0; x < sqrt(peakHeight.length); x++){
    beginShape();
      float map = map(x, 0, sqrt(peakHeight.length), 0, 255);
      color fill = color(255-map, 255-map*.6, 255);
      fill(fill);
      
      for(int z = 0; z < sqrt(peakHeight.length); z++){
        if(indexHeight < peakHeight.length){
          vertex(x * Height, peakHeight[indexHeight], -z * Height);
          indexHeight ++;
        }
      }
        vertex(x * Height, 200, sqrt(peakHeight.length) * -Height);
        vertex(x * Height, 200, 0);
      endShape(CLOSE);
  }

}
