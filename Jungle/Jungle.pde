/*/////////   FOREST ROOM   //////////

THIS SKETCH RECIEVES OSC MESSAGES FROM THE TRACKING PROGRAM.

THIS SKETCH KEEPS TRACK OF WHAT ROOM THE USER IS CURRENTLY IN AND 
SENDS MESSAGES TO THE LED EGG WITH INFORMATION THAT TRIGGERS IT TO 
DISPLAY THE APPROPRIATE COLOR DEPENDING ON WHAT ROOM THE EGG IS LOCATED IN.

WHEN A USER CHOOSES A PATH, THIS SKETCH WILL SEND A MESSAGE
TO THE QLAB COMPUTER OVER OSC.
____________________________________________________________________________

OSC MESSAGE PROTOCOL

SOME OSC LIBRARIES WON'T RECOGNIZE A MESSAGE WITH A NULL VALUE. THIS IS THE
REASON FOR THE 1 AT THE END OF THE MESSAGE.
"/correct 1"
"/incorrect 1"

KEYBOARD CONTROLS

LEFT - SENDS MESSAGE TO EGG TO DISPLAY THE CURRENT LEFT DOOR COLOR.
RIGHT - SENDS MESSAGE TO EGG TO DISPLAY THE CURRENT RIGHT DOOR COLOR.
R OR r - RESTARTS THE PUZZLE. SENDS PARTICIPANT BACK TO THE FIRST ROOM.
S OR s - TRIGGERS THE COLOR SEQUENCE TO PLAY.
I OR i - INCREMENTS THE ROOM INDEX BY ONE. 
F or f - TRIGGER THE FLICKER SEQUENCE TO RUN. HIT 'F' AGAIN TO TURN IT OFF. 
P or p - TRIGGER THE PULSE SEQUENCE TO RUN. HIT 'P' AGAIN TO TURN IT OFF.
_____________________________________________________________________________

NOTES

BE SURE THAT THE QLAB IP IS CORRECT IN THE CONSTRUCTOR.

THE CORRECT COLOR MAY NOT BE DISPLAYED AT THE GUI. HOWEVER, THE CORRECT COLOR 
WILL BE SHOWN ONCE THE OPERATOR HITS THE RIGHT AND LEFT KEYS.

YOU CAN ONLY PLAY EITHER THE FLICKER OR PULSE SEQUENCE AT A TIME. PLAYING ONE TURNS
THE OTHER OFF.

CLICK INSIDE THE CORRECT WINDOW IF THE MESSAGE DON'T APPEAR TO BE TRIGGERING. YOU MUST
HAVE THE WINDOW OF THE SKETCH SELECTED FOR THE KEYBOARD TO INTERACT WITH IT.
*/

import hypermedia.net.*;
import netP5.*;
import oscP5.*;

UDP udp;
OscP5 osc;

NetAddress QLab_Address;

OscMessage incorrect, correct;

int[] correctDoor;
int[] correctColor;
int[] incorrectColor;
byte[] buffer;

int index, fillL, fillR;

boolean flicker;
boolean pulse;

String torchIP = "192.168.6.13";

void setup(){
  size(300, 150);
  
  correctDoor = new int[20];
  correctColor = new int[20];
  incorrectColor = new int[20];
  
  //SAVES THE MESSAGE TO BE SENT TO THE EGG AS A BYTE.
  buffer = new byte[1];
  
  torchIP = "";
  //SAVES THE SEQUENCE OF THE CORRECT DOORS.
  correctDoor[0] = 0;
  correctDoor[1] = 0;
  correctDoor[2] = 0;
  correctDoor[3] = 1;
  correctDoor[4] = 0;
  correctDoor[5] = 1;
  correctDoor[6] = 0;
  correctDoor[7] = 0;
  correctDoor[8] = 0;
  correctDoor[9] = 1;
  correctDoor[10] = 1;
  correctDoor[11] = 0;
  correctDoor[12] = 1;
  correctDoor[13] = 1;
  correctDoor[14] = 0;
  correctDoor[15] = 1;
  correctDoor[16] = 0;
  correctDoor[17] = 0;
  correctDoor[18] = 0;
  correctDoor[19] = 1;
  
  //SAVES SEQUENCE OF COLORS THAT INDICATES WHICH DOOR IS THE CORRECT DOOR.
  correctColor[0] = 3;
  correctColor[1] = 0;
  correctColor[2] = 2;
  correctColor[3] = 1;
  correctColor[4] = 3;
  correctColor[5] = 3;
  correctColor[6] = 1;
  correctColor[7] = 1;
  correctColor[8] = 3;
  correctColor[9] = 1;
  correctColor[10] = 0;
  correctColor[11] = 3;
  correctColor[12] = 3;
  correctColor[13] = 2;
  correctColor[14] = 3;
  correctColor[15] = 1;
  correctColor[16] = 2;
  correctColor[17] = 0;
  correctColor[18] = 0;
  correctColor[19] = 3;
  
  //SAVE THE SEQUENCE OF COLORS THAT INDICATES THE INCORRECT DOOR.
  incorrectColor[0] = 0;
  incorrectColor[1] = 2;
  incorrectColor[2] = 3;
  incorrectColor[3] = 2;
  incorrectColor[4] = 1;
  incorrectColor[5] = 2;
  incorrectColor[6] = 3;
  incorrectColor[7] = 0;
  incorrectColor[8] = 2;
  incorrectColor[9] = 2;
  incorrectColor[10] = 1;
  incorrectColor[11] = 0;
  incorrectColor[12] = 2;
  incorrectColor[13] = 0;
  incorrectColor[14] = 0;
  incorrectColor[15] = 0;
  incorrectColor[16] = 3;
  incorrectColor[17] = 1;
  incorrectColor[18] = 3;
  incorrectColor[19] = 2;
  
  //SAVES WHICH ROOM THE PARTICIPANT IS CURRENTLY LOCATED (0-19).
  index = 0;
  
  //SAVES THE CURRENT COLOR OF THE RIGHT AND LEFT DOORS.
  fillL = correctColor[0];
  fillR = incorrectColor[0];
  
  //SEND MESSAGES TO THE EGG
  //UPD(THIS, SEND PORT)
  udp = new UDP(this, 8888);
  
  //IMCOMING MESSAGES FROM LOCAL TRACKER AND TRIGGER SKETCH
  //OSC(THIS, LISTENING PORT)
  osc = new OscP5(this, 7777);
  
  //THIS IS THE IP ADDRESS AND THE PORT NUMBER OF THE MESSAGES SEND TO QLAB.
  //QLab_Address = new NetAddress("", 5555);
  
  //CORRECT MESSAGE
  correct = new OscMessage("/correct");
  
  //INCORRECT MESSAGE
  incorrect = new OscMessage("/incorrect");
  
  //GATES TO TRIGGER MESSAGES
  flicker = false;
  pulse = false;
}

void draw(){ 
  background(0);
  
  if(flicker){
    runFlicker();  
  }
  else if(pulse){
    runPulse();
  }
  
  displayGUI();
}

void runPulse(){
  if(frameCount % 60 == 0){
    colorTranslate(2);
  }
}

void runFlicker(){
    if(frameCount % 5 == 0){
      colorTranslate(0);
    }
    else if (frameCount % 2 == 0){
      colorTranslate(3);
    }
}

void displayGUI(){
  
  //DISPLAY LEFT DOOR TO GUI
  switch(fillL){
    case 0:
      fill(255, 0, 0);
      break;
    case 1:
      fill(0, 255, 0);
      break;
    case 2:
      fill(0, 0, 255);
      break;
    case 3:
      fill(255, 255, 0);
      break;
  }
  rect(10, 10, 100, 20);
  
  //DISPLAY RIGHT DOOR TO GUI
  switch(fillR){
    case 0:
      fill(255, 0, 0);
      break;
    case 1:
      fill(0, 255, 0);
      break;
    case 2:
      fill(0, 0, 255);
      break;
    case 3:
      fill(255, 255, 0);
      break;
  }
  rect(width - 110, 10, 100, 20);
  
  //DISPLAYS CURRENT INDEX TO GUI.
  textSize(32);
  fill(255);
  text("Current Index: " + index, 20, height/2 + 20);
}

/*
_______OSC MESSAGES_______
THIS FUNCTION WILL TRIGGER WHEN OSC MESSAGES ARE RECIEVED.

THE LOGIC OF THIS FUNCTION KEEPS TRACK OF WHAT ROOM THE USER
IS CURRENTLY IN WITH index.

THE IDEA IS THAT THE EGG WILL KNOW WHAT VIRTUAL ROOM IT IS IN AT ALL TIMES.
THE OPERATOR ONLY NEEDS TO HIT THE LEFT AND RIGHT KEYS AS THE EGG 
APROACHES THE LEFT AND RIGHT DOORS.

*/

void oscEvent(OscMessage incoming){

  if(correctDoor[index] == 0){
    if(incoming.checkAddrPattern("/river_L")){
      
      osc.send(correct, QLab_Address);
      println("Correct door chosen. Advancing.");
      
      if(index < correctDoor.length-1){
        index ++;
      }
    }
    else if(incoming.checkAddrPattern("/river_R")){
      osc.send(incorrect, QLab_Address);
      index = 0;
      
      println("Incorrect door chosen. Restarting.");
    }
  }
  else if(correctDoor[index] == 1){
    if(incoming.checkAddrPattern("/river_R")){
      
      osc.send(correct, QLab_Address);
      println("Correct door chosen. Advancing.");
      
      if(index < correctDoor.length-1){
        index ++;
      }
    }
    else if(incoming.checkAddrPattern("/river_L")){
      osc.send(incorrect, QLab_Address);
      index = 0;
      
      println("Incorrect door chosen. Restarting.");
    }
  }
  
}

/*
_______COLOR TRANSLATE_______
THIS FUNCTION IS PASSED AN INTEGER COLOR CODE.
TRANSLATES THE INTEGER VALUE TO APPROPRIATE COLOR LETTER.
SENDS THE APPROPRIATE COLOR LETTER TO THE EGG VIA UDP.

*/

void colorTranslate(int colorCode){
  
  switch(colorCode){
    case 0:
      buffer[0] = 'R';
      break;
    case 1:
      buffer[0] = 'G';
      break;
    case 2:
      buffer[0] = 'B';
      break;
    case 3:
      buffer[0] = 'Y';
      break;
  }
  
  udp.send(buffer, "192.168.6.13");
}

/*
_______KEYBOARD CONTROLS_______

WHEN A PARTICIPANT HOVERS THE EGG OVER THE LEFT DOOR, 
THE OPERATOR HITS THE LEFT KEY. 

WHEN A PARTICIPANT HOVERS THE EGG OVER THE RIGHT DOOR,
THE OPERATOR HITS THE RIGHT KEY.

THE RESULT IS THAT A MESSAGE WILL BE SENT TO THE EGG VIA THE colorTranslate FUNCTION
THAT WILL CUE THE EGG TO GLOW THE APPROPRIATE COLOR.

CONTROLS
LEFT - MAKES THE EGG GLOW THE CORRECT COLOR OF THE LEFT DOOR.
RIGHT - MAKES THE EGG GLOW THE CORRECT COLOR OF THE RIGHT DOOR.
I OR i - INCREASES THE ROOM INDEX BY ONE. CAN BE USED IF THE EGG NEEDS TO BE PUT IN A SPECIFIC ROOM.
R OR r - RESTART. SENDS THE PARTICIPANT BACK TO ROOM ZERO.
S OR s - TRIGGERS TO PLAY THE COLOR SEQUENCE.
F or f - TRIGGER THE FLICKER SEQUENCE TO RUN. HIT 'F' AGAIN TO TURN IT OFF. 
P or p - TRIGGER THE PULSE SEQUENCE TO RUN. HIT 'P' AGAIN TO TURN IT OFF.

NOTE: THE SEQUENCE CAN ONLY BE TRIGGERED IF THE PARTICIPANT IS IN THE FIRST ROOM.
      THIS IS TO KEEP THE OPERATOR FROM ACCIDENTLY TRIGGERING THE SEQUENCE.
*/

void keyPressed(){
  
  if(key == CODED){
    
    //DOES THE OPERATOR HIT THE LEFT KEY?
    if(keyCode == LEFT){
      if(correctDoor[index] == 0){        
        fillL = correctColor[index];
      }
      else{
        fillL = incorrectColor[index];
      }
      colorTranslate(fillL);
    }
    
    //DOES THE OPERATOR HIT THE RIGHT KEY?
    else if(keyCode == RIGHT){
      if(correctDoor[index] == 1){
        fillR = correctColor[index];
      }
      else{
        fillR = incorrectColor[index];
      }
      colorTranslate(fillR);
    }
  }
  
  if(key == 'i' || key == 'I'){
    if(index < correctDoor.length-1){
      index ++;
    }
  }
  
  if(key == 's' || key == 'S'){
    buffer[0] = 'S';
    
    if(index == 0 && flicker == false){
      udp.send(buffer, "192.168.6.13");
      println("Play Sequence...");
    }
  }
  
  if(key == 'r' || key == 'R'){
    println("Restarting. Room 0.");
    index = 0;
  }
  
  if(key == 'f' || key == 'F'){
    flicker =! flicker;
    pulse = false;
    println("Flickering: "+flicker);
  }
  
  if(key == 'p' || key == 'P'){
    pulse =! pulse;
    flicker = false;
    println("Pulsing: "+pulse);
  }
  
  
}