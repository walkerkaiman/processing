class Flow {
  PVector pos;
  float size;
  float rot;

  Flow(PVector pos) {
    this.pos = pos;
  }

  void updateRot(int brightness) {
    rot = map(brightness, 0, 255, 0, 360);
  }

  void display() {
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(radians(rot));
    line(0, 0, DENSITY, 0);
    popMatrix();
  }
}