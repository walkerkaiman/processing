import processing.video.*;
Capture cam;
ArrayList<Flow>field = new ArrayList<Flow>();

final color BLACK = color(0);
final color WHITE = color(255);
final float STROKE_WEIGHT = 1;
final float DENSITY = 10;

void setup () {
  printArray(Capture.list());
  cam = new Capture(this, Capture.list()[3]);
  cam.start();

  size(640, 360);
  stroke(WHITE);
  strokeWeight(STROKE_WEIGHT);

  for (int y = 0; y < height; y += DENSITY) {
    for (int x = 0; x < width; x += DENSITY) {
      field.add(new Flow(new PVector(x, y)));
    }
  }
}

void captureEvent (Capture c) {
  c.read();
}

void draw() {
  background(BLACK);

  for (Flow f : field) {
    float brightness = brightness(cam.get(int(f.pos.x), int(f.pos.y)));
    f.updateRot(int(brightness));
    f.display();
  }
}