import codeanticode.syphon.*;
import gab.opencv.*;
import processing.video.*;

Capture cam;
OpenCV opencv;
SyphonServer server;


void setup()  {
  size(640, 360);
  
  cam = new Capture(this, 640, 360);
  opencv = new OpenCV(this, cam);
  server = new SyphonServer(this, "Syphon from Processing");
  
  cam.start();
}

void draw()  {
  
  if(cam.available()){
    cam.read(); 
  }
  
  background(0);
}