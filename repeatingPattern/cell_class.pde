class Cell {
  color fill;
  PVector pos;
  float size;
  
  Cell (color fill, PVector pos, float size) {
    this.fill = fill;
    this.pos = pos;
    this.size = size;
  }
  
  void display () {
    fill(fill);
    
    if(fill == black){
      noStroke();
      rect(pos.x, pos.y, size, size);
    }else{
      stroke(white);
      rect(pos.x, pos.y, size, size);
    }
    
  }
}