void initCells (int patternSize) {
  cells = new ArrayList<Cell>();
  int stateIndex = 0;
  
  for (int y = 0; y < height; y+=cellSize) {
    for (int x = 0; x < width; x += cellSize) {
      color fill = state.get(stateIndex) == 1 ? white : black;
      float size = cellSize;
      
      cells.add(new Cell(fill, new PVector(x, y), size));
      
      stateIndex++;
      stateIndex = stateIndex % patternSize;
    }
  }
}