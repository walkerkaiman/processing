final color white = color(255);
final color black = color(0);
final float cellSize = 20;

ArrayList<Cell> cells;
IntList state;

void setup () {
  size(800, 200);
  init();
}

void draw(){
  background(black);
 
  for (Cell c : cells) {
    c.display();
  }
}