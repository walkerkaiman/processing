void init () {
  int patternSize = int(random(10, 50));
  
  initStates(patternSize);
  initCells(patternSize);
}