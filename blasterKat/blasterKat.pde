import java.awt.Rectangle;

int gridscale = 40;              // Scale of grid is 1/24 of screen size

// DNA needs one vector for every spot on the grid 
// (it's like a pixel array, but with vectors instead of colors)
int dnasize;

int lifetime;  // How long should each generation live


Population population;  // Population
int lifecycle;          // Timer for cycle of generation
int recordtime;         // Fastest time to target
Obstacle target;        // Target location
Obstacle start;         // Start location
int diam = 2;          // Size of target

ArrayList<Obstacle> obstacles;  //an array list to keep track of all the obstacles!

boolean debug = false;

Rectangle newObstacle = null;

void setup() {
  size(800, 800);
  dnasize = (width / gridscale) * (height / gridscale); 
  lifetime = width/3;

  // Initialize variables
  lifecycle = 0;
  recordtime = lifetime;
  target = new Obstacle(width-diam-diam/2,height/2-diam/2,diam,diam);
  start = new Obstacle(diam/2+100,height/2-diam/2,diam,diam);

  // Create a population with a mutation rate, and population max
  int popmax = 6000;
  float mutationRate = 0.02;
  population = new Population(mutationRate,popmax);

  // Create the obstacle course  
  obstacles = new ArrayList<Obstacle>();
}

void draw() {
  background(255);

  // If the generation hasn't ended yet
  if (lifecycle < lifetime) {
    population.live(obstacles);
    if ((population.targetReached()) && (lifecycle < recordtime)) {
      recordtime = lifecycle;
    }
    lifecycle++;
  // Otherwise a new generation
  } else {
    lifecycle = 0;
    population.calcFitness();
    population.naturalSelection();
    population.generate();
  }

   if (newObstacle != null) {
     rect(newObstacle.x,newObstacle.y,newObstacle.width,newObstacle.height); 
   }
   
   println("Generation: "+population.getGenerations());
   
   if(population.getGenerations() == 100){
     saveFrame("/Users/kaimanwalker/Desktop/blasterFrames/blasterFrames-#####.png");
   }
}
