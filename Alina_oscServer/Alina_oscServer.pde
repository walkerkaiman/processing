import netP5.*;
import oscP5.*;

OscP5 osc;
NetAddress remote;
NetAddress local;
OscMessage layer1, layer2, layer3, layer4;

ArrayList<Button> buttons = new ArrayList<Button>();
Button one, two, three, four, five, six, seven, eight;

void setup(){
  size(500, 500);
  
  stroke(255);
  strokeWeight(3);
  fill(200);
  
  osc = new OscP5(this, 9000);
  remote = new NetAddress("192.168.0.4", 9000);
  local = new NetAddress("192.168.0.1", 9000);
  
  one = new Button(new PVector(width/2, 20), 0);
  two = new Button(new PVector(100, height/2), 1);
  three = new Button(new PVector(height-100, height/2), 2);
  four = new Button(new PVector(width-25, height-25), 3);
  five = new Button(new PVector(20, height-25), 4);
  six = new Button(new PVector(45, height-25), 5);
  seven = new Button(new PVector(70, height-25), 6);
  eight = new Button(new PVector(95, height-25), 7);
  
  buttons.add(one);
  buttons.add(two);
  buttons.add(three);
  buttons.add(four);
  buttons.add(five);
  buttons.add(six);
  buttons.add(seven);
  buttons.add(eight);
}

void draw(){
  background(0);
  
  for(Button current : buttons){
    current.run();
  }
  
  displayScreen();
}

void displayScreen(){
 line(50, 50, width-50, 50); 
 line(width/2, 50, width/2, height-50);
}

void mouseClicked(){
  
  for(Button current : buttons){
    if(current.inside){
      current.activate =! current.activate;
    }
    
    if(current.activate){
      //Top Position
      if(current.i == 0){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */
      }
      //Left Position
      else if(current.i == 1){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */
      }
      //Right Position
      else if(current.i == 2){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */
      }
      //Blackout
      else if(current.i == 3){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */    
      }
      //Preset 4
      else if(current.i == 4){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */    
      }
      //Preset 5
      else if(current.i == 5){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */    
      }
      //Preset 6
      else if(current.i == 6){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */    
      }
      //Preset 7
      else if(current.i == 7){
        /*
        osc.send(myMessage, remote); 
        osc.send(myMessage, local); 
        */    
      }
    }
  }
  
}