class Button{
  PVector pos;
  color fill;
  boolean activate, inside;
  int i;
  
  Button(PVector temp_pos, int temp_i){
    pos = temp_pos;
    i = temp_i;
    fill = color(200, 200, 200);
    activate = false;
    inside = false;
  }
  
  void run(){
    select();
    display();
  }
  
  void display(){
    fill(fill);
    ellipse(pos.x, pos.y, 20, 20);
  }
  
  void select(){
    float mouseOver = dist(mouseX, mouseY, pos.x, pos.y);
    
    if(mouseOver < 20){
      inside = true;
    }
    else{
      inside = false;
    }
    
    if(activate){
      fill = color(255, 0, 0);
    }
    else{
      fill = color(200, 200, 200);
    }
    
    if(activate && inside){
      for(int other = 0; other < buttons.size(); other++){
        if(other != i){
          buttons.get(other).activate = false;
        }
      }
    }
    
  }
  
}