import SimpleOpenNI.*;
SimpleOpenNI kinect;


PVector UserOnePos;
PVector UserTwoPos;
PVector UserThreePos;

void setup(){
  size(640, 480);
  
  kinect = new SimpleOpenNI(this);
  kinect.enableDepth();
  kinect.enableUser();
  
  stroke(255);
}

void draw(){
  kinect.update();
  image(kinect.depthImage(), 0, 0);
  
  IntVector userList = new IntVector();
  kinect.getUsers(userList);
  
  PVector[] IndividualPosition = new PVector[3];
 
  
  for(int i = 0; i < userList.size(); i++){
    int userId = userList.get(i);
    PVector position = new PVector();
    kinect.getCoM(userId, position);
    
    kinect.convertRealWorldToProjective(position, position);
    
    
     switch(userId){
      case 1:
        IndividualPosition[0] = position;
        UserOnePos = IndividualPosition[0];
        
        fill(255, 0, 0);
        ellipse(position.x, position.y, 25, 25);
        break;
        
      case 2:
        IndividualPosition[1] = position;
        UserTwoPos = IndividualPosition[1];
        
        fill(0,255,0);
        ellipse(position.x, position.y, 25, 25);
        
        line(UserOnePos.x, UserOnePos.y, UserTwoPos.x, UserTwoPos.y);
        break;
        
      case 3:
        IndividualPosition[2] = position;
        UserThreePos = IndividualPosition[2];
        
        fill(0, 0, 255);
        ellipse(position.x, position.y, 25, 25);
        
        line(UserOnePos.x, UserOnePos.y, UserTwoPos.x, UserTwoPos.y);
        line(UserOnePos.x, UserOnePos.y, UserThreePos.x, UserThreePos.y);
        line(UserTwoPos.x, UserTwoPos.y, UserThreePos.x, UserThreePos.y);
        break;
    }
    
    if (IndividualPosistion.size() < 3){
    println("User ID: " + userId + " Position: " + IndividualPosition[userId-1]);
    }
    
  }
}

