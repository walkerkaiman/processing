class Unit {
  PVector offset, a, b, c, d, e, f, g, h;
  float rotation, l;
  boolean direction;
  
  Unit (PVector temp_offset, boolean temp_direction, float temp_length) {
    offset = temp_offset;
    direction = temp_direction;
    l = temp_length;
  }
  
  void update () {
      float noise = noise(offset.x / 2000, offset.y / 2000, time);
      float faceOffset = noise * 100;
      rotation = noise * 180;
      
      a = new PVector(- size, - size*5, l);
      b = new PVector(size, - size*5, l);
      c = new PVector(- size, size*5, l);
      d = new PVector(size, size*5, l);
      e = new PVector(- size, - size*5, -l);
      f = new PVector(size, - size*5, -l);
      g = new PVector(- size, size*5, -l);
      h = new PVector(size, size*5, -l);

  }
  
  void display () {
    
    pushMatrix();
   
    if (direction) {
     rotateY(radians(90)); 
    }
    
    translate(offset.x, offset.y);
    rotateZ(rotation);
   
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(b.x, b.y, b.z);
      vertex(d.x, d.y, d.z);
      vertex(c.x, c.y, c.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(b.x, b.y, b.z);
      vertex(f.x, f.y, f.z);
      vertex(h.x, h.y, h.z);
      vertex(d.x, d.y, d.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(c.x, c.y, c.z);
      vertex(d.x, d.y, d.z);
      vertex(h.x, h.y, h.z);
      vertex(g.x, g.y, g.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(c.x, c.y, c.z);
      vertex(g.x, g.y, g.z);
      vertex(e.x, e.y, e.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(a.x, a.y, a.z);
      vertex(b.x, b.y, b.z);
      vertex(f.x, f.y, f.z);
      vertex(e.x, e.y, e.z);
    endShape(CLOSE);
    
    beginShape();
      vertex(e.x, e.y, e.z);
      vertex(f.x, f.y, f.z);
      vertex(h.x, h.y, h.z);
      vertex(g.x, g.y, g.z);
    endShape(CLOSE);
    popMatrix();
  }
}