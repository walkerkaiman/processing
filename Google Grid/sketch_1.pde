import peasy.*;
PeasyCam cam;

final float size = 10;

final int population = 50;
final float gridSpacing = 25;
final float faceAmp = 100;
final color black = color(0);
final color white = color(255);

float time = 0;
boolean recording = false;
ArrayList<Unit> units;

void setup () {
  cam = new PeasyCam(this, 1000);
  
  size(600, 600, P3D);
  stroke(white);
  fill(black);
  
  units = new ArrayList<Unit>();
  
  for (int y = 0; y < population; y++) {
    for (int x = 0; x < population; x++) {
      float l = population * gridSpacing *.75 - faceAmp;
      PVector pos = new PVector(x * gridSpacing - (l * .75), y * gridSpacing);
      float faceOffset = cos(pos.x/200 + pos.y/200) * faceAmp;

      if (y % 2 == 0) {
        Unit newUnit = new Unit(pos, true, l - faceOffset);
        units.add(newUnit);
      }else {
        Unit newUnit = new Unit(pos, false, -l + faceOffset);
        units.add(newUnit);
      }
    }
  }
}

void draw () {
  background(black);
  
  for (Unit u : units) {
    u.update();
    u.display();
  }
  
  time += .001;
  
  if (recording) {
    saveFrame("data/frames/frame_#####.png");
  }
}