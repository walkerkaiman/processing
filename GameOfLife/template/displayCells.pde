void displayCells () {
  for (int x = 0; x < cols; x++) {
    for (int y = 0; y < rows; y++) {
      fill(cells[x][y] ? aliveColor : deadColor); // Checks if cell is alive and fills it the proper color.
      rect(x * cellSize, y * cellSize, cellSize, cellSize);
    }
  } 
}