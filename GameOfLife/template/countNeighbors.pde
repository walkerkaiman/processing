// Counts how many alive neighbors each cells has.
// Takes the column and row position of the cell as a parameter.

int countNeighbors (int x, int y) {
  int aliveTotal = 0; // Store total alive cells from the immediate neighborhood of cells.
  
  for (int neighborX = x-1; neighborX <= x+1; neighborX++) { // Loop through this cell's neighbors
    for (int neighborY = y-1; neighborY <= y+1; neighborY++) {  
      
      if (neighborX == x && neighborY == y) { // Cell should not count itself as a neighbor.
        continue;
      }
      
      if (((neighborX >= 0) && (neighborX < cols)) && ((neighborY >= 0) && (neighborY < rows))) { // Make sure you are not out of bounds
        if (cellsBuffer[neighborX][neighborY]){ // If the neighbor is alive.
          aliveTotal++; // Count the neighbor towards the total.
        }
      }
    }
  } // ends neighbor count loop
  
  return aliveTotal;
}