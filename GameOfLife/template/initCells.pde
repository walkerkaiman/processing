void initCells () {
  for (int x = 0; x < cols; x++) {
    for (int y = 0; y < rows; y++) {
      
      // If a random amount is less than the proability of life, set the cell as alive otherwise, it is dead.
      cells[x][y] = random(100) < probabilityOfLife ? alive : dead; 
    }
  }
}