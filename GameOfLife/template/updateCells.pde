void updateCells() {
  cellsBuffer = cells; // Update cell buffer for new generation.

  for (int x = 0; x < cols; x++) { // Loop through all cells.
    for (int y = 0; y < rows; y++) {
      int neighborPopuation = countNeighbors(x, y);

      if (cellsBuffer[x][y]) { // The cell is alive.
        if (neighborPopuation < 2 || neighborPopuation > 3) { // If the cell has less than 2 or greater than 3 alive neighbors.
          cells[x][y] = dead; // Kill the cell.
        }
      } 
      else { // If the cell is dead.
        if (neighborPopuation == 3) { // If the cell has 3 alive neighbors.
          cells[x][y] = alive; // Make the cell alive.
        }
      } 
    } 
  } // End of cells loop

}