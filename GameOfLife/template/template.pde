final int cellSize = 5; // Width and Height of each cell in pixels
final float probabilityOfLife = 5; // Keep in range 0 - 100.
final int animationSpeed = 30; // FPS

final color aliveColor = color(255); // White
final color deadColor = color(0); // Black
final Boolean alive = true;
final Boolean dead = false;

Boolean[][] cells, cellsBuffer; 
int cols, rows;

void setup() {
  size (600, 600);
  frameRate(animationSpeed);
  noStroke();

  cols = width/cellSize;
  rows = height/cellSize;
  cells = new Boolean[cols][rows];
  cellsBuffer = new Boolean[cols][rows];

  initCells();
}

void draw() {
  updateCells();
  displayCells(); 
}