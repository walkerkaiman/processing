void initCells() {
  for (int x = 0; x < cols; x++) {
    for (int y = 0; y < rows; y++) {
      float seed = random(mouseX) / 100;
      PVector cellPos = new PVector(x * cellSize, y * cellSize);
      float cellWorth = noise(cellPos.x/1000, cellPos.y/1000, seed) * maxWorth;
      
      cells[x][y] = new Cell(x, y, cellPos, cellSize, cellWorth);
    }
  }
}