color fillConvert (float value) {
  float shade = map(value, minWorth, maxWorth, 0, 255);
  return color(shade);
}