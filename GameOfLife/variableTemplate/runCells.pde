void runCells () {
  int cellPopulation = cols * rows;
  
  for (int i = 0; i < cellPopulation; i++) {
    int index_X = int(random(0, cols));
    int index_Y = int(random(0, rows));
    
    cells[index_X][index_Y].run();
  }
}