class Cell {
  PVector pos;
  float worth, size;
  color cellFill;
  int x, y, richerNeighbors, sameNeighbor;
  
  Cell (int x, int y, PVector pos, float size, float worth) {
    this.pos = pos;
    this.size = size;
    this.worth = worth;
    this.x = x;
    this.y = y;
    display();
  }
  
  void run () {
    update();
    display();
  }
  
  void update () {
    applyRules(countRicher());
    //worth = constrain(worth, minWorth, maxWorth);
  }
  
  int countSame () {
    int sameNeighbors = 0;
    
    if (x > 0 && y > 0) { // Does the NW neighbor exists?
      if (cells[x-1][y-1].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
    
    if (y > 0) { // Does the N neighbor exists?
      if (cells[x][y-1].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
     
    if (x < xLimit && y > 0) { // Does the NE neighbor exists?
      if (cells[x+1][y-1].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
    
    if (x > 0) { // Does the W neighbor exists?
      if (cells[x-1][y].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
    
    if (x < xLimit) { // Does the E neighbor exists?
      if (cells[x+1][y].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
    
    if (x > 0 && y < yLimit) { // Does the SW neighbor exists?
      if (cells[x-1][y+1].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
    
    if (y < yLimit) { // Does the S neighbor exists?
      if (cells[x][y+1].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
    
    if (y < yLimit && x < xLimit) { // Does the SE neighbor exists?
      if (cells[x+1][y+1].worth == worth) { // Does the neighbor have same worth?
        sameNeighbors++;
      }
    }
    
    return sameNeighbors;
  }
  
  
  
  int countRicher () {
    richerNeighbors = 0;
        
    if (x > 0 && y > 0) { // Does the NW neighbor exists?
      if (cells[x-1][y-1].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    if (y > 0) { // Does the N neighbor exists?
      if (cells[x][y-1].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    if (x < xLimit && y > 0) { // Does the NE neighbor exists?
      if (cells[x+1][y-1].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    if (x > 0) { // Does the W neighbor exists?
      if (cells[x-1][y].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    if (x < xLimit) { // Does the E neighbor exists?
      if (cells[x+1][y].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    if (x > 0 && y < yLimit) { // Does the SW neighbor exists?
      if (cells[x-1][y+1].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    if (y < yLimit) { // Does the S neighbor exists?
      if (cells[x][y+1].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    if (y < yLimit && x < xLimit) { // Does the SE neighbor exists?
      if (cells[x+1][y+1].worth > worth) { // Does the neighbor have a larger worth?
        richerNeighbors++;
      }
    }
    
    return richerNeighbors;
  }
  
  void applyRules (int neighborCount) { 
    switch(neighborCount) {
      case 0:
        worth -= exchange;
        break;
      case 1:
        worth += exchange;
        break;
      case 2:
        worth -= exchange;
        break;
      case 3:
        worth -= exchange;
        break;
      case 4:
        worth -= exchange;
        break;
      case 5:
        worth += exchange;
        break;
      case 6:
        worth -= exchange;
        break;
      case 7:
        worth += exchange;
        break;
      case 8:
        worth += exchange;
        break;
    }
  }
  
  void display () {
    fill(fillConvert(worth >= 255 ? 0 : worth));
    rect(pos.x, pos.y, size, size);
  }
}