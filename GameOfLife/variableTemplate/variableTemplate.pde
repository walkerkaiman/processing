final int cellSize = 5;
final int animationDelay = 50;
final float minWorth = 0;
final float maxWorth = 100;
final float exchange = 100;
    
Cell [][] cells;
int lastRecordedTime, cols, rows, xLimit, yLimit;

void setup () {
  size (600, 600);
  noStroke();

  cols = width/cellSize;
  rows = height/cellSize;
  yLimit = rows - 1;
  xLimit = cols - 1;
  cells = new Cell[cols][rows];
  
  initCells();
  
  for (int x = 0; x < cols; x++) {
    for (int y = 0; y < rows; y++) {
      cells[x][y].display();
    }
  }
}

void draw () {
  runCells();
  delay(animationDelay);
}

void mousePressed () {
  initCells();
}