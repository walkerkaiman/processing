class Cloud{
  ArrayList<PVector>points;
  float tall, deep;
  int step, weight;
  
  Cloud(float temp_tall, float temp_deep, int temp_step, int temp_weight){
    points = new ArrayList<PVector>();
    tall = temp_tall;
    deep = temp_deep;
    step = temp_step;
    weight = temp_weight;
  }
  
  void display(){
    points = new ArrayList<PVector>();
    
    strokeWeight(weight);
    
    for(int x = 0; x < kinect.width; x += step){
      for(int y = 0; y < kinect.height; y += step){
        int offset = x + y * kinect.width;
        int rawDepth = depth[offset];
        PVector currentPoint = new PVector(x, y, rawDepth);
        
        if(rawDepth > tall && rawDepth < deep){
          float r = map(rawDepth, tall, deep, 52, 247);
          float g = map(rawDepth, tall, deep, 255, 47);
          float b = map(rawDepth, tall, deep, 250, 188);
          
          stroke(r, g, b);
          
          point(currentPoint.x, currentPoint.y, -currentPoint.z);
          points.add(currentPoint);
        }
      }
    }
    
  }
  
  
}