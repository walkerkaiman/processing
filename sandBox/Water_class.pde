class Water{
  ArrayList<Particle>particles;
  PVector lowest;
  int population;
  
   Water(int temp_pop){
     particles = new ArrayList<Particle>();
     lowest = new PVector(0, 0, 0);
     population = temp_pop;
     
     for(int i = 0; i < population; i++){
       //Particle(x, y, size);
       particles.add(new Particle(random(width), random(height), 15));
     }
   }
   
   void run(){
     for(int i = 0; i < particles.size(); i++){
       Particle current = particles.get(i);
       
       //checkBounds(i, current.pos);
       current.update();
     }
     
     for(int i = 0; i < particles.size(); i++){
       particles.get(i).display();
     }
   }
   
   void checkBounds(int i, PVector location){

     if(location.z < sand.tall || location.z > sand.deep || location.x < 0 || location.x > kinect.width || location.y < 0 || location.y > kinect.height){
       particles.remove(i);
       particles.add(new Particle(random(width), random(height), 15));
     }
   }
}