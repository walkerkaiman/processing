class Particle{
  PVector pos, vel, accel, downHill;
  float size, topSpeed, topForce, gravity;
  
  Particle(float temp_x, float temp_y, float temp_size){
    size = temp_size;
    pos = new PVector(temp_x, temp_y, sand.tall);
    vel = new PVector();
    accel = new PVector();
    downHill = new PVector();
    
    topSpeed = 5;
    topForce = .3;
    gravity = .2;
  }
  
  void update(){
    accel.z -= gravity;
    findDownhill();
    checkCollision();
    seek(downHill);
    
    vel.add(accel);
    vel.limit(topSpeed);
    pos.add(vel);
    accel.mult(0);
  }
  
  void display(){
    noStroke();
    fill(255);
    
    pushMatrix();
      translate(pos.x, pos.y, pos.z);
      ellipse(0, 0, size, size);
    popMatrix();
  }
  
  PVector findDownhill(){
  
    //Depth value of neighbors
    if(pos.x > 1 && pos.x < kinect.width-1 && pos.y > 1 && pos.y < kinect.height-1){
      int index = int(pos.x) + int(pos.y)*kinect.width;
      int index_depth = depth[index];
      
      int n_index = index - kinect.width;
      int n_depth = depth[n_index];
      
      int nw_index = n_index - 1;
      int nw_depth = depth[nw_index];
      
      int ne_index = n_index + 1;
      int ne_depth = depth[ne_index];
      
      int s_index = index + kinect.width;
      int s_depth = depth[s_index];
      
      int sw_index = s_index - 1;
      int sw_depth = depth[sw_index];
      
      int se_index = s_index + 1;
      int se_depth = depth[se_index];
      
      int w_index = index - 1;
      int w_depth = depth[w_index];
      
      int e_index = index + 1;
      int e_depth = depth[e_index];
      
      IntList neighbors = new IntList(n_depth, nw_depth, ne_depth, s_depth, sw_depth, se_depth, w_depth, e_depth, index_depth);
      int deepest = neighbors.max();
      
      if(deepest > sand.tall && deepest < sand.deep){
        if(n_depth == deepest){
          downHill = new PVector(pos.x, pos.y-sand.step, -n_depth);
        }
        else if(nw_depth == deepest){
          downHill = new PVector(pos.x-sand.step, pos.y-sand.step, -nw_depth);
        }
        else if(ne_depth == deepest){
          downHill = new PVector(pos.x+sand.step, pos.y-sand.step, -ne_depth);
        }
        else if(s_depth == deepest){
          downHill = new PVector(pos.x, pos.y+sand.step, -s_depth);
        }
        else if(sw_depth == deepest){
          downHill = new PVector(pos.x-sand.step, pos.y+sand.step, -sw_depth);
        }
        else if(se_depth == deepest){
          downHill = new PVector(pos.x+sand.step, pos.y+sand.step, -se_depth);
        }
        else if(e_depth == deepest){
          downHill = new PVector(pos.x+sand.step, pos.y, -e_depth);
        }
        else if(w_depth == deepest){
          downHill = new PVector(pos.x-sand.step, pos.y, -w_depth);
        }else{downHill = pos;}
      }
    }
    
    return downHill;
  }
  
  void seek(PVector downHill){
    PVector desired = PVector.sub(downHill, pos);
    desired.setMag(topSpeed);
    
    PVector steer = PVector.sub(desired, vel);
    steer.limit(topForce);
    
    accel.add(steer);
  }
  
  void checkCollision(){
    float desiredSeperation = size/4;
    PVector sum = new PVector();
    int count = 0;
    
    for(Particle current : flow.particles){
      float distance = PVector.dist(pos, current.pos);
      
      if(distance < desiredSeperation && distance > 0){
        PVector diff = PVector.sub(pos, current.pos);
        diff.normalize();
        diff.div(distance);
        sum.add(diff);
        count++;
      }
      
      if(count > 0){
        sum.setMag(topSpeed);
        
        PVector steer = PVector.sub(sum, vel);
        steer.limit(topForce);
        accel.add(steer);
      }
      
    }
    
    for(int i = 0; i < sand.points.size(); i++){
      PVector current = sand.points.get(i);
      float d = dist(current.x, current.y, current.z, pos.x, pos.y, pos.z);
      if(d < 2){
        vel.mult(0);
      }
    }
  }
  
}