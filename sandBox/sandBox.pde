import peasy.*;
import org.openkinect.processing.*;

Kinect kinect;
PeasyCam cam;
Cloud sand;
Water flow;

int[] depth;
float tilt;

void setup(){
  size(640, 480, P3D);
  
  cam = new PeasyCam(this, 1000);
  
  kinect = new Kinect(this);
  kinect.initDepth();
  
  depth = kinect.getRawDepth();
  
  //Cloud(min, max, step, strokeWeight);
  sand = new Cloud(200, 800, 4, 3);
  
  //Water(population)
  flow = new Water(500);
  
  tilt = kinect.getTilt();
}

void draw(){
  depth = kinect.getRawDepth();

  background(0);
  sand.display();
  flow.run();
}

void keyPressed(){
  if(key == CODED){
    switch(keyCode){
      case UP:
        tilt += 1;
        kinect.setTilt(tilt);
        println("Tilt: "+tilt);
      break;
      case DOWN:
        tilt -= 1;
        kinect.setTilt(tilt);
        println("Tilt: "+tilt);
      break;
    }
  }
}